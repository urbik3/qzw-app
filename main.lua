-- -----------------------------------------------------------------------------------------
-- --
-- -- main.lua
-- --
-- -----------------------------------------------------------------------------------------

require "ssk2.loadSSK"
_G.ssk.init( { math2DPlugin = true } )

require "lib.sortable_group"
require "lib.urlencode"

math.randomseed( os.time() )

system.activate("multitouch")
display.setStatusBar(display.HiddenStatusBar)
native.setProperty("androidSystemUiVisibility", "immersiveSticky")

-- REQUIRE MODULES
require "src.other.analytics"

local app = require("src.app")
app.data = {}
app.screens = {}

app.fn = require("src.fn")

app.data.lang = require("src.data.lang")
app.data.story = require("src.data.story")
app.data.achievement = require("src.data.achievement")

app.flurry = require("plugin.flurry.analytics")
-- app.ads = require("src.other.ads")
app.game = require("src.game")
app.bg = require("src.bg")
app.gui = require("src.gui")
app.player = require("src.player")
app.enemy = require("src.enemy")
app.shot = require("src.shot")
app.portal = require("src.portal")
app.turret = require("src.turret")
app.terrain = require("src.terrain")
app.consumable = require("src.consumable")
app.obstacle = require("src.obstacle")
app.sound = require("src.sound")
app.tutorial = require("src.other.tutorial")
app.leaderboards = require("src.other.leaderboards")
app.network = require("src.other.network")

app.level = require("src.managers.level")
app.screen = require("src.managers.screen")
app.state = require("src.managers.state")

app.screens.levels = require("src.screens.levels")
-- app.screens.pause = require("src.screens.pause")
app.screens.confirmation = require("src.screens.confirmation")
app.screens.win = require("src.screens.win")
app.screens.lose = require("src.screens.lose")
app.screens.gameSelect = require("src.screens.gameSelect")
app.screens.story = require("src.screens.story")
app.screens.username = require("src.screens.username")
app.screens.credits = require("src.screens.credits")
app.screens.settings = require("src.screens.settings")
app.screens.lang = require("src.screens.lang")
app.screens.achievements = require("src.screens.achievements")
app.screens.newEnemy = require("src.screens.newEnemy")
app.screens.menu = require("src.screens.menu")
app.screens.menuEnemies = require("src.screens.menuEnemies")
app.screens.menuAchievements = require("src.screens.menuAchievements")

-- GAME INIT
app.state.loadSavedData()
app.screen.init()
app.shot.init()
app.enemy.init()
app.sound.init()

if(not app.state.lang) then
    app.screens.lang.show()
else
    app.screens.gameSelect.show()
    app.screens.username.checkUsername()
end