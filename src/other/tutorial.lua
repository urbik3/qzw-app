local app = require("src.app")
local game = require("src.game")
local screen = require("src.managers.screen")

local tutorial = {}

tutorial.firstEnemySighted = true
tutorial.firstInfoConsumableSighted = true
tutorial.firstArmorDrop = true
tutorial.firstHealthpackDrop = true

tutorial.createPopup = function(btnText, text, resumeCb)
    tutorial.popupGroup = display.newGroup()
    screen.tutorialCont:insert(tutorial.popupGroup)

    local bg = display.newRect(tutorial.popupGroup, 0, 0, display.actualContentWidth, display.actualContentHeight)
    bg.fill = {0, .5}
    bg.x = display.actualContentWidth/2
    bg.y = display.actualContentHeight/2
    bg:addEventListener("touch", function()
        return true
    end)

    local window = display.newImageRect(tutorial.popupGroup, "assets/ui/window-small.png", 512, 638)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.actualContentWidth/2
    window.y = display.actualContentHeight/2

    local defaultBox = display.newText({
        x = display.actualContentWidth/2,
        y = display.actualContentHeight/2 - 80,
        width = 120,
        height = 130,
        parent = tutorial.popupGroup, text = text,
        font = screen.font,
        fontSize = 11,})
    defaultBox.fill = screen.textColorLighter
    defaultBox.anchorY = 0

    local btnResume = screen.createTextBtn(tutorial.popupGroup, btnText, resumeCb)
    btnResume.x = display.actualContentWidth/2
    btnResume.y = display.actualContentHeight/2 + 65
end

tutorial.destroyPopup = function()
    if(tutorial.popupGroup) then
        tutorial.popupGroup:removeSelf()
        tutorial.popupGroup = nil
    end
end

return tutorial