local app = require("src.app")
local json = require("json")

local leaderboards = {}

leaderboards.SHOW_TOP = "showTop"
leaderboards.SHOW_PLAYER = "showPlayer"
leaderboards.SHOW_NUM = 11

leaderboards.popup = nil
leaderboards.data = nil
leaderboards.startI = nil
leaderboards.showData = leaderboards.SHOW_TOP

leaderboards.getBoard = function(character, chapter)
    local function networkListener( event )
        if ( event.isError ) then
            print( "Network error: ", event.response )
        else
            local data = json.decode(event.response)
            if(data) then
                leaderboards.data = data
                leaderboards.showResults()
            else
                local noData = display.newText({
                    text = app.data.lang.get("networkError"), parent = leaderboards.popup.popupGroup, font = app.screen.font, fontSize = 12, align = "center",
                    x = display.contentCenterX, y = display.contentCenterY})
                noData.fill = app.screen.textColor

                if(leaderboards.popup.loader) then
                    leaderboards.popup.loader:removeSelf()
                    leaderboards.popup.loader = nil
                end
            end
        end
    end
    
    network.request(app.network.url.."?getLeaderboard&chapter="..chapter.."&character="..character, "GET", networkListener)
    leaderboards.showBoard()
end

leaderboards.destroyBoard = function()
    leaderboards.popup.popupGroup:removeSelf()
    leaderboards.popup.popupGroup = nil
    leaderboards.popup = nil
    leaderboards.showData = leaderboards.SHOW_TOP
end

leaderboards.destroyResults = function()
    if(leaderboards.popup.databoard) then
        leaderboards.popup.databoard:removeSelf()
        leaderboards.popup.databoard = nil
    end
end

leaderboards.showBoard = function()
    leaderboards.popup = app.screen.createPopupLarge(app.data.lang.get("leaderboards"))
    local loader = app.screen.createLoader(leaderboards.popup.popupGroup)
    loader.x = display.contentCenterX
    loader.y = display.contentCenterY
    leaderboards.popup.loader = loader

    local btn = app.screen.createBtn(leaderboards.popup.popupGroup, "back", function()
        leaderboards.destroyResults()
        leaderboards.destroyBoard()
        app.screens.levels.showMenu()
    end)
    btn.x = display.contentCenterX - 150
    btn.y = 285
end

leaderboards.showResults = function()
    if(leaderboards.popup.loader) then
        leaderboards.popup.loader:removeSelf()
        leaderboards.popup.loader = nil
    end

    if(not leaderboards.data) then
        return
    end

    leaderboards.startI = nil
    for i = 1, #leaderboards.data, 1 do
        if(app.state.username == leaderboards.data[i]["username"]) then
            leaderboards.startI = i
            break
        end
    end

    leaderboards.popup.databoard = display.newGroup()
    app.screen.menuCont:insert(leaderboards.popup.databoard)

    local bestLocalScore = app.state.score[app.state.actualCharacter][app.state.actualChapter]

    if(leaderboards.startI and leaderboards.startI > (leaderboards.SHOW_NUM+1)/2) then
        local btnToggleBoardData = app.screen.createTextBtn(leaderboards.popup.databoard, leaderboards.showData == leaderboards.SHOW_TOP and app.data.lang.get("myResult") or app.data.lang.get("topResults"), function()
            if(not leaderboards.data) then
                return
            end
    
            if(leaderboards.showData == leaderboards.SHOW_TOP) then
                leaderboards.showData = leaderboards.SHOW_CENTER
            else
                leaderboards.showData = leaderboards.SHOW_TOP
                leaderboards.startI = 1
            end
    
            leaderboards.destroyResults()
            leaderboards.showResults()
        end)
        btnToggleBoardData.x = display.contentCenterX + (bestLocalScore and -35 or 0)
        btnToggleBoardData.y = 285
    end

    if(bestLocalScore) then
        local btnToggleBoardData = app.screen.createTextBtn(leaderboards.popup.databoard, app.data.lang.get("uploadBestScore"), function()
            app.network.postScore(bestLocalScore)
        end)
        btnToggleBoardData.x = display.contentCenterX + 95
        btnToggleBoardData.y = 285
        btnToggleBoardData.text.size = 8
    end
    
    local data = leaderboards.data
    local startI = leaderboards.startI and leaderboards.startI or 1
    if(#data < 1) then
        local noData = display.newText({
            text = app.data.lang.get("noDataLeaderboards"), parent = leaderboards.popup.databoard, font = app.screen.font, fontSize = 16, align = "center",
            x = display.contentCenterX, y = display.contentCenterY})
        noData.fill = app.screen.textColor
    else
        local maxI = 0
        local actualStartI = leaderboards.showData == leaderboards.SHOW_TOP and 1 or math.min(math.max(1, startI - (leaderboards.SHOW_NUM-1)/2), #data - (leaderboards.SHOW_NUM-1))
        for i = actualStartI, #data, 1 do
            maxI = maxI + 1
            if(maxI > leaderboards.SHOW_NUM) then
                break
            end
            if(data[i]) then
                local item = data[i]
                local positionI = i - actualStartI + 1
                local time = math.round(item.score/1000)
                if(i%2==0) then
                    local rekt = display.newRect(leaderboards.popup.databoard, 50, 46 + positionI*18, 380, 18)
                    rekt.anchorX = 0
                    rekt.fill = app.screen.textColorLighter
                    rekt.alpha = .05
                end
                local num = display.newText({
                    text = i..".", parent = leaderboards.popup.databoard, font = app.screen.font, fontSize = 10, align = "left",
                    x = 70, y = 46 + positionI*18})
                local username = display.newText({
                    text = item.username, parent = leaderboards.popup.databoard, font = app.screen.font, fontSize = 10, align = "left",
                    x = 140, y = 46 + positionI*18})
                local score = display.newText({
                    text = math.floor(time/60).."m "..(time%60).."s", parent = leaderboards.popup.databoard, font = app.screen.font, fontSize = 10, align = "right",
                    x = 360, y = 46 + positionI*18})
                local fill = item.username == app.state.username and app.screen.textColorLighter or app.screen.textColor
                num.fill = fill
                username.fill = fill
                score.fill = fill
            end
        end
    end
end

return leaderboards