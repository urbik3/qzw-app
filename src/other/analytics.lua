local flurry = require( "plugin.flurry.analytics" )

local function flurryListener( event )
    if ( event.phase == "init" ) then  -- Successful initialization
        print( event.provider )
        flurry.logEvent("Start app")
    end
end

local apiKey = system.getInfo( "platformName" ) == "Android" and "CW9BSNZSTJJGM3YKPHQM" or "CW9BSNZSTJJGM3YKPHQM"

flurry.init(flurryListener, {apiKey = apiKey})