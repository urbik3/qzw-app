local app = require("src.app")
local json = require("json")
local sha1 = require("lib.sha1")

local networkManager = {}

networkManager.UID = "_F4rz7-e*04te47y$dFDDdf/78"

networkManager.url = "http://urbik3.4fan.cz/"

networkManager.createHash = function(values)
    local str = ""
    for i = 1, #values, 1 do
        str = str..values[i]
    end
    return sha1(app.network.UID..str)
end

local postScore
postScore = function(score)
    local loader
    local function networkListener(event)
        if (event.isError) then
            print("Network error: ", event.response)
        else
            print(event.response)
            local response = json.decode(event.response)
            if(response and (response.success or response.warning)) then
                app.state.score[app.state.actualCharacter][app.state.actualChapter] = nil
                app.state.saveData()

                if(response and response.success and response.success == "scoreInserted") then
                    local popup = app.screen.createPopup()
                    
                    local text = app.data.lang.get("scoreUpdated")
                    local infoText = display.newText({parent = popup.popupGroup, text = text, width = 120, x = display.contentCenterX, y = display.contentCenterY - 65, fontSize = 10})
                    infoText.fill = app.screen.textColor

                    local okBtn = app.screen.createTextBtn(popup.popupGroup, "Continue", function()
                        popup.popupGroup:removeSelf()
                        popup.popupGroup = nil
                        popup = nil
                    end)
                    okBtn.x = display.contentCenterX
                    okBtn.y = display.contentCenterY + 65
                end
            else
                local popup = app.screen.createPopup()
                
                local text = app.data.lang.get("scoreNotUpdated")
                local infoText = display.newText({parent = popup.popupGroup, text = text, width = 120, x = display.contentCenterX, y = display.contentCenterY - 65, fontSize = 10})
                infoText.fill = app.screen.textColor

                local okBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("tryAgain"), function()
                    popup.popupGroup:removeSelf()
                    popup.popupGroup = nil
                    popup = nil
                    postScore(score)
                end)
                okBtn.x = display.contentCenterX
                okBtn.y = display.contentCenterY + 65

                local saveLocallyBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("saveLocally"), function()
                    popup.popupGroup:removeSelf()
                    popup.popupGroup = nil
                    popup = nil
                    app.state.saveScoreLocally()
                end)
                saveLocallyBtn.x = display.contentCenterX
                saveLocallyBtn.y = display.contentCenterY + 20
                saveLocallyBtn.text.size = 12
            end
        end
        loader:removeSelf()
    end
    
    local hash = app.network.createHash({
        score,
        app.state.username,
        app.state.actualChapter,
        app.state.actualCharacter})
    local url = networkManager.url.."?postScore&score="..encodeURI(score).."&username=".. encodeURI(app.state.username).."&chapter="..encodeURI(app.state.actualChapter).."&character="..encodeURI(app.state.actualCharacter).."&hash="..encodeURI(hash)
    network.request(url, "POST", networkListener)
    loader = app.screen.createLoader(app.screen.menuCont)
    loader.x = display.contentCenterX
    loader.y = display.contentCenterY
end

networkManager.postScore = function(score)
    if(not app.state.username) then
        app.screens.username.checkUsername(function()
            postScore(score)
        end)
    else
        postScore(score)
    end
end

return networkManager