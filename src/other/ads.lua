local app = require("src.app")

local ads = {}

-- APPLOVIN
local applovin = require( "plugin.applovin" )

local function adListener( event )
 
    if ( event.phase == "init" ) then  -- Successful initialization
        print( event.isError )
        -- Load an AppLovin ad
        applovin.load( "interstitial" )
 
    elseif ( event.phase == "loaded" ) then  -- The ad was successfully loaded
        print( event.type )
 
    elseif ( event.phase == "failed" ) then  -- The ad failed to load
        print( event.type )
        print( event.isError )
        print( event.response )
 
    elseif ( event.phase == "displayed" or event.phase == "playbackBegan" ) then  -- The ad was displayed/played
        print( event.type )
 
    elseif ( event.phase == "hidden" or event.phase == "playbackEnded" ) then  -- The ad was closed/hidden
        print( event.type )
        applovin.load( "interstitial" )
 
    elseif ( event.phase == "clicked" ) then  -- The ad was clicked/tapped
        print( event.type )
    end
end
 
-- Initialize the AppLovin plugin
applovin.init( adListener, { sdkKey="KkExJOltB70uGuMChKCpnHiwDLVpmZhBPErKrd9vdnMDBLcqWUEoUibmEJOEksQud049WAVuXggQOAklTa29zQ" } )


return ads