local app = require("src.app")
local game = require("src.game")
local state = require("src.managers.state")

local bg = {
    [state.CHAPTER_TUTORIAL] = {},
    [state.CHAPTER_CASUAL] = {},
    [state.CHAPTER_HARD] = {},
    [state.CHAPTER_ENDLESS] = {},
}

-- 
-- CHAPTER TUTORIAL
-- 

-- STONES
bg[state.CHAPTER_TUTORIAL].init1 = function()
    local background = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone1.jpg", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = .5
    background.yScale = .5
    background.anchorX = 0
    background.anchorY = 0

    bg.width = background.width * background.xScale
    bg.height = background.height * background.yScale
    bg.stagePadding = 120 * background.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/stone/bg_stone_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end

-- 
-- CHAPTER CASUAL
-- 

-- STONES
bg[state.CHAPTER_CASUAL].init1 = function()
    local background = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone1.jpg", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = .5
    background.yScale = .5
    background.anchorX = 0
    background.anchorY = 0

    bg.width = background.width * background.xScale
    bg.height = background.height * background.yScale
    bg.stagePadding = 120 * background.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/stone/bg_stone_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_CASUAL].init2 = bg[state.CHAPTER_CASUAL].init1
bg[state.CHAPTER_CASUAL].init3 = bg[state.CHAPTER_CASUAL].init1

bg[state.CHAPTER_CASUAL].init4 = function()
    local bgScale = .5
    local background = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone2_01.jpg", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = bgScale
    background.yScale = bgScale
    background.anchorX = 0
    background.anchorY = 0
    
    local background2 = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone2_02.jpg", 1952, 2048)
    background2.x = 2048 * bgScale
    background2.y = 0
    background2.xScale = bgScale
    background2.yScale = bgScale
    background2.anchorX = 0
    background2.anchorY = 0
    
    local background3 = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone2_03.jpg", 2048, 952)
    background3.x = 0
    background3.y = 2048 * bgScale
    background3.xScale = bgScale
    background3.yScale = bgScale
    background3.anchorX = 0
    background3.anchorY = 0
    
    local background4 = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone2_04.jpg", 1952, 952)
    background4.x = 2048 * bgScale
    background4.y = 2048 * bgScale
    background4.xScale = bgScale
    background4.yScale = bgScale
    background4.anchorX = 0
    background4.anchorY = 0

    bg.width = 4000 * background4.xScale
    bg.height = 3000 * background4.yScale
    bg.stagePadding = 120 * background4.xScale

    -- SYKBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/stone/bg_stone_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_CASUAL].init5 = bg[state.CHAPTER_CASUAL].init4

-- DESERT
bg[state.CHAPTER_CASUAL].init6 = function()
    local background = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert1.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = .5
    background.yScale = .5
    background.anchorX = 0
    background.anchorY = 0

    bg.width = background.width * background.xScale
    bg.height = background.height * background.yScale
    bg.stagePadding = 160 * background.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/desert/bg_desert_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_CASUAL].init7 = bg[state.CHAPTER_CASUAL].init6
bg[state.CHAPTER_CASUAL].init8 = bg[state.CHAPTER_CASUAL].init6

bg[state.CHAPTER_CASUAL].init9 = function()
    local bgScale = .5
    local background = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert2_01.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = bgScale
    background.yScale = bgScale
    background.anchorX = 0
    background.anchorY = 0

    local background2 = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert2_02.png", 1952, 2048)
    background2.x = 2048 * bgScale
    background2.y = 0
    background2.xScale = bgScale
    background2.yScale = bgScale
    background2.anchorX = 0
    background2.anchorY = 0

    local background3 = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert2_03.png", 2048, 952)
    background3.x = 0
    background3.y = 2048 * bgScale
    background3.xScale = bgScale
    background3.yScale = bgScale
    background3.anchorX = 0
    background3.anchorY = 0

    local background4 = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert2_04.png", 1952, 952)
    background4.x = 2048 * bgScale
    background4.y = 2048 * bgScale
    background4.xScale = bgScale
    background4.yScale = bgScale
    background4.anchorX = 0
    background4.anchorY = 0

    bg.width = 4000 * background4.xScale
    bg.height = 3000 * background4.yScale
    bg.stagePadding = 160 * background4.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/desert/bg_desert_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_CASUAL].init10 = bg[state.CHAPTER_CASUAL].init9

-- LAVA
bg[state.CHAPTER_CASUAL].init11 = function()
    local background = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava1.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = .5
    background.yScale = .5
    background.anchorX = 0
    background.anchorY = 0

    bg.width = background.width * background.xScale
    bg.height = background.height * background.yScale
    bg.stagePadding = 80 * background.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/lava/bg_lava_skybox.png", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_CASUAL].init12 = bg[state.CHAPTER_CASUAL].init11
bg[state.CHAPTER_CASUAL].init13 = bg[state.CHAPTER_CASUAL].init11

bg[state.CHAPTER_CASUAL].init14 = function()
    local bgScale = .5
    local background = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava2_01.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = bgScale
    background.yScale = bgScale
    background.anchorX = 0
    background.anchorY = 0
    
    local background2 = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava2_02.png", 1952, 2048)
    background2.x = 2048 * bgScale
    background2.y = 0
    background2.xScale = bgScale
    background2.yScale = bgScale
    background2.anchorX = 0
    background2.anchorY = 0
    
    local background3 = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava2_03.png", 2048, 952)
    background3.x = 0
    background3.y = 2048 * bgScale
    background3.xScale = bgScale
    background3.yScale = bgScale
    background3.anchorX = 0
    background3.anchorY = 0
    
    local background4 = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava2_04.png", 1952, 952)
    background4.x = 2048 * bgScale
    background4.y = 2048 * bgScale
    background4.xScale = bgScale
    background4.yScale = bgScale
    background4.anchorX = 0
    background4.anchorY = 0

    bg.width = 4000 * background4.xScale
    bg.height = 3000 * background4.yScale
    bg.stagePadding = 80 * background4.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/lava/bg_lava_skybox.png", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_CASUAL].init15 = bg[state.CHAPTER_CASUAL].init14

-- 
-- CHAPTER HARD
-- 

-- STONES
bg[state.CHAPTER_HARD].init1 = function()
    local background = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone1.jpg", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = .5
    background.yScale = .5
    background.anchorX = 0
    background.anchorY = 0

    bg.width = background.width * background.xScale
    bg.height = background.height * background.yScale
    bg.stagePadding = 120 * background.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/stone/bg_stone_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end

bg[state.CHAPTER_HARD].init2 = function()
    local bgScale = .5
    local background = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone2_01.jpg", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = bgScale
    background.yScale = bgScale
    background.anchorX = 0
    background.anchorY = 0
    
    local background2 = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone2_02.jpg", 1952, 2048)
    background2.x = 2048 * bgScale
    background2.y = 0
    background2.xScale = bgScale
    background2.yScale = bgScale
    background2.anchorX = 0
    background2.anchorY = 0
    
    local background3 = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone2_03.jpg", 2048, 952)
    background3.x = 0
    background3.y = 2048 * bgScale
    background3.xScale = bgScale
    background3.yScale = bgScale
    background3.anchorX = 0
    background3.anchorY = 0
    
    local background4 = display.newImageRect(game.bgCont, "assets/bg/stone/bg_stone2_04.jpg", 1952, 952)
    background4.x = 2048 * bgScale
    background4.y = 2048 * bgScale
    background4.xScale = bgScale
    background4.yScale = bgScale
    background4.anchorX = 0
    background4.anchorY = 0

    bg.width = 4000 * background4.xScale
    bg.height = 3000 * background4.yScale
    bg.stagePadding = 120 * background4.xScale

    -- SYKBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/stone/bg_stone_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_HARD].init3 = bg[state.CHAPTER_HARD].init2

-- DESERT
bg[state.CHAPTER_HARD].init4 = function()
    local background = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert1.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = .5
    background.yScale = .5
    background.anchorX = 0
    background.anchorY = 0

    bg.width = background.width * background.xScale
    bg.height = background.height * background.yScale
    bg.stagePadding = 160 * background.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/desert/bg_desert_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end

bg[state.CHAPTER_HARD].init5 = function()
    local bgScale = .5
    local background = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert2_01.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = bgScale
    background.yScale = bgScale
    background.anchorX = 0
    background.anchorY = 0

    local background2 = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert2_02.png", 1952, 2048)
    background2.x = 2048 * bgScale
    background2.y = 0
    background2.xScale = bgScale
    background2.yScale = bgScale
    background2.anchorX = 0
    background2.anchorY = 0

    local background3 = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert2_03.png", 2048, 952)
    background3.x = 0
    background3.y = 2048 * bgScale
    background3.xScale = bgScale
    background3.yScale = bgScale
    background3.anchorX = 0
    background3.anchorY = 0

    local background4 = display.newImageRect(game.bgCont, "assets/bg/desert/bg_desert2_04.png", 1952, 952)
    background4.x = 2048 * bgScale
    background4.y = 2048 * bgScale
    background4.xScale = bgScale
    background4.yScale = bgScale
    background4.anchorX = 0
    background4.anchorY = 0

    bg.width = 4000 * background4.xScale
    bg.height = 3000 * background4.yScale
    bg.stagePadding = 160 * background4.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/desert/bg_desert_skybox.jpg", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_HARD].init6 = bg[state.CHAPTER_HARD].init5

-- LAVA
bg[state.CHAPTER_HARD].init7 = function()
    local background = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava1.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = .5
    background.yScale = .5
    background.anchorX = 0
    background.anchorY = 0

    bg.width = background.width * background.xScale
    bg.height = background.height * background.yScale
    bg.stagePadding = 80 * background.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/lava/bg_lava_skybox.png", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end

bg[state.CHAPTER_HARD].init8 = function()
    local bgScale = .5
    local background = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava2_01.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = bgScale
    background.yScale = bgScale
    background.anchorX = 0
    background.anchorY = 0
    
    local background2 = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava2_02.png", 1952, 2048)
    background2.x = 2048 * bgScale
    background2.y = 0
    background2.xScale = bgScale
    background2.yScale = bgScale
    background2.anchorX = 0
    background2.anchorY = 0
    
    local background3 = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava2_03.png", 2048, 952)
    background3.x = 0
    background3.y = 2048 * bgScale
    background3.xScale = bgScale
    background3.yScale = bgScale
    background3.anchorX = 0
    background3.anchorY = 0
    
    local background4 = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava2_04.png", 1952, 952)
    background4.x = 2048 * bgScale
    background4.y = 2048 * bgScale
    background4.xScale = bgScale
    background4.yScale = bgScale
    background4.anchorX = 0
    background4.anchorY = 0

    bg.width = 4000 * background4.xScale
    bg.height = 3000 * background4.yScale
    bg.stagePadding = 80 * background4.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/lava/bg_lava_skybox.png", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end
bg[state.CHAPTER_HARD].init9 = bg[state.CHAPTER_HARD].init8

-- ENDLESS
bg[state.CHAPTER_ENDLESS].init1 = function()
    local background = display.newImageRect(game.bgCont, "assets/bg/lava/bg_lava1.png", 2048, 2048)
    background.x = 0
    background.y = 0
    background.xScale = .5
    background.yScale = .5
    background.anchorX = 0
    background.anchorY = 0

    bg.width = background.width * background.xScale
    bg.height = background.height * background.yScale
    bg.stagePadding = 80 * background.xScale

    -- SKYBOX
    local bgColor = display.newRect(game.skyboxCont, display.screenOriginX, display.screenOriginY, display.actualContentWidth*2, display.actualContentHeight*2)
    bgColor.fill = {0}

    local skybox = display.newImageRect(game.skyboxCont, "assets/bg/lava/bg_lava_skybox.png", 2048, 2048)
    skybox.x = display.actualContentWidth/2
    skybox.y = display.actualContentHeight/2
    skybox.xScale = .9
    skybox.yScale = .9
    skybox.alpha = .5
    bg.farBg = skybox
end

return bg