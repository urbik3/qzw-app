local app = require("src.app")
local game = require("src.game")
local json = require("json")

local gui = {
    joystickCircle = nil,
    joystickCircleInner = nil,
    joystickRadius = 40,
    joystickSensitivity = 10,
    joystickSensitivityOuter = 40,
    guideRadius = 50,
    guiSideWidth = 65,
    gfx = {
        objectiveEnemies = {},
    },
}

gui.init = function()
end

gui.update = function(player, portal, turret, enemies, objectiveEnemies)
    if(portal) then
        gui.updatePortal(player, portal)
    end
    
    if(turret) then
        gui.updateTurret(player, turret)
    end

    if(gui.lockOnEnemy) then
        gui.updateLockOnTarget()
    end

    gui.updateArcHealthIndicator(player)

    gui.updateHealthbarPosition(player)
    for k, enemy in pairs(enemies) do
        if(enemy.hasHealthbar) then
            gui.updateHealthbarPosition(enemy)
        end
    end
    for k, enemy in pairs(objectiveEnemies) do
        gui.updateObjectiveEnemy(player, enemy)
    end

    -- UPDATE TIME
    local time = math.round(game.getGameTime()/1000)
    local timeSum = (app.state.actualChapter == app.state.CHAPTER_ENDLESS or app.state.actualChapter == app.state.CHAPTER_TUTORIAL) and 0 or math.round(app.state.getLevel(app.state.actualLevel).time/1000)
    gui.timeBtnRight.text.text = math.floor((timeSum+time)/60).."m "..((timeSum+time)%60).."s"

    -- UPDATE HEALTH
    app.gui.healthBtnRight.text.text = player.maxHp.."/"..app.state.getMaxLevel().maxHealth.." HP"

    -- UPDATE DEBUG
    -- app.gui.debugBtnRight.text.text = #app.game.enemies.."/"..#app.game.getActiveEnemies(app.game.enemies)
end

gui.createArcHealthIndicator = function(player)
    gui.arcHealthRadius = 15
    gui.arcHealthPadding = 5
    local bg = display.newCircle(app.game.zoomOutCont, gui.arcHealthRadius + gui.arcHealthPadding, gui.arcHealthRadius + gui.arcHealthPadding, gui.arcHealthRadius)
    bg:setFillColor(0, 0, 0, .5)
    gui.updateArcHealthIndicator(player)
end

gui.updateArcHealthIndicator = function(player)
    if(gui.gfx.arcMaxHealth and gui.gfx.arcMaxHealth.x) then
        gui.gfx.arcMaxHealth:removeSelf()
        gui.gfx.arcMaxHealth = nil
    end
    if(gui.gfx.arcHealth and gui.gfx.arcHealth.x) then
        gui.gfx.arcHealth:removeSelf()
        gui.gfx.arcHealth = nil
    end
    local maxHealthSweep = app.fn.mapRange(player.maxHp, 1, app.state.getMaxLevel().maxHealth, 0, 360, true)
    gui.gfx.arcMaxHealth = ssk.display.arc(app.game.zoomOutCont, gui.arcHealthRadius + gui.arcHealthPadding, gui.arcHealthRadius + gui.arcHealthPadding, {
        s = 0,
        sweep = maxHealthSweep,
        radius = gui.arcHealthRadius/2,
        strokeWidth = gui.arcHealthRadius,
        strokeColor = {1, 0, 0},
    })
    gui.gfx.arcHealth = ssk.display.arc(app.game.zoomOutCont, gui.arcHealthRadius + gui.arcHealthPadding, gui.arcHealthRadius + gui.arcHealthPadding, {
        s = 0,
        sweep = app.fn.mapRange(player.hp, 0, player.maxHp, 1, maxHealthSweep, true),
        radius = gui.arcHealthRadius/2,
        strokeWidth = gui.arcHealthRadius,
        strokeColor = {0, 1, 0},
    })
end

gui.lockOnTarget = function(player, enemy)
    gui.lockOnEnemy = enemy
    gui.lockOnCrosshair = player.crosshairSprite
    gui.lockOnCrosshair.isVisible = true

    game.guiCont:insert(player.crosshairSprite)
    player.crosshairSprite:toBack()
    gui.updateLockOnTarget()
end

gui.updateLockOnTarget = function()
    local x, y = game.enemiesCont:localToContent(gui.lockOnEnemy.getPosition().x, gui.lockOnEnemy.getPosition().y)
    gui.lockOnCrosshair.x = x - display.screenOriginX
    gui.lockOnCrosshair.y = y - display.screenOriginY
end

gui.destroyLockOn = function()
    gui.lockOnCrosshair.isVisible = false
    gui.lockOnCrosshair = nil
    gui.lockOnEnemy = nil
end

gui.createTurretGuide = function(player, turret)
    local charSprite = display.newImageRect(game.guiCont, "assets/objects/turret.png", 195, 279)
    charSprite.xScale = game.scale * .5
    charSprite.yScale = game.scale * .5
    gui.gfx.turretGuide = charSprite
    
    gui.updateTurret(player, turret)
end

gui.updateTurret = function(player, turret)
    local middlePoint = {x = display.actualContentWidth/2, y = display.actualContentHeight/2}
    local playerPos = player.getPosition()
    local angle = app.fn.getAngle(playerPos, turret.cont)
    local newPos = ssk.math2d.scale(ssk.math2d.angle2Vector(angle, true), gui.guideRadius)
    gui.gfx.turretGuide.x = middlePoint.x + newPos.x
    gui.gfx.turretGuide.y = middlePoint.y + newPos.y
    
    local distance = ssk.math2d.distanceBetween(playerPos, turret.cont)
    gui.gfx.turretGuide.alpha = app.fn.mapRange(distance, 400, 150, 1, 0, true)
    if(gui.gfx.turretGuide.alpha > .99) then
        gui.gfx.turretGuide.alpha = app.fn.mapRange(distance, 1000, 500, .4, 1, true)
    end
end

gui.removeObjectiveTurret = function(turret)
    local filteredEnemies = {}
    for k, objectiveEnemy in pairs(gui.gfx.objectiveEnemies) do
        if(turret._id ~= objectiveEnemy._id) then
            table.insert(filteredEnemies, objectiveEnemy)
        end
    end
    gui.gfx.objectiveEnemies = filteredEnemies
    gui.gfx.turretGuide:removeSelf()
end

gui.createPortalGuide = function(player, portal)
    local sprite = display.newSprite(game.radarCont, app.portal.spriteSheet, {
        name = "glow",
        start = 1,
        count = 5*5,
        time = 1500,
        loopCount = 0,
    })
    sprite.xScale = game.scale
    sprite.yScale = game.scale
    sprite:play()
    gui.gfx.portalGuide = sprite
    
    gui.updatePortal(player, portal)
end

gui.updatePortal = function(player, portal)
    local middlePoint = {x = display.actualContentWidth/2, y = display.actualContentHeight/2}
    local playerPos = player.getPosition()
    local angle = app.fn.getAngle(playerPos, portal.sprite)
    local newPos = ssk.math2d.scale(ssk.math2d.angle2Vector(angle, true), gui.guideRadius)
    gui.gfx.portalGuide.x = middlePoint.x + newPos.x
    gui.gfx.portalGuide.y = middlePoint.y + newPos.y
    
    local distance = ssk.math2d.distanceBetween(playerPos, portal.sprite)
    gui.gfx.portalGuide.alpha = app.fn.mapRange(distance, 400, 150, 1, 0, true)
    if(gui.gfx.portalGuide.alpha > .99) then
        gui.gfx.portalGuide.alpha = app.fn.mapRange(distance, 1000, 500, .4, 1, true)
    end
end

gui.addObjectiveEnemy = function(player, enemy)
    local sprite = display.newImageRect(game.radarCont, enemy.gfx.imagePath, enemy.gfx.imageWidth, enemy.gfx.imageHeight)
    sprite.xScale = game.scale * .5
    sprite.yScale = game.scale * .5
    enemy.gfx.guideSprite = sprite
    table.insert(gui.gfx.objectiveEnemies, enemy.gfx.guideSprite)
    enemy.isObjective = true
    
    gui.updateObjectiveEnemy(player, enemy)
end

gui.updateObjectiveEnemy = function(player, enemy)
    local middlePoint = {x = display.actualContentWidth/2, y = display.actualContentHeight/2}
    local playerPos = player.getPosition()
    local enemyPosition = enemy.getPosition()
    local angle = app.fn.getAngle(playerPos, enemyPosition)
    local newPos = ssk.math2d.scale(ssk.math2d.angle2Vector(angle, true), gui.guideRadius)
    enemy.gfx.guideSprite.x = middlePoint.x + newPos.x
    enemy.gfx.guideSprite.y = middlePoint.y + newPos.y
    
    local distance = ssk.math2d.distanceBetween(playerPos, enemyPosition)
    enemy.gfx.guideSprite.alpha = app.fn.mapRange(distance, 200, 100, 1, 0, true)
    if(enemy.gfx.guideSprite.alpha > .99) then
        enemy.gfx.guideSprite.alpha = app.fn.mapRange(distance, 1500, 500, .4, 1, true)
    end
end

gui.removeObjectiveEnemy = function(enemy)
    local filteredEnemies = {}
    for k, objectiveEnemy in pairs(gui.gfx.objectiveEnemies) do
        if(enemy._id ~= objectiveEnemy._id) then
            table.insert(filteredEnemies, objectiveEnemy)
        end
    end
    gui.gfx.objectiveEnemies = filteredEnemies
    enemy.gfx.guideSprite:removeSelf()
end

gui.createHealthbar = function(character, isNotVisible)
    local healthbarCont = display.newGroup()
    healthbarCont.xScale = .5
    healthbarCont.yScale = .5
    game.guiCont:insert(healthbarCont)
    healthbarCont:toBack()

    local healthbarBg = display.newImageRect(healthbarCont, "assets/ui/healthbar-bg.png", 24, 24)
    healthbarBg.alpha = .35
    local healthbarHealth = display.newImageRect(healthbarCont, "assets/ui/healthbar-health.png", 24, 24)

    character.healthbarCont = healthbarCont
    character.healthbarHealth = healthbarHealth

    if(isNotVisible == true) then
        character.healthbarCont.alpha = 0
    end

    gui.updateHealthbarPosition(character)
    gui.updateHealthbarSize(character)
end

gui.updateHealthbarPosition = function(character)
    local healthbarRadius = 6
    local characterPosition = character.getPosition()
    local guiPositionX, guiPositionY = game.enemiesCont:localToContent(characterPosition.x, characterPosition.y)
    local centerPosition = {x = guiPositionX - display.screenOriginX, y = guiPositionY - display.screenOriginY}
    local backVector = ssk.math2d.scale(ssk.math2d.angle2Vector(app.fn.deg(character.cont.rotation + 180), true), character.radius + healthbarRadius)
    character.healthbarCont.x = centerPosition.x + backVector.x
    character.healthbarCont.y = centerPosition.y + backVector.y
end

gui.updateHealthbarSize = function(character)
    if(character and character.healthbarHealth) then
        local healthScale = app.fn.mapRange(character.hp, character.maxHp, 0, 1, .001, true)
        character.healthbarHealth.xScale = healthScale
        character.healthbarHealth.yScale = healthScale
    end
end

return gui