local app = require("src.app")
local game = require("src.game")

local consumable = {}

consumable.TYPE_HEAL = "typeHeal"
consumable.TYPE_ARMOR = "typeArmor"
consumable.TYPE_SUPERCHARGE = "typeSupercharge"
consumable.TYPE_INFO = "typeInfo"

consumable.consumables = {}
consumable.consumables[consumable.TYPE_HEAL] = {
    path = "assets/consumables/256x_beaker_red.png"
}
consumable.consumables[consumable.TYPE_ARMOR] = {
    path = "assets/consumables/256x_bottle2_green.png"
}
consumable.consumables[consumable.TYPE_SUPERCHARGE] = {
    path = "assets/consumables/256x_potion_OL_blue.png"
}
consumable.consumables[consumable.TYPE_INFO] = {
    path = "assets/consumables/256x_flask_glass.png"
}

consumable.create = function(position, consumableType, amount, options)
    if(consumableType == consumable.TYPE_INFO and app.state.enemyInfos[options.enemy] == true) then return false end

    local consumableImg = display.newImageRect(game.groundCont, consumable.consumables[consumableType].path, 256, 256)
    consumableImg.xScale = game.scale * .5
    consumableImg.yScale = game.scale * .5
    consumableImg.x = position.x
    consumableImg.y = position.y
    consumableImg.alpha = 0
    transition.fadeIn(consumableImg, {time = 250})

    return {
        _inactive = false,
        _deleted= false,
        _id = math.getUID(20),
        type = consumableType,
        consumableSprite = consumableImg,
        radius = 10,
        amount = amount,
        options = options,
    }
end

consumable.consume = function(instance, player)
    local canBeAquired = true
    if(instance.type == consumable.TYPE_HEAL) then
        app.sound.playSFX("healUp")
        player.hp = math.min(player.hp + instance.amount/100*player.maxHp, player.maxHp)
        if(instance.healing) then
            transition.cancel(instance.healing)
            instance.healing = nil
        end
        app.gui.updateHealthbarSize(player)
    elseif(instance.type == consumable.TYPE_ARMOR) then
        if(app.state.getMaxLevel().maxHealth > player.maxHp) then
            app.sound.playSFX("armorUp")
            app.state.getPlayer().hp = app.state.getPlayer().hp + instance.amount
            player.maxHp = player.maxHp + instance.amount
            player.hp = math.min(player.hp + instance.amount, player.maxHp)
            app.gui.updateHealthbarSize(player)
        else
            canBeAquired = false
        end
    elseif(instance.type == consumable.TYPE_SUPERCHARGE) then
        if(not player.isSupercharged) then
            app.sound.playSFX("supercharge")
            app.player.actions.supercharge(player, instance.amount*1000)
        else
            canBeAquired = false
        end
    elseif(instance.type == consumable.TYPE_INFO) then
        app.state.enemyInfos[instance.options.enemy] = true
        app.state.saveData()
        game.stop()
        app.screens.newEnemy.show(function() game.run() end, instance.options.enemy)
    end
    if(canBeAquired == true) then
        instance._inactive = true
        transition.to(instance.consumableSprite, {alpha = 0, time = 500, xScale = game.scale * 1.5, yScale = game.scale * 1.5, onComplete = function()
            instance._deleted = true
        end})
    end
end

return consumable