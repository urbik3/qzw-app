local app = require("src.app")
local json = require("json")

local lang = {}

lang.LANG_CZ = "langCz"
lang.LANG_EN = "langEn"
lang.lang = lang.LANG_CZ

lang.get = function(key)
    return lang[lang.lang][key] and lang[lang.lang][key] or "translation error"
end

lang[lang.LANG_CZ] = {
    continue = "Pokračovat",
    submit = "Zvolit",
    cancel = "Zavřít",
    close = "Zavřít",
    resume = "Pokračovat",
    next = "Další",
    startPlaying = "Začít hrát",

    chapter0 = "Tutoriál",
    chapter1 = "Ranní krvavý deštík",
    chapter2 = "Úder tvrdou skalou",
    chapter3 = "Potopení do ohnivé bouře",
    selectChapter = "Vyber kapitolu",
    selectGame = "Výběr hry",
    selectCharacter = "Vyber postavu",
    characterSoldier = "Mariňák",
    characterMedic = "Medik",
    characterPyro = "Pyro",
    characterSniper = "Sniper",
    characterAssault = "Spec Ops",

    chooseLang = "Vyberte jazyk",
    settingsSound = "Zvukové efekty",
    settingsMusic = "Hudba",
    settingsSoundOn = "Zapnout",
    settingsSoundOff = "Vypnout",
    settings = "Nastavení",

    credits = "Titulky",
    about = "Menu",
    achievements = "Úspěchy",
    achieved = "dosáhnuto",
    notAchieved = "nedosáhnuto",
    enemies = "Nepřátelé",
    show = "Zobrazit",
    by = "od",
    from = "z",
    gameDev = "Herní vývojář",
    writer = "Copywriter",
    graphicAssets = "Grafické podklady",
    gameBackgrounds = "Herní pozadí",
    bloodOtherEffects = "Krvavé a ostatní efekty",
    crosshairs = "Zaměřovače",
    music = "Hudba",
    sounds = "Zvuky",
    mathLib = "Matematická knihovna",
    sha1Lib = "SHA1 knihovna",
    link = "Odkaz",

    selectLevel = "Vyberte úroveň",
    leaderboards = "Tabulka výsledků",
    resetChapter = "Reset kapitoly",
    resetChapterText = "Opravdu chcete vymazat Váš postup kapitolou pro tuto postavu?",
    resetChapterConfirm = "Jsem připraven na další kolo!",
    resetChapterDecline = "Nechci nic mazat.",

    networkError = "Nastala chyba při komunikaci se serverem.\nZkuste to, prosím, trochu později.",
    myResult = "Můj výsledek",
    topResults = "Nejlepší výsledky",
    uploadBestScore = "Nahrát skóre online",
    noDataLeaderboards = "Pro tuto tabulku nejsou žádná data",
    
    chooseUsername = "Zvolte unikátní jméno pro tabulky výsledků.",
    usernameTaken = "Jméno už je obsazené.",
    usernameError = "Jméno nebylo uloženo, nastala chyba.",
    usernameLengthHint = "Jméno musí být dlouhé 2 až 12 písmen.",
    username = "jméno",

    tutorialHealthpack = "Tapnutím nebo najetím hráče na lékárničku ji můžete použít k uzdravení se.",
    tutorialArmorpack = "Tapnutím nebo najetím hráče na kus brnění ho můžete použít ke zvýšení svého vlastního maximálního zdraví.",
    tutorialAiming = "Uviděl jste prvního nepřítele! Tapněte na něj a zničte ho!",
    tutorialInfoConsumable = "Dotkni se této ikony a dozvíš se novou informaci o svých nepřátelých!",
    tutorialFindPortal = "Zničil jste všecky cílové nepřátele, nyní je čas se teleportovat pryč. Musíte najít portál!",
    tutorialPortalClear = "Našli jste portál, ale v blízkosti jsou nějací nepřátele, takže nejde aktivovat. Zničte je!",
    tutorialHowToPlay1 = "Jak se hra ovládá?\nChodíte joystickem. Joystick se objeví, když se dotknete obrazovky na místě, které je prázdné.",
    tutorialHowToPlay2 = "Po straně jsou 2 ovládací tlačítka. Pomocí tlačítek aktivujete svoje schopnosti. Horní je základní střelba, dolní je speciální schopnost.",
    tutorialHowToPlay3 = "Detekoval jste nepřítele! Použijte radar kolem hráče k jeho nalezení!",
    tutorialHowToPlayAssault = "Speciální schopností této postavy je Supersíla, která zvyšuje útok, snižuje vlastní zranění a dává imunitu proti omráčení.",
    tutorialHowToPlaySniper = "Speciální schopností této postavy je ultra silný projektil, který odhodí nepřátele a omráčí je.",
    tutorialHowToPlayMedic = "Speciální schopností této postavy je uzdravení. Udravení probíhá delší dobu a je nutné zůstat mimo nebezpečí, jinak se nedokončí. Tvé brnění navíc snižuje zranění.",
    tutorialHowToPlayPyro = "Speciální schopností této postavy je výstřel z kanónu, který zraní nepřátele v okolí výbuchu. Tvé brnění navíc lehce snižuje zranění.",
    tutorialNewCharacter = "Zpřístupnila se Vám nová postava! Najdete ji ve výběru herních módů.",
    tutorialNewChapter = "Zpřístupnila se Vám další kapitola! Najdete ji ve výběru herních módů.",
    
    scoreUpdated = "Gratulace! Vaše nejvyšší skóre bylo aktualizováno.",
    scoreNotUpdated = "Vaše online skóre nebylo aktualizováno! Chcete to zkusit znova?",
    tryAgain = "Zkusit znova",
    saveLocally = "Uložit skóre lokálně",

    forfeitLevel = "Vzdát úroveň",
    storyKickAss = "Jdu jim nakopat zadky",
    youDied = "Zemřel jsi",
    loadingLevel = "Nahrávání úrovně",

    winScreenTitle = "Vyhrál jsi!",
    winScreenText = "Pořádně jsi zabojoval a pomlátil tu hordu sviní jedna radost!",
    loseScreenTitle = "Byl jsi zničen!",
    loseScreenText = {
        "Bojoval jsi statečně, ale nestačilo to. Horda nepřátelských vojáků tě rozdrtila na maděru!",
        "Tvé tělo bylo rozprášeno střelami z nepřátelských zbraní. Zbyla z tebe jenom loužička krve a zničené brnění.",
        "Myslel sis, že to bude jednoduché? Ne. Nic není jednoduché. A rozhodně ne v tomto bohem zapomenutém světě agresivních vojáků toužících po tvé krvi.",
        "Nepodařilo se ti dokončit misi! Tvou snahu zmařily desítky šmejdů s útočnými puškami.",
        "Zhebl jsi jako nic. Jako kdyby tvá snaha nebyla vůbec k ničemu. Objevují se další a další vojáci a ty upadáš do zapomnění.",
    }
}

lang[lang.LANG_EN] = {
    continue = "Continue",
    submit = "Submit",
    cancel = "Cancel",
    close = "Close",
    resume = "Resume",
    next = "Next",
    startPlaying = "Start playing",

    chapter0 = "Tutorial",
    chapter1 = "Morning blood rain",
    chapter2 = "Hard rock punch",
    chapter3 = "Firestorm dive",
    selectChapter = "Select chapter",
    selectGame = "Select game",
    selectCharacter = "Select character",
    characterSoldier = "Marine",
    characterMedic = "Medic",
    characterPyro = "Pyro",
    characterSniper = "Sniper",
    characterAssault = "Spec Ops",

    chooseLang = "Choose language",
    settingsSound = "Sound effects",
    settingsMusic = "Music",
    settingsSoundOn = "On",
    settingsSoundOff = "Off",
    settings = "Settings",
    
    credits = "Credits",
    about = "Menu",
    achievements = "Achievements",
    achieved = "achieved",
    notAchieved = "not achieved",
    enemies = "Enemies",
    show = "Show",
    by = "by",
    from = "from",
    gameDev = "Game developer",
    writer = "Copywriter",
    graphicAssets = "Graphic assets",
    gameBackgrounds = "Game backgrounds",
    bloodOtherEffects = "Blood & other effects",
    crosshairs = "Crosshairs",
    music = "Music",
    sounds = "Sounds",
    mathLib = "Math library",
    sha1Lib = "SHA1 library",
    link = "Link",

    selectLevel = "Select level",
    leaderboards = "Leaderboards",
    resetChapter = "Reset chapter",
    resetChapterText = "Are you sure you want to delete all chapter progress for this character?",
    resetChapterConfirm = "I'm ready for next run!",
    resetChapterDecline = "Do not reset the chapter.",

    networkError = "An error occured while loading data. Please try again later.",
    myResult = "My result",
    topResults = "Top results",
    uploadBestScore = "Upload best score online",
    noDataLeaderboards = "There is no data for this leaderboard",

    chooseUsername = "Choose a unique username for leaderboards.",
    usernameTaken = "Username already taken.",
    usernameError = "Username not saved, an error occured.",
    usernameLengthHint = "Username must be between 2 and 12 characters",
    username = "username",

    tutorialHealthpack = "By tapping or walking over healthpack you can use it to heal yourself.",
    tutorialArmorpack = "By tapping or walking over piece of armor you can use it to increare your own maximal health.",
    tutorialAiming = "You have sighted your first enemy! Tap on him so you can destroy him!",
    tutorialInfoConsumable = "Touch this icon to learn new information about enemies!",
    tutorialFindPortal = "You destroyed all target enemies, now it's time to teleport out of here. You have to find a portal!",
    tutorialPortalClear = "You have found a portal, but therer are enemies near, so it can't be activated. Destroy them!",
    tutorialHowToPlay1 = "Wondering how to play the game?\nYou walk by dragging a joystick. It appears when you touch the screen on position where there is nothing else.",
    tutorialHowToPlay2 = "There are 2 control buttons on the right side. You use them to activate abilities. The upper one is basic shooting, the lower one activates character special ability.",
    tutorialHowToPlay3 = "You have spotted an enemy! Use the radar around the player to find him!",
    tutorialHowToPlayAssault = "Special ability of this character is Superpower which gives extra damage output, lower incoming damage and gives immunity to stun.",
    tutorialHowToPlaySniper = "Special ability of this character is ultra strong projectile which knocks back enemies and stuns them.",
    tutorialHowToPlayMedic = "Special ability of this character is healing. Remember that you must stay out of danger so your health recovers completely. Your armor also reduces incoming damage.",
    tutorialHowToPlayPyro = "Special ability of this character is cannonball which damages enemies near explosion. Your armor also slightly reduces incoming damage.",
    tutorialNewCharacter = "You have access now to a new character! You can find it in game select screen.",
    tutorialNewChapter = "You have access now to a new chapter! You can find it in game select screen.",

    scoreUpdated = "Congratulations! Your score on leaderboard was updated!",
    scoreNotUpdated = "Your online leaderboards score was not updated! Want to try again?",
    tryAgain = "Try again",
    saveLocally = "Save score locally",

    forfeitLevel = "Forfeit level",
    storyKickAss = "Let's kick some asses",
    youDied = "You died",
    loadingLevel = "Loading level",

    winScreenTitle = "You've won!",
    winScreenText = "You fought bravely and wrecked the horde of swines like there was no tommorow!",
    loseScreenTitle = "You got destroyed!",
    loseScreenText = {
        "You fought bravely but it was not enough. Horde of enemy soldiers smashed you into pulp.",
    }
}

return lang