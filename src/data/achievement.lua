local app = require("src.app")
local json = require("json")
local stateManager = require("src.managers.state")

local achievement = {}

achievement.TYPE_NUMBER = "typeNumber"
achievement.TYPE_CONSTANT = "typeConstant"

achievement.ACHIEVEMENT_BASIC_KILLER = "killedBasic"
achievement.ACHIEVEMENT_REINCARNATION = "reincarnation"

achievement.ACHIEVEMENT_NO_DAMAGE_TAKEN = "noDamageTaken"
achievement.FULL_HEALTH_CASUAL = "fullHealthCasual"
achievement.FULL_HEALTH_HARD = "fullHealthHard"
achievement.NO_DAMAGE_TAKEN_CASUAL = "noDamageTakenCasual"
achievement.NO_DAMAGE_TAKEN_HARD = "noDamageTakenHard"

achievement.levelData = {}

local state = {
    [achievement.ACHIEVEMENT_BASIC_KILLER] = {
        name = {
            [app.data.lang.LANG_CZ] = "Zabiják rekrutů",
            [app.data.lang.LANG_EN] = "Recruit killer",
        },
        value = 0,
        type = achievement.TYPE_NUMBER,
        data = {
            {
                targetValue = 20,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Základní zabiják",
                        text = "Úspěšně jsi zabil 20 základních nepřátel!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Basic killer",
                        text = "You have successfully killed 20 basic enemies!",
                    },
                }
            },
            {
                targetValue = 100,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Pokročilý zabiják",
                        text = "Sešrotoval jsi 100 základních nepřátel!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Advanced killer",
                        text = "You wrecked 100 basic enemies!",
                    },
                }
            },
            {
                targetValue = 1000,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Ostřílený zabiják",
                        text = "Rozšlápl jsi jak mouchy už 1000 základních nepřátel!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Seasoned killer",
                        text = "You stomped on 1000 basic enemies!",
                    },
                }
            },
            {
                targetValue = 10000,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Ultra zabiják",
                        text = "10 000 základních nepřátel padlo tvou rukou!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Ultra killer",
                        text = "10 000 basic enemies fell down by your hand!",
                    },
                }
            },
        }
    },
    [achievement.ACHIEVEMENT_REINCARNATION] = {
        name = {
            [app.data.lang.LANG_CZ] = "Reinkarnace",
            [app.data.lang.LANG_EN] = "Reincarnation",
        },
        value = 0,
        type = achievement.TYPE_NUMBER,
        data = {
            {
                targetValue = 25,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Reinkarnační začátečník",
                        text = "Máš za sebou prvních 25 reinkarnací.",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Reincarnation junior",
                        text = "You have achieved your first 25 reincarnations.",
                    },
                }
            },
            {
                targetValue = 100,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Reinkarnační otužilec",
                        text = "Máš za sebou 100 reinkarnací.",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Reincarnation hardy",
                        text = "You have achieved 100 reincarnations.",
                    },
                }
            },
            {
                targetValue = 250,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Reinkarnační profík",
                        text = "Máš za sebou 250 reinkarnací.",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Reincarnation profesional",
                        text = "You have achieved 250 reincarnations.",
                    },
                }
            },
            {
                targetValue = 1000,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Reinkarnační fanatik",
                        text = "Není těch tvých smrtí trochu moc (1000)? Nemůžeš přeci pořád umírat. To ti z toho jebne.",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Reincarnation fanatic",
                        text = "Ain't this number of deaths too much (1000)? You can't just die this much, you'll get crazy.",
                    },
                }
            },
            {
                targetValue = 10000,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Dosáhl jsi nesmrtelnosti",
                        text = "10 000 smrtí a reinkarnací. To nemůže být dobré pro tvou psychiku!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "You have just achieved immortality",
                        text = "10 000 deaths and reincarnations is not good for your psyche!",
                    },
                }
            },
        }
    },
    [achievement.ACHIEVEMENT_NO_DAMAGE_TAKEN] = {
        name = {
            [app.data.lang.LANG_CZ] = "Bez škrábnutí",
            [app.data.lang.LANG_EN] = "Without a scratch",
        },
        value = {},
        type = achievement.TYPE_CONSTANT,
        data = {
            {
                targetValue = achievement.FULL_HEALTH_CASUAL,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Skvělý uhýbač",
                        text = "Povedlo se ti dohrát standartní level s plným životem!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Great dodger",
                        text = "You managed to win a standart level with full life!",
                    },
                }
            },
            {
                targetValue = achievement.FULL_HEALTH_HARD,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Mistr uhýbání",
                        text = "Povedlo se ti dohrát těžký level s plným životem!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Master of dodge",
                        text = "You managed to win a hard level with full life!",
                    },
                }
            },
            {
                targetValue = achievement.NO_DAMAGE_TAKEN_CASUAL,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Velmistr uhýbání",
                        text = "Povedlo se ti dohrát standartní level bez zranění!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Grandmaster of dodge",
                        text = "You managed to win a standart level without taking damage!",
                    },
                }
            },
            {
                targetValue = achievement.NO_DAMAGE_TAKEN_HARD,
                text = {
                    [app.data.lang.LANG_CZ] = {
                        header = "Arciuhýbač",
                        text = "Povedlo se ti dohrát těžký level bez zranění!",
                    },
                    [app.data.lang.LANG_EN] = {
                        header = "Archdodger",
                        text = "You managed to win a hard level without taking damage!",
                    },
                }
            },
        }
    }
}
achievement.state = state

achievement.logEvent = function(type, value)
    local data = state[type]
    if(data) then
        if(data.type == achievement.TYPE_NUMBER) then
            local newValue = data.value + value
            for i, achievementLevel in pairs(data.data) do
                if(achievementLevel.targetValue > data.value and achievementLevel.targetValue <= newValue) then
                    app.screens.achievements.showNewAchievement(achievementLevel.text[app.state.lang])
                end
            end
            data.value = newValue
        elseif(data.type == achievement.TYPE_CONSTANT) then
            local hasBeenAquired = false
            for i, val in pairs(data.value) do
                if(val == value) then
                    hasBeenAquired = true
                    break
                end
            end
            if(not hasBeenAquired) then
                table.insert(data.value, value)

                for i, achievementLevel in pairs(data.data) do
                    if(achievementLevel.targetValue == value) then
                        app.screens.achievements.showNewAchievement(achievementLevel.text[app.state.lang])
                        break
                    end
                end
            end
        end
        stateManager.saveData()
    end
end

achievement.getDataForSave = function()
    local data = {}
    for name, item in pairs(state) do
        data[name] = item.value
    end
    return data
end

achievement.loadDataFromSave = function(data)
    if(data) then
        for name, item in pairs(data) do
            state[name].value = item
        end
    end
end

return achievement