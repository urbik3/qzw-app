local app = require("src.app")
local state = require("src.managers.state")

local story = {}

story.PLACEMENT_DEATH = "placementDeath"

story.data = {
    [story.PLACEMENT_DEATH] = {
        {
            [app.data.lang.LANG_CZ] = {
                text = "Aaaa, jau! Jaksvině jakvždycky, sakra ... sprostě jsem zaklel a když největší bolest pomalu ustoupila pomalu se zamyslel. Aspoň těch pár vteřin než se na mě zase vrhnou mám.\nBylo to opravdu tak? Je to zvláštní, ale přišlo mi, že to teďka a nebo možná i minule bylo jakoby nedotáhnuté. Jakoby tý svinico řídí tenhle koloběh někdo zapomněl promazat kolečka. Nebo jakoby jí to začalo nudit a začala to flákat. Mě už to totiž pořádně otupělo. Prošel jsem tím už desetkrát? Nebo stokrát? Nebo tisíckrát? Ale co sejde na přesném počtu.",
            },
            [app.data.lang.LANG_EN] = {
                text = "Aaaa, jau! Fucking shitstorm, fuck! Im reincarnated again. One must be really happy 'bout that.",
            },
        },
        {
            [app.data.lang.LANG_CZ] = {
                text = "Pokaždý je to jiný hnusný masitý tělo, a jen snad ten rozum teče z jednoho do druhýho. Možná pokaždý něco ztrácim. Nebo možná pokaždý něco ztrácí ta svině co za to všechno může. Aspoň teďka naposled mi to tak přišlo. Jakoby v poslední sekundě střihnutí tý pupeční šňůry něco zablesklo skrz toho světa za. No a nebo mi už konečně začíná hrabat.\nNení čas to řešit, zase slyšim a vidim ty svině jak jdou. Jako lidský drony na porážku. Jen zjistit jestli moji nebo jejich.",
            },
            [app.data.lang.LANG_EN] = {
                text = "Everytime it's another ghastly fleshy body and only the reason flows from one to another.",
            },
        }
    },
    [state.CHARACTER_BASIC] = {
        [state.CHAPTER_CASUAL] = {
            [1] = {
                [app.data.lang.LANG_CZ] = {
                    header = "Úvod",
                    text = "Říká se že tenhle svět je jen iluze, že je to Samsára, past do které se pořád rodíme. Sakra. No a tak jsem tu skončil zase. Úplně tomu nerozumím, ale nehledě na to co dělám, a to i když už s tím vším skončím, tak se tu znovu a znovu objevim. Už si přesně nepamatuji jak jsem skončil minule. Bylo to bolestivé? Nebo jsem našel nějakou zkratku a tentokrát to tak zlé nebylo? Nevím. Ale asi to bolelo jako vždycky jako svině. Aspoň to by vysvětlovalo tu ostrou bolest co při každým zrození nastane. Aspoň mě to vždycky dostane do ráže to těm parchantům pořádně nandat.",
                },
                [app.data.lang.LANG_EN] = {
                    header = "Introduction",
                    text = "It is said that this world is just an illusion, that it is Samsara, a trap in which we are born to.",
                },
            },
            [2] = {
                [app.data.lang.LANG_CZ] = {
                    header = "Život na hovno",
                    text = "Bzzzt. Jau! Jaktysviň! Bzzzzt. Ztráta vědomí. A zase narození. Šťastný a veselý. Co to sakra bylo? Bolelo to pořádně a přitom tak nějak jinak. Divně. No jo, tělesně. Tim to bylo. Ale co to je za blbost? Já měl tělo? I tam co jsem byl zheblý? Hmmm nad tim se teda budu muset fakt zamyslet. Co jsem tak strašlivě zkurvenýho musel udělat, že jsem v týhle Samsárový smyčce? Nebo že bych to nebyl já co za to může, ale celý to posraný lidstvo? Vlastně si už ani nepamatuju jak dávno jsem viděl nějakýho člověka v tomhle pekle. Uchopit zbraň, nabít, zatnout zuby. Už dou.",
                }
            },
        }
    }
}

story.getStartLevel = function(character, chapter, level)
    local data = story.data[character] and story.data[character][chapter] and story.data[character][chapter][level] and story.data[character][chapter][level][app.state.lang]
    return data and data or nil
end

story.getDeath = function()
    return story.data[story.PLACEMENT_DEATH][math.random(1, #story.data[story.PLACEMENT_DEATH])][app.state.lang]
end

return story