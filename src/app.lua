local json = require("json")

local app = {
    time = {
        gameStartTime = 0,
    }
}

app.time.gameStart = function()
   app.time.gameStartTime = system.getTimer()
end

return app