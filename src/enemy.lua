local app = require("src.app")
local game = require("src.game")
local json = require("json")
local random = require("lib.random")

local enemy = {
    gfx = {}
}

enemy.STATUS_IDLE = "status_idle"
enemy.STATUS_NOTICED = "status_noticed"
enemy.STATUS_FOLLOW = "status_follow"
enemy.STATUS_ATTACK = "status_attack"

enemy.ENEMY_EXPLOSIVE = "enemyExplosive"
enemy.ENEMY_BASIC = "enemyBasic"
enemy.ENEMY_SOLDIER = "enemySoldier"
enemy.ENEMY_SCOUT = "enemyScout"
enemy.ENEMY_STEALTH = "enemyStealth"
enemy.ENEMY_CANNONEER = "enemyCannoneer"
enemy.ENEMY_MEDIC = "enemyMedic"
enemy.ENEMY_SNIPER = "enemySniper"
enemy.ENEMY_NEBULIZER = "enemyNebulizer"
enemy.ENEMY_HEAVY = "enemyHeavy"
enemy.ENEMY_MUTANT = "enemyMutant"

enemy.init = function()
    enemy.gfx.bloodEffect = graphics.newImageSheet("assets/effects/bloodEffect6.png", {
        width = 256,
        height = 256,
        numFrames = 4*8
    })
    enemy.gfx.portalEffect = graphics.newImageSheet("assets/effects/03.png", {
        width = 128,
        height = 128,
        numFrames = 5*5
    })
    enemy.gfx.bloodstains = {
        {
            path = "assets/bloodstains/bloodstain1.png",
            anchorX = .4,
            anchorY = .4
        },
        {
            path = "assets/bloodstains/bloodstain5.png",
            anchorX = .5,
            anchorY = .5
        },
        {
            path = "assets/bloodstains/bloodstain9.png",
            anchorX = .55,
            anchorY = .2
        },
        {
            path = "assets/bloodstains/bloodstain10.png",
            anchorX = .55,
            anchorY = .5
        },
    }

    -- ENEMIES
    enemy.enemies = {
        [enemy.ENEMY_BASIC] = {
            gfx = {
                imagePath = "assets/enemies/enemy1.png",
                imageWidth = 101,
                imageHeight = 125,
                shotScale = .5,
            },
            radius = 9,
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = 0,
                [app.consumable.TYPE_HEAL] = .1,
                [app.consumable.TYPE_ARMOR] = .25,
            },
            ai = {},
            movement = {},
        },
        [enemy.ENEMY_SOLDIER] = {
            gfx = {
                imagePath = "assets/enemies/enemy5.png",
                imageWidth = 157,
                imageHeight = 229,
                initialShotDegrees = 5,
                initialShotPosition = 40,
            },
            hp = 25,
            maxHp = 25,
            anchorY = .65,
            radius = 11,
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = 0,
                [app.consumable.TYPE_HEAL] = .25,
                [app.consumable.TYPE_ARMOR] = .1,
            },
            ai = {
                battleRoarDistance = 100,
                speed = 3,
            },
            movement = {},
        },
        [enemy.ENEMY_SCOUT] = {
            gfx = {
                imagePath = "assets/enemies/enemy3.png",
                imageWidth = 180,
                imageHeight = 223,
                initialShotDegrees = -5,
                initialShotPosition = 50,
            },
            hp = 10,
            maxHp = 10,
            radius = 11,
            anchorY = .65,
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = 0,
                [app.consumable.TYPE_HEAL] = .1,
                [app.consumable.TYPE_ARMOR] = .1,
            },
            ai = {
                battleRoarDistance = 150,
                shootDelay = 500,
                fieldOfView = 45,
            },
            movement = {
                circularSpeed = 1/500,
                circularRadius = 250,
            },
            shotType = "shotLaser",
            onUpdate = enemy.updateScout
        },
        [enemy.ENEMY_MEDIC] = {
            gfx = {
                imagePath = "assets/enemies/enemy8.png",
                imageWidth = 160,
                imageHeight = 251,
                initialShotDegrees = 0,
                initialShotPosition = 80,
                shotScale = 1,
            },
            hp = 20,
            maxHp = 20,
            radius = 13,
            anchorY = .8,
            shotType = "shotMedic",
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = 0,
                [app.consumable.TYPE_HEAL] = .25,
                [app.consumable.TYPE_ARMOR] = .1,
            },
            ai = {
                battleRoarDistance = 50,
                noticeDistance = 250,
                shootDelay = 500,
                maxRotationSpeed = 2,
                rotationAcc = .15,
            },
            movement = {},
            onUpdate = enemy.updateMedic,
        },
        [enemy.ENEMY_STEALTH] = {
            gfx = {
                imagePath = "assets/enemies/enemy9.png",
                imageWidth = 143,
                imageHeight = 264,
                initialShotDegrees = 5,
                initialShotPosition = 50,
            },
            hp = 15,
            maxHp = 15,
            radius = 11,
            anchorY = .75,
            isStealth = true,
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = 0,
                [app.consumable.TYPE_HEAL] = .25,
                [app.consumable.TYPE_ARMOR] = .1,
            },
            ai = {
                battleRoarDistance = 100,
                noticeDistance = 110,
                idealDistance = 100,
                shootDelay = 1500,
            },
            movement = {},
            onUpdate = enemy.updateStealth,
        },
        [enemy.ENEMY_MUTANT] = {
            gfx = {
                imagePath = "assets/enemies/enemy6.png",
                imageWidth = 179,
                imageHeight = 256,
                initialShotDegrees = -5,
                initialShotPosition = 50,
            },
            hp = 250,
            maxHp = 250,
            radius = 15,
            shotType = "shotMutant",
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = .7,
                [app.consumable.TYPE_HEAL] = .05,
                [app.consumable.TYPE_ARMOR] = .05,
            },
            ai = {
                battleRoarDistance = 300,
                voiceRoar = true,
                shootDelay = 1000,
                shootDelayVariation = 2500,
                speed = 3,
            },
            movement = {},
        },
        [enemy.ENEMY_HEAVY] = {
            gfx = {
                imagePath = "assets/enemies/enemy2.png",
                imageWidth = 179,
                imageHeight = 250,
                initialShotDegrees = -5,
                initialShotPosition = 50,
            },
            hp = 100,
            maxHp = 100,
            radius = 15,
            shotType = "shotHeavy",
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = .35,
                [app.consumable.TYPE_HEAL] = .05,
                [app.consumable.TYPE_ARMOR] = .05,
            },
            ai = {
                battleRoarDistance = 300,
                voiceRoar = true,
                shootDelay = 1000,
                shootDelayVariation = 3000,
            },
            movement = {},
        },
        [enemy.ENEMY_SNIPER] = {
            gfx = {
                imagePath = "assets/enemies/enemy4.png",
                imageWidth = 135,
                imageHeight = 256,
                initialShotDegrees = -5,
                initialShotPosition = 50,
            },
            hp = 15,
            maxHp = 15,
            radius = 11,
            shotType = "shotSniper",
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = .2,
                [app.consumable.TYPE_HEAL] = .1,
                [app.consumable.TYPE_ARMOR] = .1,
            },
            ai = {
                battleRoarDistance = 50,
                noticeDistance = 400,
                stopFollowingDistance = 400,
                shootDelay = 5000,
                fieldOfView = 45,
                maxRotationSpeed = 3,
                rotationAcc = .25,
            },
            movement = {
                rotationFix = false
            },
            onUpdate = enemy.updateSniper,
        },
        [enemy.ENEMY_CANNONEER] = {
            gfx = {
                imagePath = "assets/enemies/enemy7.png",
                imageWidth = 260,
                imageHeight = 364,
                initialShotDegrees = 5,
                initialShotPosition = 60,
            },
            hp = 30,
            maxHp = 30,
            radius = 20,
            anchorY = .65,
            shotType = "shotCannoneer",
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = .2,
                [app.consumable.TYPE_HEAL] = .1,
                [app.consumable.TYPE_ARMOR] = .1,
            },
            ai = {
                battleRoarDistance = 100,
                noticeDistance = 250,
                idealDistance = 350,
                stopFollowingDistance = 400,
                shootDelay = 2500,
            },
            movement = {},
        },
        [enemy.ENEMY_NEBULIZER] = {
            gfx = {
                imagePath = "assets/enemies/enemy10.png",
                imageWidth = 203,
                imageHeight = 279,
                initialShotDegrees = 5,
                initialShotPosition = 60,
            },
            hp = 20,
            maxHp = 20,
            radius = 12,
            anchorY = .65,
            shotType = "shotNebulizer",
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = .2,
                [app.consumable.TYPE_HEAL] = .1,
                [app.consumable.TYPE_ARMOR] = .1,
            },
            ai = {
                battleRoarDistance = 50,
                noticeDistance = 250,
                stopFollowingDistance = 400,
                shootDelay = 4000,
                shootingCharge = 2000,
                shootingVolley = 500,
            },
            movement = {},
        },
        [enemy.ENEMY_EXPLOSIVE] = {
            gfx = {
                imagePath = "assets/objects/explosive.png",
                imageWidth = 217,
                imageHeight = 214,
                scale = .75,
            },
            hp = 5,
            maxHp = 5,
            radius = 12,
            anchorY = .5,
            isMetal = true,
            hasHealthbar = false,
            drops = {
                [app.consumable.TYPE_SUPERCHARGE] = 0,
                [app.consumable.TYPE_HEAL] = 0,
                [app.consumable.TYPE_ARMOR] = 0,
            },
            ai = {
                isExplosive = true,
            },
            movement = {},
            onUpdate = function()end,
        },
    }
    
    -- HARD ENEMIES
    enemy.enemiesHard = {
        [enemy.ENEMY_BASIC] = {
            hp = 15,
            maxHp = 15,
            ai = {
                maxRotationSpeed = 2,
                rotationAcc = .2,
            },
        },
        [enemy.ENEMY_SOLDIER] = {
            hp = 60,
            maxHp = 60,
            ai = {
                battleRoarDistance = 150,
                maxRotationSpeed = 2,
                rotationAcc = .2,
            },
        },
        [enemy.ENEMY_SCOUT] = {
            hp = 30,
            maxHp = 30,
            ai = {
                battleRoarDistance = 200,
                shootDelay = 350,
                fieldOfView = 60,
                maxRotationSpeed = 3,
                rotationAcc = .25,
            },
        },
        [enemy.ENEMY_MEDIC] = {
            hp = 40,
            maxHp = 40,
            ai = {
                battleRoarDistance = 100,
                noticeDistance = 250,
                shootDelay = 250,
                maxRotationSpeed = 5,
                rotationAcc = .4
            },
        },
        [enemy.ENEMY_STEALTH] = {
            hp = 50,
            maxHp = 50,
            isStealth = true,
            ai = {
                battleRoarDistance = 150,
                noticeDistance = 200,
                maxRotationSpeed = 2,
                rotationAcc = .2,
                speed = 2,
            },
        },
        [enemy.ENEMY_MUTANT] = {
            hp = 600,
            maxHp = 600,
            ai = {
                battleRoarDistance = 350,
                voiceRoar = true,
                shootDelay = 500,
                shootDelayVariation = 1500,
            },
        },
        [enemy.ENEMY_HEAVY] = {
            hp = 300,
            maxHp = 300,
            ai = {
                battleRoarDistance = 350,
                voiceRoar = true,
                shootDelay = 500,
                shootDelayVariation = 2000,
            },
        },
        [enemy.ENEMY_SNIPER] = {
            hp = 25,
            maxHp = 25,
            ai = {
                shootDelay = 4000,
                fieldOfView = 60,
            },
        },
        [enemy.ENEMY_CANNONEER] = {
            hp = 50,
            maxHp = 50,
            ai = {
                battleRoarDistance = 150,
                shootDelay = 2000,
                maxRotationSpeed = 2,
                rotationAcc = .2,
            },
        },
        [enemy.ENEMY_NEBULIZER] = {
            hp = 100,
            maxHp = 100,
            ai = {
                noticeDistance = 400,
                stopFollowingDistance = 400,
                idealDistance = 350,
                maxRotationSpeed = 3,
                rotationAcc = .5,
            }
        },
        [enemy.ENEMY_EXPLOSIVE] = {
            ai = {},
        },
    }
end

enemy.create = function(position, enemyType, options)
    local maxHp = 4
    local instance = {
        _id = math.getUID(20),
        _deleted = false,
        name = "enemy",
        type = enemyType,
        isObjective = false,
        radius = 8,
        anchorY = .75,
        anchorX = .5,
        shotType = "shotBasic",
        isStealth = false,
        shotExplosive = false,
        dropsConsumable = options.dropsConsumable,
        hp = maxHp,
        maxHp = maxHp,
        hasHealthbar = true,
        drops = {},
        timers = {},
        gfx = {
            initialShotDegrees = 6,
            initialShotPosition = 20,
            scale = 1,
            shotScale = 1,
        },
        movement = {
            rotation = 0
        },
        ai = {
            status = enemy.STATUS_IDLE,
            shootWhen = false,
            noticeDistance = 200,
            stopFollowingDistance = 400,
            idealDistance = 120,
            battleRoarDistance = 100,
            shootDelay = 1500,
            shootDelayVariation = 0,
            speed = 1,
            fieldOfView = 10,
            maxRotationSpeed = 1,
            rotationAcc = .1,
            isPatrol = options.isPatrol,
            patrolLocation = {},
        },
        onUpdate = enemy.update,
    }
    
    instance.drops = app.fn.mergeTables(instance.drops, enemy.enemies[enemyType].drops, false)
    instance.movement = app.fn.mergeTables(instance.movement, enemy.enemies[enemyType].movement, false)
    instance.gfx = app.fn.mergeTables(instance.gfx, enemy.enemies[enemyType].gfx, false)
    instance.ai = app.fn.mergeTables(instance.ai, enemy.enemies[enemyType].ai, false)
    instance = app.fn.mergeTables(instance, enemy.enemies[enemyType], false)

    if(app.state.actualChapter == app.state.CHAPTER_HARD or app.state.actualChapter == app.state.CHAPTER_ENDLESS) then
        instance.ai = app.fn.mergeTables(instance.ai, enemy.enemiesHard[enemyType].ai, false)
        instance = app.fn.mergeTables(instance, enemy.enemiesHard[enemyType], false)
    end

    if(instance.movement.circularRadius) then
        instance.movement.circularCenter = position
        instance.movement.circularDeg = math.random(0, 360)
    end

    local cont = display.newGroup()
    game.enemiesCont:insert(cont)
    cont.x = position.x
    cont.y = position.y
    cont.rotation = math.random(0, 360)
    instance.cont = cont

    local charSprite = display.newImageRect(cont, instance.gfx.imagePath, instance.gfx.imageWidth, instance.gfx.imageHeight)
    charSprite.xScale = game.scale * instance.gfx.scale
    charSprite.yScale = game.scale * instance.gfx.scale
    charSprite.anchorY = instance.anchorY
    charSprite.anchorX = instance.anchorX
    instance.charSprite = charSprite

    if(instance.isStealth) then
        charSprite.alpha = .2
    end

    if(instance.type == "enemySniper") then
        local laserSprite = display.newImageRect(cont, "assets/shots/laser-aim.png", 943, 76)
        laserSprite.xScale = game.scale
        laserSprite.yScale = game.scale
        laserSprite.anchorX = 1
        laserSprite.rotation = 90
        laserSprite.x = -3
        laserSprite.y = -30
    end

    instance.getPosition = function()
        return {x = cont.x, y = cont.y}
    end
    instance.setPosition = function(pos)
        cont.x = pos.x
        cont.y = pos.y
    end

    -- SPAWNING ENEMY FROM PORTAL
    if(options.spawnedCb) then
        -- SET SCOUT REAL POSITION
        if(instance.type == enemy.ENEMY_SCOUT) then
            instance.movement.circularDeg = instance.movement.circularDeg + instance.movement.circularSpeed
            local circularMovement = app.fn.polarToCartesian(instance.movement.circularRadius, instance.movement.circularDeg)
            instance.cont.x = instance.movement.circularCenter.x + circularMovement.x
            instance.cont.y = instance.movement.circularCenter.y + circularMovement.y
            instance.cont.rotation = app.fn.getAngle(instance.cont, instance.movement.circularCenter) - 90
        end
        
        local portalEffect = display.newSprite(game.groundCont, enemy.gfx.portalEffect, {
            name = "glow",
            start = 1,
            count = 5*5,
            time = 1000,
            loopCount = 5,
        })
        portalEffect.x = instance.getPosition().x
        portalEffect.y = instance.getPosition().y
        portalEffect.xScale = .75
        portalEffect.yScale = .75
        portalEffect:play()
        transition.from(portalEffect, {alpha = 0, time = 500})
        transition.from(cont, {alpha = 0, time = 500})

        portalEffect:addEventListener("sprite", function(event)
            if(event.phase == "ended") then
                transition.to(portalEffect, {alpha = 0, xScale = .5, yScale = .5, time = 500, onComplete = function()
                    if(portalEffect.x) then
                        portalEffect:removeSelf()
                    end
                    portalEffect = nil

                    if(instance.hasHealthbar) then
                        app.gui.createHealthbar(instance, instance.isStealth)
                    end
                    options.spawnedCb()
                end})
            end
        end)
    -- QUICK SPAWN
    elseif(instance.hasHealthbar) then
        app.gui.createHealthbar(instance, instance.isStealth)
    end

    -- local collisionsCircle = display.newCircle(cont, 0, 0, instance.radius)
    -- collisionsCircle.fill = {1, .5, .5, .5}

    return instance
end

enemy.update = function(instance, player, enemies)
    if(instance.movement.stun) then
        return
    end

    if(game.turret and (game.turret.timers.activate or game.turret.active)) then
        instance.ai.status = enemy.STATUS_FOLLOW
        player = game.turret
    end

    -- FOLLOW
    if(instance.ai.status == enemy.STATUS_FOLLOW and not instance.ai.startShooting and not instance.movement.knockback) then
        local distance = ssk.math2d.distanceBetween(instance.getPosition(), player.getPosition())

        if(distance > instance.ai.idealDistance) then
            local direction = app.fn.getAngle(instance.getPosition(), player.getPosition())
            local resultVector = ssk.math2d.scale(ssk.math2d.angle2Vector(direction, true), instance.ai.speed)
            instance.cont.x = instance.cont.x + resultVector.x
            instance.cont.y = instance.cont.y + resultVector.y
        end
        
        if(distance > instance.ai.stopFollowingDistance) then
            instance.ai.status = enemy.STATUS_IDLE
        end
    end

    -- PATROL
    if(instance.ai.isPatrol and instance.ai.status == enemy.STATUS_IDLE and not instance.movement.knockback) then
        if(not instance.ai.patrolLocation[0]) then
            instance.ai.patrolLocation[0] = instance.getPosition()
            instance.ai.patrolLastVisited = 0
        end
        if(not instance.ai.patrolLocation[1]) then
            local position = instance.getPosition()
            local angle
            local angleX = "right"
            if(position.x > app.bg.width - position.x) then
                angleX = "left"
            end
            local angleY = "bottom"
            if(position.y > app.bg.height - position.y) then
                angleY = "top"
            end
            local angleDir = angleX..angleY
            if(angleDir == "righttop") then
                angle = math.random(0, 90)
            elseif(angleDir == "rightbottom") then
                angle = math.random(90, 180)
            elseif(angleDir == "lefttop") then
                angle = math.random(270, 360)
            elseif(angleDir == "leftbottom") then
                angle = math.random(180, 270)
            end

            local distance = random(100, 500)
            local vector = ssk.math2d.scale(ssk.math2d.angle2Vector(angle, true), distance)
            local destination = ssk.math2d.add(instance.getPosition(), vector)
            instance.ai.patrolLocation[1] = destination
        end

        local nextPatrolLocation = 1
        if(instance.ai.patrolLastVisited == 1) then
            nextPatrolLocation = 0
        end

        local position = instance.getPosition()
        local angle = app.fn.getAngle(position, instance.ai.patrolLocation[nextPatrolLocation])
        local vector = ssk.math2d.scale(ssk.math2d.angle2Vector(angle, true), instance.ai.speed)
        local positionNext = ssk.math2d.add(position, vector)
        app.fn.rotateTo(instance, instance.cont, instance.ai.patrolLocation[nextPatrolLocation], {maxRotationSpeed = instance.ai.maxRotationSpeed, rotationAcc = instance.ai.rotationAcc})

        if(math.abs(instance.cont.rotation - angle) < 5) then
            instance.setPosition(positionNext)
            if(ssk.math2d.distanceBetween(positionNext, instance.ai.patrolLocation[nextPatrolLocation]) < 50) then
                instance.ai.patrolLastVisited = nextPatrolLocation
            end
        end
    end

    -- NOTICE & SHOOT
    local distance = ssk.math2d.distanceBetween(instance.getPosition(), player.getPosition())
    if(distance < instance.ai.noticeDistance or instance.ai.status == enemy.STATUS_NOTICED) then
        if(instance.ai.status == enemy.STATUS_IDLE) then
            instance.ai.status = enemy.STATUS_NOTICED
        end
        local angleBetween = app.fn.rotationDiff(instance.cont, player.getPosition())
        if(not instance.ai.startShooting) then
            angleBetween = app.fn.rotateTo(instance, instance.cont, player.getPosition(), {maxRotationSpeed = instance.ai.maxRotationSpeed, rotationAcc = instance.ai.rotationAcc})
        end
        if(instance.ai.shootWhen and instance.ai.shootWhen <= game.getTime()) then
            local shotOptions = {
                initialDegrees = instance.gfx.initialShotDegrees,
                initialPosition = instance.gfx.initialShotPosition,
                scale = instance.gfx.shotScale
            }
            if(app.shot.types[instance.shotType].isExplosive == true) then
                shotOptions.explodeDistance = math.max(distance, app.shot.types[instance.shotType].blastRadius + instance.radius)
            end
            if(instance.type == enemy.ENEMY_NEBULIZER) then
                if(not instance.ai.startShooting and instance.ai.shootDelay) then
                    instance.ai.startShooting = game.getTime()
                else
                    local timeElapsed = game.getTime() - instance.ai.startShooting
                    if(timeElapsed > instance.ai.shootingCharge and timeElapsed < (instance.ai.shootingCharge + instance.ai.shootingVolley)) then
                        shotOptions = app.fn.mergeTables(shotOptions, {shotAngle = math.random(-10, 10)})
                        local shot = app.shot.create(instance, instance.shotType, shotOptions)
                        table.insert(game.shots, shot)
                    elseif(timeElapsed > (instance.ai.shootingCharge + instance.ai.shootingVolley)) then
                        instance.ai.startShooting = false
                        instance.ai.shootWhen = false
                    else
                        instance.ai.startingPosition = {}
                        instance.ai.startingPosition.x = instance.cont.x
                        instance.ai.startingPosition.y = instance.cont.y
                        instance.cont.x = instance.ai.startingPosition.x + math.random(-1, 1)
                        instance.cont.y = instance.ai.startingPosition.y + math.random(-1, 1)
                    end
                end
            else
                if(instance.type == enemy.ENEMY_STEALTH) then
                    local shot = app.shot.create(instance, instance.shotType, app.fn.mergeTables(shotOptions, {shotAngle = 15}))
                    table.insert(game.shots, shot)
                    local shot = app.shot.create(instance, instance.shotType, app.fn.mergeTables(shotOptions, {shotAngle = -15}))
                    table.insert(game.shots, shot)
                    local shot = app.shot.create(instance, instance.shotType, shotOptions)
                    table.insert(game.shots, shot)
                else
                    local shot = app.shot.create(instance, instance.shotType, shotOptions)
                    table.insert(game.shots, shot)
                end
                if(instance.isStealth and instance.healthbarCont.alpha <= .1) then
                    transition.to(instance.healthbarCont, {time = 500, alpha = 1})
                    transition.to(instance.charSprite, {time = 500, alpha = 1})
                end
                instance.ai.shootWhen = false
            end
        end
        if(angleBetween < instance.ai.fieldOfView and instance.ai.shootWhen == false) then
            instance.ai.shootWhen = game.getTime() + instance.ai.shootDelay + math.random(0, instance.ai.shootDelayVariation)
            instance.ai.status = enemy.STATUS_FOLLOW
        end
    else
        instance.ai.shootWhen = false
        if(distance < instance.ai.stopFollowingDistance) then
            app.fn.rotateTo(instance, instance.cont, player.getPosition(), {maxRotationSpeed = instance.ai.maxRotationSpeed, rotationAcc = instance.ai.rotationAcc})
        end
    end

    enemy.collision(instance, enemies)
end

enemy.updateScout = function(instance, player, enemies)
    if(instance.movement.stun) then
        return
    end

    -- IDLE SCOUTING
    if(instance.ai.status == enemy.STATUS_IDLE and not instance.movement.knockback) then
        instance.movement.circularDeg = instance.movement.circularDeg + instance.movement.circularSpeed
        local circularMovement = app.fn.polarToCartesian(instance.movement.circularRadius, instance.movement.circularDeg)
        instance.cont.x = instance.movement.circularCenter.x + circularMovement.x
        instance.cont.y = instance.movement.circularCenter.y + circularMovement.y
        instance.cont.rotation = app.fn.getAngle(instance.cont, instance.movement.circularCenter) - 90
    end

    -- NOTICE & SHOOT
    if(ssk.math2d.distanceBetween(instance.getPosition(), player.getPosition()) < instance.ai.noticeDistance or
        instance.ai.status == enemy.STATUS_NOTICED or instance.ai.status == enemy.STATUS_ATTACK) then

        local angleBetween = app.fn.rotateTo(instance, instance.cont, player.getPosition(), {maxRotationSpeed = instance.ai.maxRotationSpeed, rotationAcc = instance.ai.rotationAcc})
        if(instance.ai.shootWhen and instance.ai.shootWhen <= game.getTime()) then
            local shot = app.shot.create(instance, instance.shotType, {
                initialDegrees = 5,
                initialPosition = 40,
                scale = instance.gfx.shotScale
            })
            table.insert(game.shots, shot)
            local shot = app.shot.create(instance, instance.shotType, {
                initialDegrees = -5,
                initialPosition = 35,
                scale = instance.gfx.shotScale
            })
            table.insert(game.shots, shot)
            instance.ai.shootWhen = false
        end
        if(angleBetween < instance.ai.fieldOfView and instance.ai.shootWhen == false) then
            instance.ai.shootWhen = game.getTime() + instance.ai.shootDelay
            instance.ai.status = enemy.STATUS_ATTACK
        end
    else
        instance.ai.shootWhen = false
    end

    if(ssk.math2d.distanceBetween(instance.getPosition(), player.getPosition()) > instance.ai.stopFollowingDistance) then
        instance.ai.status = enemy.STATUS_IDLE
    end

    enemy.collision(instance, enemies)
end

enemy.updateSniper = function(instance, player, enemies)
    if(instance.movement.stun) then
        return
    end

    -- IDLE AIMING
    if(instance.ai.status == enemy.STATUS_IDLE) then
        if(instance.movement.rotationFix == false) then
            instance.movement.rotationFix = instance.cont.rotation
        end
        instance.cont.rotation = instance.movement.rotationFix + math.sin(game.getTime()/1000) * 45
    else
        instance.movement.rotationFix = false
    end

    -- NOTICE & SHOOT
    if(ssk.math2d.distanceBetween(instance.getPosition(), player.getPosition()) < instance.ai.noticeDistance or
        instance.ai.status == enemy.STATUS_NOTICED or instance.ai.status == enemy.STATUS_ATTACK) then

        local angleBetween = app.fn.rotateTo(instance, instance.cont, player.getPosition(), {maxRotationSpeed = instance.ai.maxRotationSpeed, rotationAcc = instance.ai.rotationAcc})
        if(instance.ai.shootWhen and instance.ai.shootWhen <= game.getTime()) then
            local shot = app.shot.create(instance, instance.shotType, {
                initialDegrees = instance.gfx.initialShotDegrees,
                initialPosition = instance.gfx.initialShotPosition,
                scale = instance.gfx.shotScale
            })
            table.insert(game.shots, shot)
            instance.ai.shootWhen = false
        end
        if(angleBetween < instance.ai.fieldOfView and instance.ai.shootWhen == false) then
            instance.ai.shootWhen = game.getTime() + instance.ai.shootDelay
            instance.ai.status = enemy.STATUS_ATTACK
        end
    else
        instance.ai.shootWhen = false
    end

    if(ssk.math2d.distanceBetween(instance.getPosition(), player.getPosition()) > instance.ai.stopFollowingDistance) then
        instance.ai.status = enemy.STATUS_IDLE
    end

    enemy.collision(instance, enemies)
end

enemy.updateMedic = function(instance, player, enemies)
    if(instance.movement.stun) then
        return
    end

    local shortestDistance = {distance = 999999, enemy = nil}
    for k, singleEnemy in pairs(enemies) do
        local distance = ssk.math2d.distanceBetween(singleEnemy.getPosition(), instance.getPosition())
        if(distance < shortestDistance.distance and singleEnemy._id ~= instance._id) then
            shortestDistance.distance = distance
            shortestDistance.enemy = singleEnemy
        end
        if(distance < instance.ai.noticeDistance) then
            if(singleEnemy.hp < singleEnemy.maxHp and singleEnemy._id ~= instance._id and singleEnemy.type ~= enemy.ENEMY_MEDIC) then
                local angleBetween = app.fn.rotateTo(instance, instance.cont, singleEnemy.getPosition(), {maxRotationSpeed = instance.ai.maxRotationSpeed, rotationAcc = instance.ai.rotationAcc})
                if(instance.ai.shootWhen and instance.ai.shootWhen <= game.getTime()) then
                    local shot = app.shot.create(instance, instance.shotType, {
                        initialDegrees = instance.gfx.initialShotDegrees,
                        initialPosition = instance.gfx.initialShotPosition,
                        scale = instance.gfx.shotScale
                    })
                    table.insert(game.shots, shot)
                    instance.ai.shootWhen = false
                end
                if(angleBetween < instance.ai.fieldOfView and instance.ai.shootWhen == false) then
                    instance.ai.shootWhen = game.getTime() + instance.ai.shootDelay
                end
            end
        end
    end

    if(shortestDistance.distance > instance.ai.noticeDistance and shortestDistance.enemy) then
        local angleBetween = app.fn.rotateTo(instance, instance.cont, shortestDistance.enemy.getPosition(), {maxRotationSpeed = instance.ai.maxRotationSpeed, rotationAcc = instance.ai.rotationAcc})
        if(angleBetween < instance.ai.fieldOfView and not instance.movement.knockback) then
            local direction = app.fn.getAngle(instance.getPosition(), shortestDistance.enemy.getPosition())
            local resultVector = ssk.math2d.scale(ssk.math2d.angle2Vector(direction, true), instance.ai.speed)
            instance.cont.x = instance.cont.x + resultVector.x
            instance.cont.y = instance.cont.y + resultVector.y
        end
    end

    enemy.collision(instance, enemies)
end

enemy.collision = function(instance, characters)
    for k, character in pairs(characters) do
        if(character._id ~= instance._id) then
            local distance = ssk.math2d.distanceBetween(character.getPosition(), instance.getPosition())
            if(distance < instance.radius + character.radius) then
                local angle = app.fn.getAngle(instance.getPosition(), character.getPosition()) - math.pi
                local distanceCorrection = instance.radius + character.radius - distance
                local vectorCorrection = ssk.math2d.scale(ssk.math2d.angle2Vector(angle, true), distanceCorrection)
                local characterPosition = character.getPosition()
                character.setPosition({x = characterPosition.x + vectorCorrection.x, y = characterPosition.y + vectorCorrection.y})
            end
        end
    end
end

enemy.takeDamage = function(instance, shot)
    local damage = shot.damage

    if(instance.isSupercharged) then
        damage = damage/2
    end

    if(instance.armor) then
        damage = damage - instance.armor
    end

    if(instance.healing and instance.removeHealing) then
        instance.removeHealing()
    end

    if(not instance.isMetal) then
        local bloodEffect = display.newSprite(game.groundCont, enemy.gfx.bloodEffect, {
            name = "splash",
            start = 1,
            count = 4*8,
            time = 700,
            loopCount = 1,
        })
        bloodEffect.x = instance.getPosition().x
        bloodEffect.y = instance.getPosition().y
        bloodEffect.xScale = game.scale*1.5
        bloodEffect.yScale = game.scale*1.5
        bloodEffect.rotation = math.random(0, 360)
        bloodEffect:toBack()
        bloodEffect:play()
        bloodEffect:addEventListener("sprite", function(event)
            if(event.phase == "ended") then
                bloodEffect:removeSelf()
                bloodEffect = nil
            end
        end)
        app.sound.playSFX("bloodSplash2")
    end

    -- KNOCKBACK
    if(shot.knockback and not instance.isSupercharged and instance.knockback ~= false) then
        instance.movement.knockback = true
        local pos = instance.getPosition()
        local directionPos
        if(shot.isExplosive) then
            local shotPos = shot.getPosition()
            local vector = {x = shotPos.x - pos.x, y = shotPos.y - pos.y}
            -- DIRECT EXPLOSIVE HIT
            if(app.fn.vectorLength(vector) < 10) then
                shot.stun = 1500
                shot.stunColor = "red"
                shot.damage = shot.damage * 2
                instance.movement.knockback = false
            end
            directionPos = app.fn.normalize(vector)
        else
            directionPos = app.fn.polarToCartesian(1, app.fn.toRadian(shot.cont.rotation + 90))
        end

        if(instance.movement.knockback) then
            local vector = app.fn.multiplyVector(directionPos, shot.knockback)
            local endPos = {x = pos.x + vector.x, y = pos.y + vector.y}

            local stepX = vector.x * game.mspf/1000
            local stepY = vector.y * game.mspf/1000

            local startTime = game.getTime()
            local endTime = startTime + shot.knockbackDuration

            if(instance.timers.knockback) then
                timer.cancel(instance.timers.knockback)
                instance.timers.knockback = nil
            end

            local step
            step = function()
                if(instance._deleted) then
                    return
                end
                local runTime = game.getTime() - startTime
                local stepModifikator = math.max((1-runTime/(shot.knockbackDuration)), 0)

                if(instance.name == "player") then
                    instance.movement.knockbackX = stepX * stepModifikator
                    instance.movement.knockbackY = stepY * stepModifikator
                    app.player.update(instance, game.consumables, game.terrain)
                elseif(instance.cont) then
                    instance.cont.x = instance.getPosition().x - stepX * stepModifikator
                    instance.cont.y = instance.getPosition().y - stepY * stepModifikator
                end

                if(runTime > shot.knockbackDuration) then
                    instance.movement.knockback = false
                    instance.movement.knockbackX = 0
                    instance.movement.knockbackY = 0
                    instance.timers.knockbackTimer = nil
                else
                    instance.timers.knockback = timer.performWithDelay(game.mspf, step)
                end
            end
            step()
        end
    end
    
    -- STUN
    if(shot.stun > 0 and not instance.isSupercharged) then
        local resetStun = function()
            instance.timers.stun = nil
            instance.movement.stun = false
            instance.movement.stunEnds = false
            instance.charSprite.fill.effect = nil
        end
        if(instance.movement.stun == true) then
            if(instance.movement.stunEnds < game.getTime() + shot.stun) then
                timer.cancel(instance.timers.stun)
                instance.timers.stun = timer.performWithDelay(shot.stun, resetStun)
                instance.movement.stunEnds = game.getTime() + shot.stun
            end
        else
            local colorMatrix = {
                1, 0, 0, 0,
                0, 1, 0, 0,
                -0.2, 0.2, 0.1, 1,
                0, 0, 0, 1,
            }
            if(shot.stunColor == "red") then
                colorMatrix = {
                    .1, .1, -.2, 1,
                    0, 1, 0, 0,
                    0, 0, 1, 0,
                    0, 0, 0, 1,
                }
            end
            instance.movement.stun = true
            instance.charSprite.fill.effect = "filter.colorMatrix"
            instance.charSprite.fill.effect.coefficients = colorMatrix
            
            instance.movement.stunEnds = game.getTime() + shot.stun
            instance.timers.stun = timer.performWithDelay(shot.stun, resetStun)
        end
    end

    if(instance.name == "enemy") then
        if(instance.hp == instance.maxHp and instance.ai.voiceRoar) then
            app.sound.playSFX("growl2")
        end

        if(instance.isStealth and instance.healthbarCont.alpha <= .1) then
            transition.to(instance.healthbarCont, {time = 500, alpha = 1})
            transition.to(instance.charSprite, {time = 500, alpha = 1})
        end

        if(instance.ai.status == enemy.STATUS_IDLE) then
            instance.ai.status = enemy.STATUS_NOTICED
        end

        for k, enemy in pairs(game.enemies) do
            if(ssk.math2d.distanceBetween(instance.getPosition(), enemy.getPosition()) < instance.ai.battleRoarDistance and enemy.ai.status == app.enemy.STATUS_IDLE) then
                enemy.ai.status = app.enemy.STATUS_NOTICED
            end
        end
    else
        app.data.achievement.levelData.hasBeenHit = true
    end

    instance.hp = instance.hp - damage
    if(instance.hasHealthbar) then
        app.gui.updateHealthbarSize(instance)
    end
    if(instance.hp <= 0) then
        enemy.destroy(instance)
    end
end

enemy.destroy = function(instance)
    if(not instance.isMetal) then
        local bloodAsset = enemy.gfx.bloodstains[math.random(1, #enemy.gfx.bloodstains)]
        local bloodImg = display.newImageRect(game.groundCont, bloodAsset.path, 256, 256)
        bloodImg.anchorX = bloodAsset.anchorX
        bloodImg.anchorY = bloodAsset.anchorY
        bloodImg.xScale = game.scale*1.5
        bloodImg.yScale = game.scale*1.5
        bloodImg.x = instance.getPosition().x
        bloodImg.y = instance.getPosition().y
        bloodImg.rotation =  math.random(0, 360)
        bloodImg.alpha = 0
        bloodImg:toBack()
        transition.fadeIn(bloodImg, {time = 700})
        app.sound.playSFX("bloodSplash")
    end
    
    if((instance.name == "enemy" or instance.name == "turret") and instance.ai.isExplosive) then
        local shotOptions = {
            initialDegrees = 0,
            initialPosition = 0,
            scale = instance.gfx.shotScale,
            explodeDistance = 0,
        }
        table.insert(game.shots, app.shot.create(instance, "shotExplosive", shotOptions))
    end

    if(instance.healthbarCont) then
        transition.to(instance.healthbarCont, {time = 250, xScale = .01, yScale = .01, alpha = 0, onComplete = function(self)
            if(self.removeSelf and self.x) then
                self:removeSelf()
            end
        end})
    end

    if(instance.name == "enemy") then
        -- REMOVE TIMERS
        for i, singleTimer in pairs(instance.timers) do
            timer.cancel(singleTimer)
        end
        -- REMOVE FROM TURRET TARGET ENEMY
        if(game.turret and game.turret.targetEnemy) then
            if(game.turret.targetEnemy._id == instance._id) then
                game.turret.targetEnemy = nil
            end
        end

        -- REMOVE PERSISTENT LOCK
        if(app.gui.lockOnEnemy and instance._id == app.gui.lockOnEnemy._id) then
            app.gui.destroyLockOn()
            game.playerInstance.lockedOnEnemy = nil
        end

        -- REMOVE ENEMY
        instance._deleted = true
        instance.cont:removeSelf()
        local filteredEnemies = {}
        for k, enemy in pairs(game.enemies) do
            if(enemy._deleted == false) then
                table.insert(filteredEnemies, enemy)
            end
        end
        game.enemies = filteredEnemies

        -- DROP CONSUMABLE
        local dropsSupercharge
        local dropsHeal
        local dropsArmor
        if(
            instance.dropsConsumable == app.consumable.TYPE_ARMOR or
            instance.dropsConsumable == app.consumable.TYPE_HEAL or
            instance.dropsConsumable == app.consumable.TYPE_SUPERCHARGE) then
            if(instance.dropsConsumable == app.consumable.TYPE_ARMOR) then
                dropsArmor = true
            elseif(instance.dropsConsumable == app.consumable.TYPE_HEAL) then
                dropsHeal = true
            elseif(instance.dropsConsumable == app.consumable.TYPE_SUPERCHARGE) then
                dropsSupercharge = true
            end
        else
            dropsSupercharge = instance.drops[app.consumable.TYPE_SUPERCHARGE] > math.random()
            if(not dropsSupercharge) then
                dropsHeal = instance.drops[app.consumable.TYPE_HEAL] > math.random()
                if(not dropsHeal) then
                    dropsArmor = instance.drops[app.consumable.TYPE_ARMOR] > math.random()
                end
            end
        end

        if(dropsArmor) then
            if(app.state.getMaxLevel().maxHealth > game.playerInstance.maxHp) then
                -- local armorAmount = app.state.actualChapter == app.state.CHAPTER_HARD and 5 or 10
                local armorAmount = app.state.actualChapter == app.state.CHAPTER_ENDLESS and 25 or 10
                local consumable = app.consumable.create(instance.getPosition(), app.consumable.TYPE_ARMOR, armorAmount)
                table.insert(game.consumables, consumable)

                -- TUTORIAL
                if(app.state.actualCharacter == app.state.CHARACTER_BASIC and app.game.chapter == app.state.CHAPTER_TUTORIAL and app.tutorial.firstArmorDrop) then
                    app.tutorial.firstArmorDrop = false
                    game.stop()
                    app.tutorial.createPopup(app.data.lang.get("continue"), app.data.lang.get("tutorialArmorpack"), function()
                        app.tutorial.destroyPopup()
                        game.run()
                    end)
                end
            end
        elseif(dropsHeal) then
            local healthAmount = app.state.actualChapter == app.state.CHAPTER_CASUAL and 75 or 50
            local consumable = app.consumable.create(instance.getPosition(), app.consumable.TYPE_HEAL, healthAmount)
            table.insert(game.consumables, consumable)

            -- TUTORIAL
            if(app.state.actualCharacter == app.state.CHARACTER_BASIC and app.game.chapter == app.state.CHAPTER_TUTORIAL and app.tutorial.firstHealthpackDrop) then
                app.tutorial.firstHealthpackDrop = false
                game.stop()
                app.tutorial.createPopup(app.data.lang.get("continue"), app.data.lang.get("tutorialHealthpack"), function()
                    app.tutorial.destroyPopup()
                    game.run()
                end)
            end
        elseif(dropsSupercharge) then
            local timeAmount = 10
            local consumable = app.consumable.create(instance.getPosition(), app.consumable.TYPE_SUPERCHARGE, timeAmount)
            table.insert(game.consumables, consumable)
        end
        
        -- REMOVE OBJECTIVE ENEMY
        if(#game.objectiveEnemies > 0) then
            if(instance.isObjective) then
                app.gui.removeObjectiveEnemy(instance)
                local filteredEnemies = {}
                for k, enemy in pairs(game.objectiveEnemies) do
                    if(enemy._deleted == false) then
                        table.insert(filteredEnemies, enemy)
                    end
                end
                game.objectiveEnemies = filteredEnemies
                if(#game.objectiveEnemies <= 0) then
                    game.objectiveComplete("killEnemies")
                end
            end
        end

        -- ACHIEVEMENTS
        if(instance.type == enemy.ENEMY_BASIC) then
            app.data.achievement.logEvent(app.data.achievement.ACHIEVEMENT_BASIC_KILLER, 1)
        end

    elseif(instance.name == "player") then
        game._lose = true
    elseif(instance.name == "turret") then
        for i, val in pairs(instance.timers) do
            timer.cancel(val)
            instance.timers[i] = nil
        end

        instance.cont:removeSelf()
        instance = nil
        game.turret = nil
    end
end

return enemy