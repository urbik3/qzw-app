local app = require("src.app")
local screen = require("src.managers.screen")
local state = require("src.managers.state")

local gameSelect = {}

gameSelect.gameSelectGroup = nil

gameSelect.selectedCharacter = nil
gameSelect.selectedChapter = nil

gameSelect.BTN_STATE_SELECTED = "btn_selected"
gameSelect.BTN_STATE_NORMAL = "btn_normal"
gameSelect.BTN_STATE_LOCKED = "btn_locked"

gameSelect.createBtn = function(parent, state, charnum)
    local localUiScale = 1.7
    local btnGroup = display.newGroup()
    parent:insert(btnGroup)
    local btn
    if(state == gameSelect.BTN_STATE_SELECTED) then
        btn = display.newImageRect(btnGroup, "assets/ui/btn.png", 132, 132)
    else
        btn = display.newImageRect(btnGroup, "assets/ui/btn-disabled.png", 132, 132)
    end
    btn.xScale = screen.uiScale*localUiScale
    btn.yScale = screen.uiScale*localUiScale
    btn:addEventListener("touch", function(event)
        if(event.phase == "began" and state ~= gameSelect.BTN_STATE_LOCKED) then
            local newCharnum
            if(gameSelect.selectedCharacter == charnum) then newCharnum = nil else newCharnum = charnum end
            gameSelect.selectedCharacter = newCharnum
            gameSelect.selectedChapter = nil
            gameSelect.destroy()
            gameSelect.show()
            return true
        end
    end)

    if(state ~= gameSelect.BTN_STATE_LOCKED) then
        local width
        local height
        if(charnum == 1) then
            width = 157
            height = 223
        elseif(charnum == 2) then
            width = 155
            height = 247
        elseif(charnum == 3) then
            width = 134
            height = 182
        elseif(charnum == 4) then
            width = 175
            height = 229
        elseif(charnum == 5) then
            width = 135
            height = 254
        end
        local icon = display.newImageRect(btnGroup, "assets/characters/character"..charnum..".png", width, height)
        icon.xScale = screen.uiScale * .7
        icon.yScale = screen.uiScale * .7
    else
        local icon = display.newImageRect(btnGroup, "assets/ui/icons/icon-locked.png", 152, 152)
        icon.xScale = screen.uiScale*localUiScale
        icon.yScale = screen.uiScale*localUiScale
        icon.alpha = .25
    end

    return btnGroup
end

gameSelect.charnumToConst = function(charnum)
    local hash = {
        state.CHARACTER_BASIC,
        state.CHARACTER_MEDIC,
        state.CHARACTER_ASSAULT,
        state.CHARACTER_PYRO,
        state.CHARACTER_SNIPER,
    }
    local const
    if(not charnum) then const = nil else const = hash[charnum] end
    return const
end

gameSelect.chapternumToName = function(difficultynum)
    local hash = {
        app.data.lang.get("chapter1"), app.data.lang.get("chapter2"), app.data.lang.get("chapter3")
    }
    hash[0] = app.data.lang.get("chapter0")
    local name = hash[difficultynum]
    if(not name) then name = app.data.lang.get("selectChapter") end
    return name
end

gameSelect.chapternumToConst = function(difficultynum)
    local hash = {
        state.CHAPTER_CASUAL,
        state.CHAPTER_HARD,
        state.CHAPTER_ENDLESS,
    }
    hash[0] = state.CHAPTER_TUTORIAL
    local const
    if(not difficultynum) then const = nil else const = hash[difficultynum] end
    return const
end

gameSelect.destroy = function()
    gameSelect.gameSelectGroup:removeSelf()
    gameSelect.gameSelectGroup = nil
end

gameSelect.show = function()
    local pauseMenuGroup = display.newGroup()
    screen.menuCont:insert(pauseMenuGroup)
    gameSelect.gameSelectGroup = pauseMenuGroup

    local bg = display.newRect(pauseMenuGroup, 0, 0, display.actualContentWidth*2, display.actualContentHeight*2)
    bg.x = display.screenOriginX
    bg.y = display.screenOriginY
    bg.fill = {0, .5}

    local window = display.newImageRect(pauseMenuGroup, "assets/ui/window.png", 1312, 848)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.contentCenterX
    window.y = display.contentCenterY

    local header = display.newImageRect(pauseMenuGroup, "assets/ui/header.png", 478, 112)
    header.xScale = screen.uiScale
    header.yScale = screen.uiScale
    header.x = display.contentCenterX
    header.y = 35

    local headerText = display.newText({parent = pauseMenuGroup, text = app.data.lang.get("selectGame"), font = screen.font, fontSize = 16, align = "center"})
    headerText.x = display.contentCenterX
    headerText.y = 35
    headerText.fill = screen.textColor

    -- CHARACTER SELECTION
    local startX = 90
    local startY = 115
    local marginX = 75

    local state1 = gameSelect.selectedCharacter == 1 and gameSelect.BTN_STATE_SELECTED or gameSelect.BTN_STATE_NORMAL
    state1 = state.progression[state.CHARACTER_BASIC].available == true and state1 or gameSelect.BTN_STATE_LOCKED
    local char1 = gameSelect.createBtn(pauseMenuGroup, state1, 1)
    char1.x = startX
    char1.y = startY

    local state2 = gameSelect.selectedCharacter == 2 and gameSelect.BTN_STATE_SELECTED or gameSelect.BTN_STATE_NORMAL
    state2 = state.progression[state.CHARACTER_MEDIC].available == true and state2 or gameSelect.BTN_STATE_LOCKED
    local char2 = gameSelect.createBtn(pauseMenuGroup, state2, 2)
    char2.x = startX + marginX
    char2.y = startY

    local state4 = gameSelect.selectedCharacter == 4 and gameSelect.BTN_STATE_SELECTED or gameSelect.BTN_STATE_NORMAL
    state4 = state.progression[state.CHARACTER_PYRO].available == true and state4 or gameSelect.BTN_STATE_LOCKED
    local char4 = gameSelect.createBtn(pauseMenuGroup, state4, 4)
    char4.x = startX + marginX*2
    char4.y = startY

    local state5 = gameSelect.selectedCharacter == 5 and gameSelect.BTN_STATE_SELECTED or gameSelect.BTN_STATE_NORMAL
    state5 = state.progression[state.CHARACTER_SNIPER].available == true and state5 or gameSelect.BTN_STATE_LOCKED
    local char5 = gameSelect.createBtn(pauseMenuGroup, state5, 5)
    char5.x = startX + marginX*3
    char5.y = startY

    local state3 = gameSelect.selectedCharacter == 3 and gameSelect.BTN_STATE_SELECTED or gameSelect.BTN_STATE_NORMAL
    state3 = state.progression[state.CHARACTER_ASSAULT].available == true and state3 or gameSelect.BTN_STATE_LOCKED
    local char3 = gameSelect.createBtn(pauseMenuGroup, state3, 3)
    char3.x = startX + marginX*4
    char3.y = startY

    local characterText = gameSelect.charnumToConst(gameSelect.selectedCharacter) == nil and app.data.lang.get("selectCharacter") or app.data.lang.get(app.player.characters[gameSelect.charnumToConst(gameSelect.selectedCharacter)].type)
    local characterName = display.newText({parent = pauseMenuGroup, text = characterText, font = screen.font, fontSize = 16, align = "center"})
    characterName.x = display.contentCenterX
    characterName.y = 67
    characterName.fill = screen.textColor

    -- CHAPTER SELECTION
    local stateTutorial = gameSelect.selectedChapter == 0 and screen.BTN_STATE_NORMAL or screen.BTN_STATE_REDUCED
    stateTutorial = gameSelect.selectedCharacter == nil and screen.BTN_STATE_DISABLED or stateTutorial
    local btnTutorial = screen.createBtn(pauseMenuGroup, "flag", function()
        local newChapternum
        if(gameSelect.selectedChapter == 0) then newChapternum = nil else newChapternum = 0 end
        gameSelect.selectedChapter = newChapternum
        gameSelect.destroy()
        gameSelect.show()
    end, stateTutorial)
    btnTutorial.xScale = screen.uiScale * 1.5
    btnTutorial.yScale = screen.uiScale * 1.5
    btnTutorial.x = startX + marginX*.5
    btnTutorial.y = startY + 100

    local iconCasual = "locked"
    local stateCasual = gameSelect.selectedCharacter == nil
    if(not stateCasual) then
        stateCasual = state.progression[gameSelect.charnumToConst(gameSelect.selectedCharacter)][state.CHAPTER_CASUAL]
        if(stateCasual) then
            iconCasual = "rocket"
            if(gameSelect.selectedChapter == 1) then
                stateCasual = screen.BTN_STATE_NORMAL
            else
                stateCasual = screen.BTN_STATE_REDUCED
            end
        else
            stateCasual = screen.BTN_STATE_DISABLED
        end
    else
        stateCasual = screen.BTN_STATE_DISABLED
    end
    local btnCasual = screen.createBtn(pauseMenuGroup, iconCasual, function()
        local newChapternum
        if(gameSelect.selectedChapter == 1) then newChapternum = nil else newChapternum = 1 end
        gameSelect.selectedChapter = newChapternum
        gameSelect.destroy()
        gameSelect.show()
    end, stateCasual)
    btnCasual.xScale = screen.uiScale * 1.5
    btnCasual.yScale = screen.uiScale * 1.5
    btnCasual.x = startX + marginX*1.5
    btnCasual.y = startY + 100

    local iconHard = "locked"
    local stateHard = gameSelect.selectedCharacter == nil
    if(not stateHard) then
        stateHard = state.progression[gameSelect.charnumToConst(gameSelect.selectedCharacter)][state.CHAPTER_HARD]
        if(stateHard) then
            iconHard = "rockets"
            if(gameSelect.selectedChapter == 2) then
                stateHard = screen.BTN_STATE_NORMAL
            else
                stateHard = screen.BTN_STATE_REDUCED
            end
        else
            stateHard = screen.BTN_STATE_DISABLED
        end
    else
        stateHard = screen.BTN_STATE_DISABLED
    end
    local btnHard = screen.createBtn(pauseMenuGroup, iconHard, function()
        local newChapternum
        if(gameSelect.selectedChapter == 2) then newChapternum = nil else newChapternum = 2 end
        gameSelect.selectedChapter = newChapternum
        gameSelect.destroy()
        gameSelect.show()
    end, stateHard)
    btnHard.xScale = screen.uiScale * 1.5
    btnHard.yScale = screen.uiScale * 1.5
    btnHard.x = startX + marginX*2.5
    btnHard.y = startY + 100

    local iconEndless = "locked"
    local stateEndless = gameSelect.selectedCharacter == nil
    if(not stateEndless) then
        stateEndless = state.progression[gameSelect.charnumToConst(gameSelect.selectedCharacter)][state.CHAPTER_ENDLESS]
        if(stateEndless) then
            iconEndless = "sight"
            if(gameSelect.selectedChapter == 3) then
                stateEndless = screen.BTN_STATE_NORMAL
            else
                stateEndless = screen.BTN_STATE_REDUCED
            end
        else
            stateEndless = screen.BTN_STATE_DISABLED
        end
    else
        stateEndless = screen.BTN_STATE_DISABLED
    end
    local btnEndless = screen.createBtn(pauseMenuGroup, iconEndless, function()
        local newChapternum
        if(gameSelect.selectedChapter == 3) then newChapternum = nil else newChapternum = 3 end
        gameSelect.selectedChapter = newChapternum
        gameSelect.destroy()
        gameSelect.show()
    end, stateEndless)
    btnEndless.xScale = screen.uiScale * 1.5
    btnEndless.yScale = screen.uiScale * 1.5
    btnEndless.x = startX + marginX*3.5
    btnEndless.y = startY + 100

    local chapterName = display.newText({parent = pauseMenuGroup, text = gameSelect.chapternumToName(gameSelect.selectedChapter), font = screen.font, fontSize = 16, align = "center"})
    chapterName.x = display.contentCenterX
    chapterName.y = 167
    chapterName.fill = screen.textColor

    -- BTN BAR
    local btnAccept = screen.createBtn(pauseMenuGroup, "accept", function()
        state.actualCharacter = gameSelect.charnumToConst(gameSelect.selectedCharacter)
        state.actualChapter = gameSelect.chapternumToConst(gameSelect.selectedChapter)
        gameSelect.destroy()
        if(state.actualChapter == state.CHAPTER_TUTORIAL) then
            state.actualLevel = 1
            app.screens.story.checkStory()
        else
            app.screens.levels.showMenu()
        end
    end, (gameSelect.selectedCharacter and gameSelect.selectedChapter) and screen.BTN_STATE_NORMAL or screen.BTN_STATE_DISABLED)
    btnAccept.x = startX + marginX*4
    btnAccept.y = 285
    
    local menuBtn = screen.createBtn(pauseMenuGroup, "chat", function()
        gameSelect.destroy()
        app.screens.menu.show()
    end, screen.BTN_STATE_NORMAL)
    menuBtn.x = startX
    menuBtn.y = 285
    
    local optionsBtn = screen.createBtn(pauseMenuGroup, "settings", function()
        gameSelect.destroy()
        app.screens.settings.show()
    end, screen.BTN_STATE_NORMAL)
    optionsBtn.x = startX + marginX*2
    optionsBtn.y = 285
end

return gameSelect