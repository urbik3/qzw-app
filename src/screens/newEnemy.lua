local app = require("src.app")
local game = require("src.game")
local screen = require("src.managers.screen")

local newEnemyScreen = {}

local enemyTexts = {
    [app.enemy.ENEMY_BASIC] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Rekrut",
            text = "Toto je nejobyčejněnší voják, na kterého narazíte. Je malý, dobře viditelný, lehce zhebne a nedává skoro žádnou ránu. "..
                    "Velmi důležité je, že z něj padá brnění, které zvyšuje tvé maximální zdraví!",
        },
        [app.data.lang.LANG_EN] = {
            header = "Recruit",
            text = "This is the most basic soldier you will encounter. He's small, cleary visible, dies easily and doesn't hit hard. "..
                    "It's very important that he drops armor which you can use to upgrade your maximal health.",
        },
    },
    [app.enemy.ENEMY_SOLDIER] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Voják",
            text = "Tento voják už má větší zkušenosti a je lépe vytrénovaný než obyčejný rekrut. Občas po sobě zanechá lékárničku, kterou se můžeš uzdravit.",
        },
        [app.data.lang.LANG_EN] = {
            header = "Soldier",
            text = "This soldier is more experienced and better trained that a common recruit. He seldom drops first aid kit, which you can use to heal yourself.",
        },
    },
    [app.enemy.ENEMY_CANNONEER] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Kanonýr",
            text = "Tohle je pořádná držka. Když se mu podíváš do očí, vidíš přes helmu jen samé plameny. Nebojí se po tobě vypálit jednu raketu za druhou."..
                    "\nObčas z něj padá Supersíla! Jo, a taky je tím ohněm úplně sežehnutej, takže je mu úplně u prdele, že střílí kolem vlastních vojáků. Prostě hlava vymletá...",
        },
        [app.data.lang.LANG_EN] = {
            header = "Cannoneer",
            text = "This is one big ass freak. If you will live long enough to look him in the eyes, you will see only eternal flames coming out of them. "..
                    "Sometimes he drops Superpower. And he is also quite a dumbass, because he shoot at you even when there are his comrades near you."
        },
    },
    [app.enemy.ENEMY_HEAVY] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Těžkooděnec",
            text = "Velmi dobře obrněný pokročilý voják s omračující zbraní. Jeho válečný ryk je slyšet napříč vzdálenými nepřáteli. Pokud ho chcete udolat, je důležité jeho projektilům uhýbat!",
        },
        [app.data.lang.LANG_EN] = {
            header = "Heavy",
            text = "Very heavily armored advanced soldier with stunning weapon. His battle roar hear enemies in far distance. If you want to defeat him, it is important to dodge his projectiles!",
        },
    },
    [app.enemy.ENEMY_SCOUT] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Průzkumník",
            text = "Tento nepřítel vyniká vysokou rychlostí střelby, rychlostí otáčení a průběžným prozkoumáváním okolí. Může na Vás kdykoliv narazit a začít střílet salvu za salvou.",
        },
        [app.data.lang.LANG_EN] = {
            header = "Scout",
            text = "This enemy excels with high rate of fire, high speed turning and constant scouting of certain area. He can spot you on any occasion and start firing at you from a distance.",
        },
    },
    [app.enemy.ENEMY_STEALTH] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Maskovaný voják",
            text = "Dávejte si pozor, kam šlapete! Mohli byste se také najednou ocitnout v blízkosti několika těchto těžce viditelných nepřátel. Ti zabíjí nepozorovaně a zblízka.",
        },
        [app.data.lang.LANG_EN] = {
            header = "Stealth soldier",
            text = "Be careful where you step! You could suddenly run into a bunch of this difficult to be seen enemies. They kill stealthily and from proximity.",
        },
    },
    [app.enemy.ENEMY_MEDIC] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Vojenský doktor",
            text = "Podívejte se na něj dobře a zapamatujte si ho. Tento nepřítel Vám udělá ze života peklo zvlášť když je schovaný v centru nebezpečných nepřátel."..
                    "Jinak sám neútočí, 'jen' uzdravuje.",
        },
        [app.data.lang.LANG_EN] = {
            header = "Medic",
            text = "Look at him closely and remember him. This foe will give you hell, especially when he's far in the middle of dangerous enemies. He doesn't attack by its own, he 'just' heals.",
        },
    },
    [app.enemy.ENEMY_SNIPER] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Snajpr",
            text = "Stacionární šmejd, co se nikam nevrhá, jen číhá na oběť a z dálky střílí. Jeho zbraň má velkou ráži, takže když se strefí, omráčí Vás to a odhodí daleko dozadu.",
        },
        [app.data.lang.LANG_EN] = {
            header = "Sniper",
            text = "Stacionary creep who never wants to be involved in close range combat, he just waits and fires from a distance. The projectile is quite strong so he stuns you and throws you way back.",
        },
    },
    [app.enemy.ENEMY_NEBULIZER] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Nebulizér",
            text = "Hajzl, co na tebe rád střílí z dálky. Chvíli mu trvá než vystřelí, ale když se mu to konečně povede, tak je to o hubu. Jeho střely tě omráčí na pekelně dlouhou dobu.",
        },
        [app.data.lang.LANG_EN] = {
            header = "Nebulizer",
            text = "Prick who likes to shoot at you from a distance. It takes quite a while until he shoot, but when he does, you don't wanna be around. His projectiles will stun you for quite a while.",
        },
    },
    [app.enemy.ENEMY_MUTANT] = {
        type = "enemy",
        [app.data.lang.LANG_CZ] = {
            header = "Mutant",
            text = "Poplach, poplach! Právě jste detekoval přítomnost Mutanta v jedné z vyšších úrovní. Úhlavní nepřítel lidské rasy. Nasraný ničitel se super silnou zbraní a téměř nekonečným zdravím. To bude o koule.",
        },
        [app.data.lang.LANG_EN] = {
            header = "Mutant",
            text = "(Red) alert, alert! You have just detected Mutant in one of higher levels. Arch enemy of human race. Angry destroyer with super strong weapon and almost endless amount of health. This will be really close.",
        },
    },
}
newEnemyScreen.enemyTexts = enemyTexts

newEnemyScreen.group = nil

newEnemyScreen.hide = function()
    newEnemyScreen.group:removeSelf()
    newEnemyScreen.group = nil
end

newEnemyScreen.show = function(cb, enemy, parent)
    parent = parent and parent or game.pauseMenuCont
    newEnemyScreen.group = display.newGroup()
    parent:insert(newEnemyScreen.group)

    local bg = display.newRect(newEnemyScreen.group, 0, 0, display.actualContentWidth, display.actualContentHeight)
    bg.fill = {0, .5}
    bg.x = display.actualContentWidth/2
    bg.y = display.actualContentHeight/2
    bg:addEventListener("touch", function()
        return true
    end)

    local window = display.newImageRect(newEnemyScreen.group, "assets/ui/window.png", 1312, 648)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.actualContentWidth/2
    window.y = display.actualContentHeight/2

    local header = display.newText({
        x = display.actualContentWidth/2,
        y = display.actualContentHeight/2 - 85,
        width = 1312 * screen.uiScale - 40,
        height = 30,
        parent = newEnemyScreen.group,
        align = "center",
        text = enemyTexts[enemy][app.data.lang.lang].header,
        font = screen.font,
        fontSize = 16,})
    header.fill = screen.textColorLighter
    header.anchorY = 0

    local enemyImg = display.newImageRect(
        newEnemyScreen.group,
        app.enemy.enemies[enemy].gfx.imagePath,
        app.enemy.enemies[enemy].gfx.imageWidth,
        app.enemy.enemies[enemy].gfx.imageHeight)
    enemyImg.xScale = .5
    enemyImg.yScale = .5
    enemyImg.x = display.actualContentWidth/2 + 130
    enemyImg.y = display.actualContentHeight/2

    local text = display.newText({
        x = display.actualContentWidth/2 - 55,
        y = display.actualContentHeight/2 - 50,
        width = 1312*.75 * screen.uiScale - 60,
        height = 100,
        parent = newEnemyScreen.group,
        align = "left",
        text = enemyTexts[enemy][app.data.lang.lang].text,
        font = screen.font,
        fontSize = 11,})
    text.fill = screen.textColorLighter
    text.anchorY = 0

    local btnContinue = screen.createTextBtn(newEnemyScreen.group, app.data.lang.get("continue"), function()
        newEnemyScreen.hide()
        cb()
    end, app.screen.BTN_GREEN)
    btnContinue.x = display.actualContentWidth/2
    btnContinue.y = display.actualContentHeight/2 + 96

    return newEnemyScreen
end

return newEnemyScreen