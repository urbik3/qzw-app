local app = require("src.app")

local creditsScreen = {}

creditsScreen.screen = nil

creditsScreen.init = function()
    creditsScreen.credits = {
        {name = "Urbik", value = app.data.lang.get("gameDev"), highlight = true},
        {name = "PreVitus", value = app.data.lang.get("writer")},

        {name = app.data.lang.get("graphicAssets"), value = app.data.lang.get("by").." Tatermand", link = "https://opengameart.org/users/tatermand"},
        {name = app.data.lang.get("gameBackgrounds"), value = app.data.lang.get("by").." Beatheart", link = "https://marketplace.coronalabs.com/vendor/beatheart"},
        {name = app.data.lang.get("bloodOtherEffects"), value = app.data.lang.get("by").." Ashishlko11", link = "https://www.gamedevmarket.net/member/ashishlko11/"},
        {name = "GUI", value = "Starlight "..app.data.lang.get("by").." Evil System", link = "https://www.deviantart.com/evil-s"},
        {name = app.data.lang.get("crosshairs"), value = app.data.lang.get("by").." Kenney", link = "http://www.kenney.nl"},

        {name = app.data.lang.get("music"), value = app.data.lang.get("by").." Ross Bugden", link = "https://www.youtube.com/channel/UCQKGLOK2FqmVgVwYferltKQ"},
        {name = app.data.lang.get("sounds"), value = app.data.lang.get("from").." Zapsplat", link = "https://www.zapsplat.com/"},
        {name = app.data.lang.get("sounds"), value = app.data.lang.get("from").." Sound Bible", link = "http://soundbible.com/"},
        
        {name = app.data.lang.get("mathLib"), value = app.data.lang.get("by").." Roaming Gamer", link = "https://roaminggamer.github.io/RGDocs/pages/SSK2/"},
        {name = app.data.lang.get("sha1Lib"), value = app.data.lang.get("by").." Jeffrey Friedl", link = "http://regex.info/blog/lua/sha1"},
    }
end

creditsScreen.destroy = function()
    creditsScreen.screen:removeSelf()
    creditsScreen.screen = nil
end

creditsScreen.show = function()
    creditsScreen.init()
    creditsScreen.screen = display.newGroup()
    app.screen.menuCont:insert(creditsScreen.screen)

    local popup = app.screen.createPopupLarge(app.data.lang.get("credits"))
    creditsScreen.screen:insert(popup.popupGroup)

    local startY = 52
    local startX = 70
    local startXValue = 180
    local startXLink = 393
    local marginY = 17

    for i = 1, #creditsScreen.credits, 1 do
        if(i%2 == 0) then
            local bg = display.newRect(creditsScreen.screen, startX-10, startY+i*marginY-marginY/4, 360, marginY)
            bg.anchorX = 0
            bg.fill = app.screen.textColorLighter
            bg.alpha = .05
        end

        local name = display.newText({
            text = creditsScreen.credits[i].name,
            parent = creditsScreen.screen,
            font = app.screen.font,
            fontSize = 10,
            x = startX,
            y = startY + i*marginY-3,
            width = 120,
            height = marginY-4,
            align = "left"})
        name.fill = creditsScreen.credits[i].highlight and app.screen.textColorLighter or app.screen.textColor
        name.anchorX = 0

        local value = display.newText({
            text = creditsScreen.credits[i].value,
            parent = creditsScreen.screen,
            font = app.screen.font,
            fontSize = 10,
            x = startXValue,
            y = startY + i*marginY-3,
            width = 200,
            height = marginY-4,
            align = "left"})
        value.fill = creditsScreen.credits[i].highlight and app.screen.textColorLighter or app.screen.textColor
        value.anchorX = 0

        if(creditsScreen.credits[i].link) then
            local btnLink = app.screen.createTextBtn(creditsScreen.screen, app.data.lang.get("link"), function()
                system.openURL(creditsScreen.credits[i].link)
            end)
            btnLink.x = i%2==0 and startXLink-40 or startXLink
            btnLink.y = startY + i*marginY-4.5
            btnLink.xScale = .45
            btnLink.yScale = .45
            btnLink.text.size = 20
        end
    end
    
    local backBtn = app.screen.createBtn(creditsScreen.screen, "back", function()
        creditsScreen.destroy()
        app.screens.menu.show()
    end, app.screen.BTN_STATE_NORMAL)
    backBtn.x = 90
    backBtn.y = 285
end

return creditsScreen