local app = require("src.app")
local game = require("src.game")
local screen = require("src.managers.screen")

local confirmationScreen = {}

confirmationScreen.group = nil

confirmationScreen.hide = function()
    confirmationScreen.group:removeSelf()
    confirmationScreen.group = nil
end

confirmationScreen.show = function()
    confirmationScreen.group = display.newGroup()
    game.pauseMenuCont:insert(confirmationScreen.group)

    local bg = display.newRect(confirmationScreen.group, 0, 0, display.actualContentWidth, display.actualContentHeight)
    bg.fill = {0, .5}
    bg.x = display.actualContentWidth/2
    bg.y = display.actualContentHeight/2
    bg:addEventListener("touch", function()
        return true
    end)

    local window = display.newImageRect(confirmationScreen.group, "assets/ui/window-small.png", 512, 638)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.actualContentWidth/2
    window.y = display.actualContentHeight/2

    local marginY = 40

    local btnResume = screen.createTextBtn(confirmationScreen.group, app.data.lang.get("resume"), function()
        confirmationScreen.hide()
    end, app.screen.BTN_GREEN)
    btnResume.x = display.actualContentWidth/2
    btnResume.y = display.actualContentHeight/2 - 65

    local btnExit = screen.createTextBtn(confirmationScreen.group, app.data.lang.get("forfeitLevel"), function()
        confirmationScreen.hide()
        game.lose()
    end)
    btnExit.x = display.actualContentWidth/2
    btnExit.y = display.actualContentHeight/2 + 65
end

return confirmationScreen