local app = require("src.app")
local game = require("src.game")
local screen = require("src.managers.screen")

local winScreen = {}

winScreen.group = nil

winScreen.hide = function()
    winScreen.group:removeSelf()
    winScreen.group = nil
end

winScreen.show = function(cb)
    winScreen.group = display.newGroup()
    game.pauseMenuCont:insert(winScreen.group)

    local bg = display.newRect(winScreen.group, 0, 0, display.actualContentWidth, display.actualContentHeight)
    bg.fill = {0, .5}
    bg.x = display.actualContentWidth/2
    bg.y = display.actualContentHeight/2
    bg:addEventListener("touch", function()
        return true
    end)

    local window = display.newImageRect(winScreen.group, "assets/ui/window-small.png", 512, 638)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.actualContentWidth/2
    window.y = display.actualContentHeight/2
    
    local timeBtn
    timeBtn = app.screen.createTextBtn(winScreen.group, "0", function()end)
    timeBtn.x = display.actualContentWidth/2
    timeBtn.y = display.actualContentHeight/2 - 93
    timeBtn.xScale = .55
    timeBtn.yScale = .55
    timeBtn.text.text = app.gui.timeBtnRight.text.text

    local header = display.newText({
        x = display.actualContentWidth/2,
        y = display.actualContentHeight/2 - 76,
        width = 512 * screen.uiScale - 30,
        height = 45,
        parent = winScreen.group,
        align = "center",
        text = app.data.lang.get("loseScreenTitle"),
        font = screen.font,
        fontSize = 14,})
    header.fill = screen.textColorLighter
    header.anchorY = 0

    local text = display.newText({
        x = display.actualContentWidth/2,
        y = display.actualContentHeight/2 - 55,
        width = 512 * screen.uiScale - 30,
        height = 100,
        parent = winScreen.group,
        align = "left",
        text = app.data.lang.get("loseScreenText")[math.random(1, #app.data.lang.get("loseScreenText"))],
        font = screen.font,
        fontSize = 11,})
    text.fill = screen.textColorLighter
    text.anchorY = 0

    local btnContinue = screen.createTextBtn(winScreen.group, app.data.lang.get("continue"), function()
        winScreen.hide()
        cb()
    end, app.screen.BTN_GREEN)
    btnContinue.x = display.actualContentWidth/2
    btnContinue.y = display.actualContentHeight/2 + 65
end

return winScreen