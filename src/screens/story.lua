local app = require("src.app")
local screen = require("src.managers.screen")
local state = require("src.managers.state")
local json = require("json")

local story = {}

story.checkStory = function()
    local storyData = app.data.story.getStartLevel(state.actualCharacter, state.actualChapter, state.actualLevel)

    if(storyData) then
        story.showStory(storyData)
    else
        app.screen.startLevel()
    end
end

story.hideStory = function()
    story.popup.popupGroup:removeSelf()
    story.popup = nil
end

story.showStory = function(storyData)
    local popup = screen.createPopupLarge(storyData.header)
    story.popup = popup

    local defaultBox = display.newText({
        x = display.contentCenterX,
        y = 60,
        width = 360,
        height = 200,
        parent = story.popup.popupGroup,
        text = storyData.text,
        font = screen.font,
        fontSize = 11,})
    defaultBox.fill = screen.textColorLighter
    defaultBox.anchorY = 0

    local btnStartLevel = screen.createTextBtn(story.popup.popupGroup, app.data.lang.get("storyKickAss"), function()
        story.hideStory()
        app.screen.startLevel()
    end)
    btnStartLevel.text.size = 8
    btnStartLevel.x = display.contentCenterX
    btnStartLevel.y = 284
end

story.showDeath = function()
    local storyData = app.data.story.getDeath()
    local popup = screen.createPopupLarge(app.data.lang.get("youDied"))
    story.popup = popup

    local defaultBox = display.newText({
        x = display.contentCenterX,
        y = 60,
        width = 360,
        height = 170,
        parent = story.popup.popupGroup,
        text = storyData.text,
        font = screen.font,
        fontSize = 11,})
    defaultBox.fill = screen.textColorLighter
    defaultBox.anchorY = 0

    local btnShowLevels = screen.createTextBtn(story.popup.popupGroup, app.data.lang.get("continue"), function()
        story.hideStory()
        if(app.state.actualChapter == app.state.CHAPTER_TUTORIAL) then
            app.screens.gameSelect.show()
        else
            app.screens.levels.showMenu(true)
        end
    end)
    btnShowLevels.x = display.contentCenterX
    btnShowLevels.y = 284
end

return story