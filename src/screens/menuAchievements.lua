local app = require("src.app")
local achievement = require "src.data.achievement"

local menuAchievementsScreen = {}

menuAchievementsScreen.screen = nil

local startLineX = display.actualContentWidth/2 - 55 + display.screenOriginX
local startLineY = display.actualContentHeight/2 - 90 + display.screenOriginY
local startXLink = display.actualContentWidth/2 + 155 + display.screenOriginX
local marginLineY = 20
menuAchievementsScreen.createLine = function(type, data, line)
    local name = display.newText({
        text = data.name[app.state.lang],
        parent = menuAchievementsScreen.screen,
        font = app.screen.font,
        fontSize = 10,
        x = startLineX,
        y = startLineY + line*marginLineY,
        width = 250,
        height = 20,
        align = "left"})
    name.fill = app.screen.textColor

    local btnLink = app.screen.createTextBtn(menuAchievementsScreen.screen, app.data.lang.get("show"), function()
        menuAchievementsScreen.showAchievement(type)
    end)
    btnLink.x = line%2==0 and startXLink-40 or startXLink
    btnLink.y = startLineY + line*marginLineY-4.5
    btnLink.xScale = .45
    btnLink.yScale = .45
    btnLink.text.size = 20
end

menuAchievementsScreen.destroy = function()
    menuAchievementsScreen.screen:removeSelf()
    menuAchievementsScreen.screen = nil
end

menuAchievementsScreen.show = function()
    menuAchievementsScreen.screen = display.newGroup()
    app.screen.menuCont:insert(menuAchievementsScreen.screen)

    local popup = app.screen.createPopupLarge(app.data.lang.get("achievements"))
    menuAchievementsScreen.screen:insert(popup.popupGroup)
    
    local backBtn = app.screen.createBtn(menuAchievementsScreen.screen, "back", function()
        menuAchievementsScreen.destroy()
        app.screens.menu.show()
    end, app.screen.BTN_STATE_NORMAL)
    backBtn.x = 90
    backBtn.y = 285

    local i = 0
    for key, achievement in pairs(app.data.achievement.state) do
        menuAchievementsScreen.createLine(key, achievement, i)
        i = i + 1
    end
end

menuAchievementsScreen.showAchievement = function(achievementName)
    menuAchievementsScreen.destroy()
    menuAchievementsScreen.screen = display.newGroup()
    app.screen.menuCont:insert(menuAchievementsScreen.screen)

    local popup = app.screen.createPopupLarge(app.data.achievement.state[achievementName].name[app.state.lang])
    menuAchievementsScreen.screen:insert(popup.popupGroup)
    
    local backBtn = app.screen.createBtn(menuAchievementsScreen.screen, "back", function()
        menuAchievementsScreen.destroy()
        menuAchievementsScreen.show()
    end, app.screen.BTN_STATE_NORMAL)
    backBtn.x = 90
    backBtn.y = 285

    local type = app.data.achievement.state[achievementName].type
    local value = app.data.achievement.state[achievementName].value
    for i, achievement in pairs(app.data.achievement.state[achievementName].data) do
        local targetValue = achievement.targetValue
        local name = display.newText({
            text = achievement.text[app.state.lang].header,
            parent = menuAchievementsScreen.screen,
            font = app.screen.font,
            fontSize = 10,
            x = startLineX,
            y = startLineY + (i-1)*marginLineY,
            width = 250,
            height = 20,
            align = "left"})
        name.fill = app.screen.textColor

        local isLink = false
        if(type == app.data.achievement.TYPE_NUMBER) then
            if(value >= targetValue) then
                isLink = true
            end
        elseif(type == app.data.achievement.TYPE_CONSTANT) then
            for key, one in pairs(value) do
                if(one == targetValue) then
                    isLink = true
                    break
                end
            end
        end

        if(isLink) then
            local btnLink = app.screen.createTextBtn(menuAchievementsScreen.screen, app.data.lang.get("show"), function()
                menuAchievementsScreen.showAchievementDetail(achievement.text[app.state.lang].text)
            end)
            btnLink.x = i%2==1 and startXLink-40 or startXLink
            btnLink.y = startLineY + (i-1)*marginLineY-4.5
            btnLink.xScale = .45
            btnLink.yScale = .45
            btnLink.text.size = 20
        end
    end
end

menuAchievementsScreen.showAchievementDetail = function(text)
    local popup = app.screen.createPopup()
    local description = display.newText({
        text = text,
        parent = popup.popupGroup,
        font = app.screen.font,
        fontSize = 10,
        x = display.actualContentWidth/2 + display.screenOriginX,
        y = display.actualContentHeight/2 + display.screenOriginY - 30,
        width = 512 * app.screen.uiScale - 20,
        height = 638 * app.screen.uiScale - 100,
        align = "left"})
    description.fill = app.screen.textColor
    local closeBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("close"), function()
        popup.popupGroup:removeSelf()
        popup = nil
    end)
    closeBtn.x = display.actualContentWidth/2 + display.screenOriginX
    closeBtn.y = display.actualContentHeight/2 + display.screenOriginY + 70

end

return menuAchievementsScreen