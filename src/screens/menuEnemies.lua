local app = require("src.app")

local menuEnemiesScreen = {}

menuEnemiesScreen.screen = nil

menuEnemiesScreen.init = function()

end

menuEnemiesScreen.createBtn = function(enemyType, enemy)
    local isAvailable = app.state.enemyInfos[enemyType] == true
    local icon = nil
    if(not isAvailable) then icon = "locked" end
    local btn = app.screen.createBtn(menuEnemiesScreen.screen, icon, function()
        menuEnemiesScreen.destroy()
        local screen = app.screens.newEnemy.show(function()
            menuEnemiesScreen.show()
        end, enemyType, app.screen.menuCont)
        screen.group.x = screen.group.x + display.screenOriginX
        screen.group.y = screen.group.y + display.screenOriginY
    end, isAvailable and app.screen.BTN_STATE_NORMAL or app.screen.BTN_STATE_DISABLED)
    btn.xScale = .4
    btn.yScale = .4
    if(isAvailable) then
        local charSprite = display.newImageRect(btn, app.enemy.enemies[enemyType].gfx.imagePath, app.enemy.enemies[enemyType].gfx.imageWidth, app.enemy.enemies[enemyType].gfx.imageHeight)
        charSprite.xScale = .4
        charSprite.yScale = .4
    end

    return btn
end

menuEnemiesScreen.destroy = function()
    menuEnemiesScreen.screen:removeSelf()
    menuEnemiesScreen.screen = nil
end

menuEnemiesScreen.show = function()
    menuEnemiesScreen.init()
    menuEnemiesScreen.screen = display.newGroup()
    app.screen.menuCont:insert(menuEnemiesScreen.screen)

    local popup = app.screen.createPopupLarge(app.data.lang.get("enemies"))
    menuEnemiesScreen.screen:insert(popup.popupGroup)
    
    local backBtn = app.screen.createBtn(menuEnemiesScreen.screen, "back", function()
        menuEnemiesScreen.destroy()
        app.screens.menu.show()
    end, app.screen.BTN_STATE_NORMAL)
    backBtn.x = 90
    backBtn.y = 285

    local i = 0
    local columns = 5
    local marginRight = 67
    local marginBottom = 67
    local startX = display.actualContentWidth/2 + display.screenOriginX - marginRight*2
    local startY = display.actualContentHeight/2 + display.screenOriginY - marginBottom
    for key, enemy in pairs(app.screens.newEnemy.enemyTexts) do
        local btn = menuEnemiesScreen.createBtn(key, enemy)
        btn.x = startX + i % columns * marginRight
        btn.y = startY + math.floor(i / columns) * marginBottom
        i = i + 1
    end
end

return menuEnemiesScreen