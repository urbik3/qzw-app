local app = require("src.app")

local settingsScreen = {}

settingsScreen.screen = nil

settingsScreen.destroy = function()
    settingsScreen.screen:removeSelf()
    settingsScreen.screen = nil
end

settingsScreen.show = function(pauseMenuPopup)
    settingsScreen.screen = display.newGroup()
    app.screen.menuCont:insert(settingsScreen.screen)

    local popup = app.screen.createPopupLarge(app.data.lang.get("settings"))
    settingsScreen.screen:insert(popup.popupGroup)

    -- LANG SELECTION
    local langText = display.newText({
        parent = popup.popupGroup,
        text = app.data.lang.get("chooseLang")..":",
        x = display.contentCenterX - 100,
        y = display.contentCenterY - 80,
        width = 150,
        align = "left",
        fontSize = 12})
    langText.fill = app.screen.textColor
    
    local czBtn = app.screen.createTextBtn(popup.popupGroup, "Česky", function()
        app.state.lang = app.data.lang.LANG_CZ
        app.data.lang.lang = app.data.lang.LANG_CZ
        app.state.saveData()
        settingsScreen.destroy()
        settingsScreen.show(pauseMenuPopup)
    end, app.data.lang.lang == app.data.lang.LANG_CZ and app.screen.BTN_GREEN or app.screen.BTN_ORANGE)
    czBtn.x = display.contentCenterX + 20
    czBtn.y = display.contentCenterY - 80
    czBtn.xScale = .75
    czBtn.yScale = .75

    local engBtn = app.screen.createTextBtn(popup.popupGroup, "English", function()
        app.state.lang = app.data.lang.LANG_EN
        app.data.lang.lang = app.data.lang.LANG_EN
        app.state.saveData()
        settingsScreen.destroy()
        settingsScreen.show(pauseMenuPopup)
    end, app.data.lang.lang == app.data.lang.LANG_EN and app.screen.BTN_GREEN or app.screen.BTN_ORANGE)
    engBtn.x = display.contentCenterX + 120
    engBtn.y = display.contentCenterY - 80
    engBtn.xScale = .75
    engBtn.yScale = .75

    -- SOUND OPTIONS
    local soundText = display.newText({
        parent = popup.popupGroup,
        text = app.data.lang.get("settingsSound")..":",
        x = display.contentCenterX - 100,
        y = display.contentCenterY - 45,
        width = 150,
        align = "left",
        fontSize = 12})
    soundText.fill = app.screen.textColor
    
    local soundOnBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("settingsSoundOn"), function()
        app.sound.soundOn()
        settingsScreen.destroy()
        settingsScreen.show(pauseMenuPopup)
    end, app.sound.soundControl == app.sound.SOUND_CONTROL_ON and app.screen.BTN_GREEN or app.screen.BTN_ORANGE)
    soundOnBtn.x = display.contentCenterX + 20
    soundOnBtn.y = display.contentCenterY - 45
    soundOnBtn.xScale = .75
    soundOnBtn.yScale = .75

    local soundOffBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("settingsSoundOff"), function()
        app.sound.soundOff()
        settingsScreen.destroy()
        settingsScreen.show(pauseMenuPopup)
    end, app.sound.soundControl == app.sound.SOUND_CONTROL_OFF and app.screen.BTN_GREEN or app.screen.BTN_ORANGE)
    soundOffBtn.x = display.contentCenterX + 120
    soundOffBtn.y = display.contentCenterY - 45
    soundOffBtn.xScale = .75
    soundOffBtn.yScale = .75

    -- MUSIC OPTIONS
    local musicText = display.newText({
        parent = popup.popupGroup,
        text = app.data.lang.get("settingsMusic")..":",
        x = display.contentCenterX - 100,
        y = display.contentCenterY - 10,
        width = 150,
        align = "left",
        fontSize = 12})
    musicText.fill = app.screen.textColor
    
    local musicOnBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("settingsSoundOn"), function()
        app.sound.musicOn()
        settingsScreen.destroy()
        settingsScreen.show(pauseMenuPopup)
    end, app.sound.musicControl == app.sound.MUSIC_CONTROL_ON and app.screen.BTN_GREEN or app.screen.BTN_ORANGE)
    musicOnBtn.x = display.contentCenterX + 20
    musicOnBtn.y = display.contentCenterY - 10
    musicOnBtn.xScale = .75
    musicOnBtn.yScale = .75

    local musicOffBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("settingsSoundOff"), function()
        app.sound.musicOff()
        settingsScreen.destroy()
        settingsScreen.show(pauseMenuPopup)
    end, app.sound.musicControl == app.sound.MUSIC_CONTROL_OFF and app.screen.BTN_GREEN or app.screen.BTN_ORANGE)
    musicOffBtn.x = display.contentCenterX + 120
    musicOffBtn.y = display.contentCenterY - 10
    musicOffBtn.xScale = .75
    musicOffBtn.yScale = .75

    -- BACK
    if(pauseMenuPopup) then
        local backBtn = app.screen.createBtn(popup.popupGroup, "back", function()
            settingsScreen.destroy()
        end, app.screen.BTN_STATE_NORMAL)
        backBtn.x = 90
        backBtn.y = 285
    else
        local backBtn = app.screen.createBtn(popup.popupGroup, "back", function()
            settingsScreen.destroy()
            app.screens.gameSelect.show()
        end, app.screen.BTN_STATE_NORMAL)
        backBtn.x = 90
        backBtn.y = 285
    end
end

return settingsScreen