local app = require("src.app")
local state = require("src.managers.state")
local screen = require("src.managers.screen")
local json = require("json")
local sha1 = require("lib.sha1")

local levelScreen = {}

levelScreen.selectedLevel = false

levelScreen.BTN_STATE_AVAILABLE = "btn_state_available"
levelScreen.BTN_STATE_AVAILABLE_SELECTED = "btn_state_available_selected"
levelScreen.BTN_STATE_FINISHED = "btn_state_finished"
levelScreen.BTN_STATE_FINISHED_SELECTED = "btn_state_finished_selected"
levelScreen.BTN_STATE_SELECTED = "btn_state_selected"
levelScreen.BTN_STATE_LOCKED = "btn_state_locked"

levelScreen.createLevelBtn = function(parent, levelNumber, chapter)
    local btnState
    local levelState = app.state.getLevel(levelNumber).state
    if(levelState == state.LEVEL_LOCKED) then
        btnState = levelScreen.BTN_STATE_LOCKED
    elseif(levelState == state.LEVEL_FINISHED) then
        if(levelScreen.selectedLevel == levelNumber) then
            btnState = levelScreen.BTN_STATE_FINISHED_SELECTED
        else
            btnState = levelScreen.BTN_STATE_FINISHED
        end
    elseif(levelState == state.LEVEL_AVAILABLE) then
        if(levelScreen.selectedLevel == levelNumber) then
            btnState = levelScreen.BTN_STATE_AVAILABLE_SELECTED
        else
            btnState = levelScreen.BTN_STATE_AVAILABLE
        end
    end

    local btnGroup = display.newGroup()
    btnGroup.xScale = screen.uiScale
    btnGroup.yScale = screen.uiScale
    parent:insert(btnGroup)

    local btnImg
    if(btnState == levelScreen.BTN_STATE_AVAILABLE) then
        btnImg = display.newImageRect(btnGroup, "assets/ui/levels/level-available.png", 187, 172)
    elseif(btnState == levelScreen.BTN_STATE_AVAILABLE_SELECTED) then
        btnImg = display.newImageRect(btnGroup, "assets/ui/levels/level-available-selected.png", 187, 172)
    elseif(btnState == levelScreen.BTN_STATE_FINISHED_SELECTED) then
        btnImg = display.newImageRect(btnGroup, "assets/ui/levels/level-finished-selected.png", 187, 172)
    elseif(btnState == levelScreen.BTN_STATE_FINISHED) then
        btnImg = display.newImageRect(btnGroup, "assets/ui/levels/level-finished.png", 187, 172)
    elseif(btnState == levelScreen.BTN_STATE_LOCKED) then
        btnImg = display.newImageRect(btnGroup, "assets/ui/levels/level-locked.png", 172, 172)
    end
    
    if(btnState ~= levelScreen.BTN_STATE_LOCKED) then
        btnImg.x = 8
        local numberImg = display.newImageRect(btnGroup, "assets/ui/levels/level-number-"..levelNumber..".png", 187, 172)
        numberImg.x = 8
    end

    if(btnState ~= levelScreen.BTN_STATE_LOCKED) then
        btnImg:addEventListener("touch", function(event)
            if(event.phase == "began") then
            if(levelScreen.selectedLevel == levelNumber) then
                    levelScreen.selectedLevel = false
                else
                    levelScreen.selectedLevel = levelNumber
                end
                levelScreen.destroyMenu()
                levelScreen.showMenu()
            end
        end)
    end

    return btnGroup
end

levelScreen.initLevel = function()
    levelScreen.destroyMenu(true)
    state.actualLevel = levelScreen.selectedLevel
    levelScreen.selectedLevel = false
    app.screens.story.checkStory()
end

levelScreen.destroyMenu = function(destroyScreenCapture)
    if(destroyScreenCapture and screen.screenCapture) then
        screen.screenCapture:removeSelf()
        screen.screenCapture = nil
    end
    levelScreen.levelsMenuGroup:removeSelf()
    levelScreen.levelsMenuGroup = nil
end

levelScreen.selectedCharacterIndex = 1
levelScreen.showMenu = function(isTransition)
    local levelsMenuGroup = display.newGroup()
    screen.menuCont:insert(levelsMenuGroup)
    levelScreen.levelsMenuGroup = levelsMenuGroup

    if(isTransition) then
        transition.from(levelsMenuGroup, {time = 500, alpha = 0})
    end

    local bg = display.newRect(levelsMenuGroup, 0, 0, display.actualContentWidth*2, display.actualContentHeight*2)
    bg.x = display.screenOriginX
    bg.y = display.screenOriginY
    bg.fill = {0, .5}

    local window = display.newImageRect(levelsMenuGroup, "assets/ui/window.png", 1312, 848)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.contentCenterX
    window.y = display.contentCenterY

    local header = display.newImageRect(levelsMenuGroup, "assets/ui/header.png", 478, 112)
    header.xScale = screen.uiScale
    header.yScale = screen.uiScale
    header.x = display.contentCenterX
    header.y = 35

    local headerText = display.newText({parent = levelsMenuGroup, text = app.data.lang.get("selectLevel"), font = screen.font, fontSize = 16, align = "center"})
    headerText.x = display.contentCenterX
    headerText.y = 35
    headerText.fill = screen.textColor

    -- CHARACTER
    local avatar = display.newImageRect(levelsMenuGroup, "assets/ui/avatar.png", 234, 234)
    avatar.xScale = screen.uiScale
    avatar.yScale = screen.uiScale
    avatar.x = 63
    avatar.y = 53

    local charWidth = app.player.characters[app.screens.gameSelect.charnumToConst(app.screens.gameSelect.selectedCharacter)].gfx.width
    local charHeight = app.player.characters[app.screens.gameSelect.charnumToConst(app.screens.gameSelect.selectedCharacter)].gfx.height
    local icon = display.newImageRect(levelsMenuGroup, "assets/characters/character"..app.screens.gameSelect.selectedCharacter..".png", charWidth, charHeight)
    icon.xScale = screen.uiScale * .65
    icon.yScale = screen.uiScale * .65
    icon.x = 61
    icon.y = 49

    -- USERNAME
    local username = display.newImageRect(levelsMenuGroup, "assets/ui/item-short-no-icon.png", 355, 124)
    username.xScale = screen.uiScale * .68
    username.yScale = screen.uiScale * .68
    username.anchorX = 0
    username.x = 20
    username.y = 105

    local textUsername = display.newText({parent = levelsMenuGroup, text = app.state.username and app.state.username or "N/A", align = "center", font = app.screen.font, fontSize = 8})
    textUsername.anchorX = 0
    textUsername.x = 30
    textUsername.y = 105
    textUsername.fill = screen.textColor
    if(not app.state.username) then
        username:addEventListener("touch", function(event)
            if(event.phase == "began") then
                app.screens.username.checkUsername()
            end
        end)
    end

    -- CHAPTER TIME
    local chapterTime = display.newImageRect(levelsMenuGroup, "assets/ui/item-short.png", 355, 124)
    chapterTime.xScale = screen.uiScale * .68
    chapterTime.yScale = screen.uiScale * .68
    chapterTime.anchorX = 0
    chapterTime.x = 20
    chapterTime.y = 137

    local iconTime = display.newImageRect(levelsMenuGroup, "assets/ui/icons/icon-time.png", 152, 152)
    iconTime.xScale = screen.uiScale * .68
    iconTime.yScale = screen.uiScale * .68
    iconTime.anchorX = 0
    iconTime.x = 18
    iconTime.y = 137

    local time = state.getChapterTime()
    if(levelScreen.selectedLevel) then
        time = state.getLevel(levelScreen.selectedLevel).time
    end
    local timeString = app.screen.formatTime(time)
    local textTime = display.newText({parent = levelsMenuGroup, text = timeString, align = "left", font = app.screen.font, fontSize = 8})
    textTime.anchorX = 0
    textTime.x = 50
    textTime.y = 137
    textTime.fill = screen.textColor

    -- CHAPTER HEALTH
    local chapterHealth = display.newImageRect(levelsMenuGroup, "assets/ui/item-short.png", 355, 124)
    chapterHealth.xScale = screen.uiScale * .68
    chapterHealth.yScale = screen.uiScale * .68
    chapterHealth.anchorX = 0
    chapterHealth.x = 20
    chapterHealth.y = 169

    local iconHeart = display.newImageRect(levelsMenuGroup, "assets/ui/icons/icon-heart.png", 152, 152)
    iconHeart.xScale = screen.uiScale * .68
    iconHeart.yScale = screen.uiScale * .68
    iconHeart.anchorX = 0
    iconHeart.x = 18
    iconHeart.y = 169

    local textHeart = display.newText({parent = levelsMenuGroup, text = state.getPlayer().hp.."/"..state.getMaxLevel().maxHealth, align = "left", font = app.screen.font, fontSize = 7})
    textHeart.anchorX = 0
    textHeart.x = 50
    textHeart.y = 169
    textHeart.fill = screen.textColor

    -- SIDE BTNS
    local leaderboardBtn = app.screen.createTextBtn(levelsMenuGroup, app.data.lang.get("leaderboards"), function()
        app.leaderboards.getBoard(app.state.actualCharacter, app.state.actualChapter)
        levelScreen.destroyMenu()
    end)
    leaderboardBtn.xScale = .585
    leaderboardBtn.yScale = .585
    leaderboardBtn.x = 56
    leaderboardBtn.y = 202
    
    if(app.state.actualChapter ~= app.state.CHAPTER_ENDLESS) then
        local resetBtn = app.screen.createTextBtn(levelsMenuGroup, app.data.lang.get("resetChapter"), function()
            local popup = app.screen.createPopup()

            local defaultBox = display.newText({
                x = display.contentCenterX,
                y = display.contentCenterY - 80,
                width = 120,
                height = 130,
                parent = popup.popupGroup,
                text = app.data.lang.get("resetChapterText"),
                font = screen.font,
                fontSize = 11,})
            defaultBox.fill = screen.textColorLighter
            defaultBox.anchorY = 0

            local btnAccept = screen.createTextBtn(popup.popupGroup, app.data.lang.get("resetChapterConfirm"), function()
                app.state.player[app.state.actualCharacter][app.state.actualChapter] = app.fn.deepcopy(
                    app.state.original.player[app.state.actualCharacter][app.state.actualChapter])
                app.state.levels[app.state.actualCharacter][app.state.actualChapter] = app.fn.deepcopy(
                    app.state.original.levels[app.state.actualCharacter][app.state.actualChapter])
                app.state.saveData()
                popup.popupGroup:removeSelf()
                popup.popupGroup = nil
                levelScreen.destroyMenu()
                levelScreen.showMenu()
            end, app.screen.BTN_GREEN)
            btnAccept.text.size = 8
            btnAccept.x = display.contentCenterX
            btnAccept.y = display.contentCenterY + 20

            local btnDecline = screen.createTextBtn(popup.popupGroup, app.data.lang.get("resetChapterDecline"), function()
                popup.popupGroup:removeSelf()
                popup.popupGroup = nil
            end, app.screen.BTN_ORANGE)
            btnDecline.text.size = 8
            btnDecline.x = display.contentCenterX
            btnDecline.y = display.contentCenterY + 65
        end)
        resetBtn.xScale = .585
        resetBtn.yScale = .585
        resetBtn.x = 56
        resetBtn.y = 233
    end

    -- local saveScoreBtn = app.screen.createTextBtn(levelsMenuGroup, "Save score", function()
    --     app.network.postScore(app.state.getChapterTime())
    -- end)
    -- saveScoreBtn.xScale = .585
    -- saveScoreBtn.yScale = .585
    -- saveScoreBtn.x = 56
    -- saveScoreBtn.y = 264
    
    -- BTNS
    local acceptBtn = screen.createBtn(levelsMenuGroup, "accept", levelScreen.initLevel, levelScreen.selectedLevel == false and screen.BTN_STATE_DISABLED or screen.BTN_STATE_NORMAL)
    acceptBtn.x = display.contentWidth/2 + 150
    acceptBtn.y = 285

    local backBtn = screen.createBtn(levelsMenuGroup, "back", function()
        app.screens.levels.destroyMenu()
        levelScreen.selectedLevel = false
        app.screens.gameSelect.show()
    end, screen.BTN_STATE_NORMAL)
    backBtn.x = display.contentWidth/2 - 150
    backBtn.y = 285

    local infoBtn = screen.createBtn(levelsMenuGroup, "chat", function() end, screen.BTN_STATE_NORMAL)
    infoBtn.x = display.contentWidth/2
    infoBtn.y = 285

    if(state.actualChapter == state.CHAPTER_CASUAL) then
        local startX = 130
        local startY = 90
        local marginX = 65
        local marginY = 65
        levelScreen.showFirstChapter(levelsMenuGroup, startX, startY, marginX, marginY)
    elseif(state.actualChapter == state.CHAPTER_HARD) then
        local marginX = 65
        local marginY = 65
        local startY = 90
        local startX = display.contentWidth/2 - marginX
        levelScreen.showSecondChapter(levelsMenuGroup, startX, startY, marginX, marginY)
    else
        local marginX = 65
        local marginY = 65
        local startY = 90
        local startX = display.contentWidth/2 - marginX
        levelScreen.showThirdChapter(levelsMenuGroup, startX, startY, marginX, marginY)
    end
end

levelScreen.showFirstChapter = function(levelsMenuGroup, startX, startY, marginX, marginY)
    local btnLevel1 = levelScreen.createLevelBtn(levelsMenuGroup, 1, app.state.CHAPTER_HARD)
    btnLevel1.x = startX
    btnLevel1.y = startY

    local btnLevel2 = levelScreen.createLevelBtn(levelsMenuGroup, 2)
    btnLevel2.x = startX + marginX
    btnLevel2.y = startY

    local btnLevel3 = levelScreen.createLevelBtn(levelsMenuGroup, 3)
    btnLevel3.x = startX + marginX*2
    btnLevel3.y = startY

    local btnLevel4 = levelScreen.createLevelBtn(levelsMenuGroup, 4)
    btnLevel4.x = startX + marginX*3
    btnLevel4.y = startY

    local btnLevel5 = levelScreen.createLevelBtn(levelsMenuGroup, 5)
    btnLevel5.x = startX + marginX*4
    btnLevel5.y = startY

    local btnLevel6 = levelScreen.createLevelBtn(levelsMenuGroup, 6)
    btnLevel6.x = startX
    btnLevel6.y = startY + marginY

    local btnLevel7 = levelScreen.createLevelBtn(levelsMenuGroup, 7)
    btnLevel7.x = startX + marginX
    btnLevel7.y = startY + marginY

    local btnLevel8 = levelScreen.createLevelBtn(levelsMenuGroup, 8)
    btnLevel8.x = startX + marginX*2
    btnLevel8.y = startY + marginY

    local btnLevel9 = levelScreen.createLevelBtn(levelsMenuGroup, 9)
    btnLevel9.x = startX + marginX*3
    btnLevel9.y = startY + marginY

    local btnLevel10 = levelScreen.createLevelBtn(levelsMenuGroup, 10)
    btnLevel10.x = startX + marginX*4
    btnLevel10.y = startY + marginY

    local btnLevel11 = levelScreen.createLevelBtn(levelsMenuGroup, 11)
    btnLevel11.x = startX
    btnLevel11.y = startY + marginY*2

    local btnLevel12 = levelScreen.createLevelBtn(levelsMenuGroup, 12)
    btnLevel12.x = startX + marginX
    btnLevel12.y = startY + marginY*2

    local btnLevel13 = levelScreen.createLevelBtn(levelsMenuGroup, 13)
    btnLevel13.x = startX + marginX*2
    btnLevel13.y = startY + marginY*2

    local btnLevel14 = levelScreen.createLevelBtn(levelsMenuGroup, 14)
    btnLevel14.x = startX + marginX*3
    btnLevel14.y = startY + marginY*2

    local btnLevel15 = levelScreen.createLevelBtn(levelsMenuGroup, 15)
    btnLevel15.x = startX + marginX*4
    btnLevel15.y = startY + marginY*2
end

levelScreen.showSecondChapter = function(levelsMenuGroup, startX, startY, marginX, marginY)
    local btnLevel1 = levelScreen.createLevelBtn(levelsMenuGroup, 1, app.state.CHAPTER_HARD)
    btnLevel1.x = startX - marginX * .5
    btnLevel1.y = startY
    local btnLevel2 = levelScreen.createLevelBtn(levelsMenuGroup, 2, app.state.CHAPTER_HARD)
    btnLevel2.x = startX + marginX * .5
    btnLevel2.y = startY
    local btnLevel3 = levelScreen.createLevelBtn(levelsMenuGroup, 3, app.state.CHAPTER_HARD)
    btnLevel3.x = startX + marginX * 1.5
    btnLevel3.y = startY

    local btnLevel4 = levelScreen.createLevelBtn(levelsMenuGroup, 4, app.state.CHAPTER_HARD)
    btnLevel4.x = startX + marginX * 0
    btnLevel4.y = startY + marginY
    local btnLevel5 = levelScreen.createLevelBtn(levelsMenuGroup, 5, app.state.CHAPTER_HARD)
    btnLevel5.x = startX + marginX * 1
    btnLevel5.y = startY + marginY
    local btnLevel6 = levelScreen.createLevelBtn(levelsMenuGroup, 6, app.state.CHAPTER_HARD)
    btnLevel6.x = startX + marginX * 2
    btnLevel6.y = startY + marginY

    local btnLevel7 = levelScreen.createLevelBtn(levelsMenuGroup, 7, app.state.CHAPTER_HARD)
    btnLevel7.x = startX + marginX * .5
    btnLevel7.y = startY + marginY * 2
    local btnLevel8 = levelScreen.createLevelBtn(levelsMenuGroup, 8, app.state.CHAPTER_HARD)
    btnLevel8.x = startX + marginX * 1.5
    btnLevel8.y = startY + marginY * 2
    local btnLevel9 = levelScreen.createLevelBtn(levelsMenuGroup, 9, app.state.CHAPTER_HARD)
    btnLevel9.x = startX + marginX * 2.5
    btnLevel9.y = startY + marginY * 2
end

levelScreen.showThirdChapter = function(levelsMenuGroup, startX, startY, marginX, marginY)
    local btnEndless = app.screen.createBtn(levelsMenuGroup, "sight", function()
        if(levelScreen.selectedLevel) then
            levelScreen.selectedLevel = false
        else
            levelScreen.selectedLevel = 1
        end
        levelScreen.destroyMenu()
        levelScreen.showMenu()
    end, levelScreen.selectedLevel == 1 and app.screen.BTN_STATE_NORMAL or app.screen.BTN_STATE_REDUCED)
    btnEndless.x = startX + marginX * 1
    btnEndless.y = startY + marginY
    btnEndless.xScale = .5
    btnEndless.yScale = .5
end

return levelScreen