local app = require("src.app")

local screen = {}

screen.popup = nil

screen.destroy = function()
    screen.popup.popupGroup:removeSelf()
    screen.popup.popupGroup = nil
    screen.popup = nil
end

screen.show = function()
    local popup = app.screen.createPopup()
    screen.popup = popup

    local infoText = display.newText({parent = popup.popupGroup, text = "Choose language:\nVyber jazyk:", width = 120, x = display.contentCenterX, y = display.contentCenterY - 65, fontSize = 10})
    infoText.fill = app.screen.textColor

    local czBtn = app.screen.createTextBtn(popup.popupGroup, "Česky", function()
        app.state.lang = app.data.lang.LANG_CZ
        app.data.lang.lang = app.data.lang.LANG_CZ
        app.state.saveData()
        screen.destroy()
        app.screens.gameSelect.show()
        app.screens.username.checkUsername()
    end)
    czBtn.x = display.contentCenterX
    czBtn.y = display.contentCenterY + 65

    local engBtn = app.screen.createTextBtn(popup.popupGroup, "English", function()
        app.state.lang = app.data.lang.LANG_EN
        app.data.lang.lang = app.data.lang.LANG_EN
        app.state.saveData()
        screen.destroy()
        app.screens.gameSelect.show()
        app.screens.username.checkUsername()
    end)
    engBtn.x = display.contentCenterX
    engBtn.y = display.contentCenterY + 30
end

return screen