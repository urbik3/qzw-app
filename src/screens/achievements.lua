local app = require("src.app")
local game = require("src.game")
local screen = require("src.managers.screen")

local achievements = {}

local popup = nil
local haveGameBeenStopped = false

achievements.hide = function()
    if(popup) then
        popup.popupGroup:removeSelf()
        popup = nil
    end

    if(haveGameBeenStopped) then
        haveGameBeenStopped = false
        game.run()
    end
end

achievements.showNewAchievement = function(data)
    
    if(popup) then
        popup.popupGroup:removeSelf()
        popup = nil
    end

    if(game.status == game.STATUS_PLAYING) then
        game.stop()
        haveGameBeenStopped = true
    end

    popup = {}
    popup.popupGroup = display.newGroup()
    screen.tutorialCont:insert(popup.popupGroup)

    local bg = display.newRect(popup.popupGroup, 0, 0, display.actualContentWidth, display.actualContentHeight)
    bg.fill = {0, .5}
    bg.x = display.actualContentWidth/2
    bg.y = display.actualContentHeight/2
    bg:addEventListener("touch", function()
        return true
    end)

    local window = display.newImageRect(popup.popupGroup, "assets/ui/window-small.png", 512, 638)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.actualContentWidth/2
    window.y = display.actualContentHeight/2

    local header = display.newText({
        x = display.actualContentWidth/2,
        y = display.actualContentHeight/2 - 80,
        width = 512 * screen.uiScale - 40,
        height = 40,
        parent = popup.popupGroup,
        align = "center",
        text = data.header,
        font = screen.font,
        fontSize = 16,})
    header.fill = screen.textColorLighter
    header.anchorY = 0

    local text = display.newText({
        x = display.actualContentWidth/2,
        y = display.actualContentHeight/2 - 30,
        width = 512 * screen.uiScale - 40,
        height = 100,
        parent = popup.popupGroup,
        align = "left",
        text = data.text,
        font = screen.font,
        fontSize = 11,})
    text.fill = screen.textColorLighter
    text.anchorY = 0

    local btnContinue = screen.createTextBtn(popup.popupGroup, app.data.lang.get("continue"), achievements.hide, app.screen.BTN_GREEN)
    btnContinue.x = display.actualContentWidth/2
    btnContinue.y = display.actualContentHeight/2 + 65

    return popup
end

return achievements