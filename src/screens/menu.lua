local app = require("src.app")

local menuScreen = {}

menuScreen.screen = nil

menuScreen.init = function()

end

menuScreen.destroy = function()
    menuScreen.screen:removeSelf()
    menuScreen.screen = nil
end

menuScreen.show = function()
    menuScreen.init()
    menuScreen.screen = display.newGroup()
    app.screen.menuCont:insert(menuScreen.screen)

    local popup = app.screen.createPopupLarge(app.data.lang.get("about"))
    menuScreen.screen:insert(popup.popupGroup)
    
    local backBtn = app.screen.createBtn(menuScreen.screen, "back", function()
        menuScreen.destroy()
        app.screens.gameSelect.show()
    end, app.screen.BTN_STATE_NORMAL)
    backBtn.x = 90
    backBtn.y = 285
    
    local creditsBtn = app.screen.createTextBtn(menuScreen.screen, app.data.lang.get("credits"), function()
        menuScreen.destroy()
        app.screens.credits.show()
    end)
    creditsBtn.x = display.actualContentWidth/2 + display.screenOriginX
    creditsBtn.y = 100
    
    local achievementsBtn = app.screen.createTextBtn(menuScreen.screen, app.data.lang.get("achievements"), function()
        menuScreen.destroy()
        app.screens.menuAchievements.show()
    end)
    achievementsBtn.x = display.actualContentWidth/2 + display.screenOriginX
    achievementsBtn.y = 150
    
    local enemiesBtn = app.screen.createTextBtn(menuScreen.screen, app.data.lang.get("enemies"), function()
        menuScreen.destroy()
        app.screens.menuEnemies.show()
    end)
    enemiesBtn.x = display.actualContentWidth/2 + display.screenOriginX
    enemiesBtn.y = 200
end

return menuScreen