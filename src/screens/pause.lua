local app = require("src.app")
local game = require("src.game")
local screen = require("src.managers.screen")

local pauseScreen = {}

pauseScreen.group = nil

pauseScreen.hide = function()
    pauseScreen.group:removeSelf()
    pauseScreen.group = nil
end

pauseScreen.show = function()
    pauseScreen.group = display.newGroup()
    game.pauseMenuCont:insert(pauseScreen.group)

    local bg = display.newRect(pauseScreen.group, 0, 0, display.actualContentWidth, display.actualContentHeight)
    bg.fill = {0, .5}
    bg.x = display.actualContentWidth/2
    bg.y = display.actualContentHeight/2
    bg:addEventListener("touch", function()
        return true
    end)

    local window = display.newImageRect(pauseScreen.group, "assets/ui/window-small.png", 512, 638)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.actualContentWidth/2
    window.y = display.actualContentHeight/2

    local marginY = 40

    local btnResume = screen.createTextBtn(pauseScreen.group, app.data.lang.get("resume"), function()
        pauseScreen.hide()
    end, app.screen.BTN_GREEN)
    btnResume.x = display.actualContentWidth/2
    btnResume.y = display.actualContentHeight/2 - 65

    local btnOptions = screen.createTextBtn(pauseScreen.group, app.data.lang.get("settings"), function()
        pauseScreen.hide()
        app.screens.settings.show(true)
    end)
    btnOptions.x = display.actualContentWidth/2
    btnOptions.y = display.actualContentHeight/2 - 25

    local btnExit = screen.createTextBtn(pauseScreen.group, app.data.lang.get("forfeitLevel"), function()
        pauseScreen.hide()
        game.lose()
    end)
    btnExit.x = display.actualContentWidth/2
    btnExit.y = display.actualContentHeight/2 + 65
end

return pauseScreen