local app = require("src.app")
local json = require("json")

local username = {}

username.checkUsername = function(cb)
    if(not app.state.username) then
        local popup = app.screen.createPopup()

        local infoText = display.newText({parent = popup.popupGroup, text = app.data.lang.get("chooseUsername"), width = 120, x = display.contentCenterX, y = display.contentCenterY - 65, fontSize = 10})
        infoText.fill = app.screen.textColor

        local textfield = native.newTextField(display.contentCenterX, display.contentCenterY - 20, 120, 20)
        popup.popupGroup:insert(textfield)
        textfield.placeholder = app.data.lang.get("username")
        
        local submitBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("submit"), function()
            if(#textfield.text > 1 and #textfield.text < 13) then
                local hash = app.network.createHash({textfield.text})
                local loader
                local function networkListener(event)
                    if (event.isError) then
                        print("Network error: ", event.response)
                    else
                        local response = json.decode(event.response)
                        if(response and response.error) then
                            infoText.text = app.data.lang.get("usernameTaken")
                        elseif(response) then
                            app.state.username = response.username
                            app.state.saveData()
                            popup.popupGroup:removeSelf()
                            popup.popupGroup = nil
                            popup = nil
                            if(cb) then
                                cb()
                            end
                        else
                            print(event.response)
                            infoText.text = app.data.lang.get("usernameError")
                        end
                    end
                    loader:removeSelf()
                end
                
                local url = app.network.url.."?postUsername&username="..encodeURI(textfield.text).."&hash="..encodeURI(hash)
                network.request(url, "POST", networkListener)
                loader = app.screen.createLoader(popup.popupGroup)
                loader.x = display.contentCenterX
                loader.y = display.contentCenterY - 65
            else
                infoText.text = app.data.lang.get("usernameLengthHint")
            end

        end, app.screen.BTN_GREEN)
        submitBtn.x = display.contentCenterX
        submitBtn.y = display.contentCenterY + 25

        local cancelBtn = app.screen.createTextBtn(popup.popupGroup, app.data.lang.get("cancel"), function()
            popup.popupGroup:removeSelf()
            popup.popupGroup = nil
            popup = nil
        end)
        cancelBtn.x = display.contentCenterX
        cancelBtn.y = display.contentCenterY + 65
    end
end

return username