local fn = {}

fn.distance = function (a, b)
    return math.sqrt((a.x-b.x)^2+(a.y-b.y)^2)
end

fn.normalize = function (a)
    local length = math.sqrt(a.x^2 + a.y^2)
    if(length <= 0) then
        return {x = 0, y = 0}
    end
    return {x = a.x / length, y = a.y / length}
end

fn.multiplyVector = function (a, multiplier)
    return {x = a.x * multiplier, y = a.y * multiplier}
end

fn.vectorLength = function (a)
    return fn.distance({x = 0, y = 0}, a)
end

fn.polarToCartesian = function(r, angle)
    return {x = r * math.cos(angle), y = r * math.sin(angle)}
end

fn.toRadian = function(degree)
    return degree*math.pi/180
end
fn.toDegree = function(rad)
    return rad*180/math.pi
end
fn.deg = function(deg)
    if(deg < 0) then return 360 + deg elseif (deg > 360) then return deg - 360 else return deg end
end

fn.getAngle = function(pointAngleTo, pointAngleFrom)
    return fn.deg(fn.toDegree(math.atan2(pointAngleFrom.y - pointAngleTo.y, pointAngleFrom.x - pointAngleTo.x)) + 90)
    -- return fn.deg(ssk.math2d.vector2Angle(ssk.math2d.sub(pointAngleFrom, pointAngleTo))) -- returns integer only numbers
end

fn.mapRange = function(num, in_min, in_max, out_min, out_max, limit_range, easing)
    if(easing == nil) then easing = function(x) return x end end

    local result = easing((num - in_min) / (in_max - in_min)) * (out_max - out_min) + out_min
    
    if(limit_range) then
        if(out_min < out_max) then
            result = math.max(out_min, result)
            result = math.min(out_max, result)
        else
            result = math.max(out_max, result)
            result = math.min(out_min, result)
        end
    end
    return result
end

fn.rotationDiff = function(gfx, point)
    local rotateTo = fn.getAngle(gfx, point)
    return math.abs(fn.deg(rotateTo) - fn.deg(gfx.rotation))
end

fn.rotateTo = function(instance, gfx, point, options)
    local rotateTo = fn.getAngle(gfx, point)
    local rotationSide
    if(rotateTo < gfx.rotation) then
        if(math.abs(rotateTo - gfx.rotation) > 180) then
            rotationSide = 1
        else
            rotationSide = -1
        end
        else if(math.abs(rotateTo - gfx.rotation) > 180) then
            rotationSide = -1
        else
            rotationSide = 1
        end
    end

    if(math.abs(gfx.rotation - rotateTo) > options.maxRotationSpeed) then
        instance.movement.rotation = instance.movement.rotation + options.rotationAcc
    else
        instance.movement.rotation = 0
    end

    instance.movement.rotation = math.min(instance.movement.rotation, options.maxRotationSpeed)
    
    local resultRotation = gfx.rotation + instance.movement.rotation * rotationSide

    if(math.abs(resultRotation - rotateTo) < options.rotationAcc) then
        gfx.rotation = fn.deg(rotateTo)
    else
        gfx.rotation = fn.deg(resultRotation)
    end

    local rotationDiff = math.abs(fn.deg(rotateTo) - fn.deg(gfx.rotation))
    if(rotationDiff < options.maxRotationSpeed) then
        gfx.rotation = fn.deg(rotateTo)
        rotationDiff = 0
    end

    return rotationDiff
end

fn.mergeTables = function(table1, table2, copyTables)
    local newTable = {}
    for k, v in pairs(table1) do
        newTable[k] = v
    end
    for k, v in pairs(table2) do
        if(type(v) == "table" and copyTables) then
            newTable[k] = v
        elseif(type(v) ~= "table") then
            newTable[k] = v
        end
    end
    return newTable
end
fn.concatTables = function(t1, t2)
    local newTable = {}
    for k, v in ipairs(t1) do
        table.insert(newTable, v)
    end
    for k, v in ipairs(t2) do
        table.insert(newTable, v)
    end
    return newTable
end
fn.deepcopy = function(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[fn.deepcopy(orig_key)] = fn.deepcopy(orig_value)
        end
        setmetatable(copy, fn.deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end



return fn