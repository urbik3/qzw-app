local game = require("src.game")
local app = require("src.app")
local state = require("src.managers.state")
local json = require("json")
local random = require("lib.random")

local player = {
    listeners = {},
    timers = {},
}

player.getCharacterData = function()
    return player.characters[state.actualCharacter]
end

player.characters = {
    [state.CHARACTER_BASIC] = {
        type = state.CHARACTER_BASIC,
        gfx = {
            spriteName = "character1",
            width = 157,
            height = 223,
        },
        crosshair = "crosshair021",
        maxSpeed = 3,
        persistentLock = true,
        autoShoot = true,
        actions = {
            attack1Interval = 500,
            attack2Interval = 2500,
            attack1 = "shoot1",
            attack2 = "shoot2",
        },
    },
    [state.CHARACTER_MEDIC] = {
        type = state.CHARACTER_MEDIC,
        gfx = {
            spriteName = "character2",
            width = 155,
            height = 247,
        },
        crosshair = "crosshair070",
        maxSpeed = 3,
        persistentLock = true,
        autoShoot = true,
        armor = 2,
        actions = {
            attack1Interval = 250,
            attack2Interval = 15000,
            attack1 = "shoot3",
            attack2 = "heal",
            action2Icon = "plus",
        },
    },
    [state.CHARACTER_ASSAULT] = {
        type = state.CHARACTER_ASSAULT,
        gfx = {
            spriteName = "character3",
            width = 134,
            height = 182,
        },
        crosshair = "crosshair125",
        maxSpeed = 3,
        persistentLock = true,
        autoShoot = true,
        actions = {
            attack1Interval = 500,
            attack2Interval = 40000,
            attack1 = "shoot4",
            attack2 = "supercharge",
            action2Icon = "storm",
        },
    },
    [state.CHARACTER_PYRO] = {
        type = state.CHARACTER_PYRO,
        gfx = {
            spriteName = "character4",
            width = 175,
            height = 229,
        },
        crosshair = "crosshair053",
        crosshairY = -120,
        maxSpeed = 3,
        persistentLock = true,
        autoShoot = true,
        armor = 1,
        actions = {
            attack1Interval = 1500,
            attack2Interval = 5000,
            attack1 = "shoot6",
            action1Icon = "diamond",
            attack2 = "shoot5",
            action2Icon = "rocket",
        },
    },
    [state.CHARACTER_SNIPER] = {
        type = state.CHARACTER_SNIPER,
        gfx = {
            spriteName = "character5",
            width = 135,
            height = 254,
        },
        crosshair = "crosshair094",
        maxSpeed = 3,
        persistentLock = true,
        autoShoot = true,
        actions = {
            attack1Interval = 2500,
            attack2Interval = 5000,
            attack1 = "shoot7",
            attack2 = "shoot8",
            action2Icon = "sight",
        },
    },
}
player.charactersHard = {
    [state.CHARACTER_MEDIC] = {
        armor = 10,
    },
    [state.CHARACTER_PYRO] = {
        armor = 5,
    },
}

player.moveToTouch = false
player.joystickMovement = true

player.create = function(options)
    local characterData = player.getCharacterData()

    local cont = display.newGroup()
    game.playerCont:insert(cont)
    cont.x = display.contentCenterX
    cont.y = display.contentCenterY

    local charSprite = display.newImageRect(cont, "assets/characters/".. characterData.gfx.spriteName ..".png", characterData.gfx.width, characterData.gfx.height)
    charSprite.xScale = game.scale
    charSprite.yScale = game.scale
    charSprite.anchorY = .65

    local crosshair = display.newImageRect(cont, "assets/crosshairs/".. characterData.crosshair ..".png", 128, 128)
    crosshair.xScale = game.scale * 1.5
    crosshair.yScale = game.scale * 1.5
    crosshair.y = characterData.crosshairY == nil and -80 or characterData.crosshairY
    crosshair.x = 3
    if(characterData.persistentLock) then
        crosshair.isVisible = false
    end

    local maxHp = app.state.getPlayer().hp
    local instance = {
        _id = math.getUID(20),
        name = "player",
        type = characterData.type,
        charSprite = charSprite,
        crosshairSprite = crosshair,
        cont = cont,
        radius = 14,
        hp = maxHp,
        maxHp = maxHp,
        hasHealthbar = true,
        isSupercharged = false,
        gfx = {},
        timers = {},
        movement = {
            maxSpeed = characterData.maxSpeed,
            x = 0,
            y = 0,
            moveToX = 0,
            moveToY = 0,
            knockback = false,
            knockbackX = 0,
            knockbackY = 0,
        },
        actions = {
            action1Time = -999999,
            action1Interval = characterData.actions.attack1Interval,
            action1Available = true,
            action2Time = -999999,
            action2Interval = characterData.actions.attack2Interval,
            action2Available = true,
            action1Icon = "rocket",
            action2Icon = "rockets",
            defaultActionBtn = nil,
        }
    }
    instance.actions = app.fn.mergeTables(instance.actions, characterData.actions, false)
    instance = app.fn.mergeTables(instance, characterData, false)

    if((app.state.chapter == app.state.CHAPTER_HARD or app.state.chapter == app.state.CHAPTER_ENDLESS) and player.charactersHard[characterData.type]) then
        instance = app.fn.mergeTables(instance, player.charactersHard[characterData.type], false)
    end

    instance.getPosition = function()
        return {
            x = -game.bgCont.x + display.contentCenterX,
            y = -game.bgCont.y + display.contentCenterY,
        }
    end
    instance.setPosition = function(pos)
        pos.x = instance.getPosition().x - pos.x
        pos.y = instance.getPosition().y - pos.y
        local moveToX = instance.movement.moveToX
        local moveToY = instance.movement.moveToY
        instance.movement.moveToX = pos.x
        instance.movement.moveToY = pos.y
        app.player.update(instance, {}, {})
        instance.movement.moveToX = moveToX
        instance.movement.moveToY = moveToY
    end

    instance.rotateTo = function(degrees)
        instance.cont.rotation = degrees
        if(game.darkness) then
            game.darkness.rotation = degrees
        end
    end

    player.moveEvent(instance)
    player.targetingEvent(instance)
    player.shootEvent(instance)
    app.gui.createHealthbar(instance)
    
    if(options.position) then

    else
        local minPosX = display.actualContentWidth/2 - app.bg.stagePadding
        local minPosY = display.actualContentHeight/2 - app.bg.stagePadding
        local maxPosX = -app.bg.width + app.bg.stagePadding + display.actualContentWidth/2
        local maxPosY = -app.bg.height + app.bg.stagePadding + display.actualContentHeight/2

        instance.movement.moveToX = random(minPosX, maxPosX)
        instance.movement.moveToY = random(minPosY, maxPosY)
        app.player.update(instance, {}, {})
        instance.movement.moveToX = 0
        instance.movement.moveToY = 0
    end

    -- local collisionsCircle = display.newCircle(cont, 0, 0, instance.radius)
    -- collisionsCircle.fill = {1, .5, .5, .5}

    return instance
end

local targetEventListener
player.listeners.targetingFn = nil
player.targetingEvent = function(instance)
    local targetingFactory = function(instance)
        return function(event)
            local touchRadius = 10

            if(not instance) then return true end

            if(instance.movement.stun) then return end

            if(event.phase == "ended") then
                -- TARGET ENEMY
                for k, enemy in pairs(game.enemies) do
                    local enemyPos = enemy.getPosition()
                    local enemyPosX, enemyPosY = game.groundCont:localToContent(enemyPos.x, enemyPos.y)
                    enemyPos = {x = enemyPosX, y = enemyPosY}
                    if(ssk.math2d.distanceBetween(enemyPos, event) < touchRadius + enemy.radius) then
                        instance.rotateTo(app.fn.getAngle(instance.getPosition(), enemy.getPosition()))
                        if(instance.shootOnTouch) then
                            if(instance.actions.action1Time + instance.actions.action1Interval < game.getTime()) then
                                if(instance.actions.action1IntervalToChange) then
                                    instance.actions.action1Interval = instance.actions.action1IntervalToChange
                                    instance.actions.action1IntervalToChange = nil
                                end
                                player.actions[instance.actions.attack1](instance)
                                instance.actions.action1Time = game.getTime()
                                -- app.gui.shootBtnLeft1.height = 0
                                app.gui.shootBtnRight1.height = 0
                                -- app.gui.infoBtnLeft1 = app.gui.infoBtnLeft1:changeState(app.screen.BTN_STATE_DISABLED)
                                app.gui.infoBtnRight1 = app.gui.infoBtnRight1:changeState(app.screen.BTN_STATE_DISABLED)
                                -- transition.to(app.gui.shootBtnLeft1, {time = instance.actions.action1Interval, height = display.actualContentHeight/2})
                                transition.to(app.gui.shootBtnRight1, {time = instance.actions.action1Interval, height = display.actualContentHeight/2, onComplete = function()
                                    if(app.gui.infoBtnRight1 and app.gui.infoBtnRight1.x) then
                                        -- app.gui.infoBtnLeft1 = app.gui.infoBtnLeft1:changeState(app.screen.BTN_STATE_INFOBOX)
                                        app.gui.infoBtnRight1 = app.gui.infoBtnRight1:changeState(app.screen.BTN_STATE_INFOBOX)
                                    end
                                end})
                            end
                        end
                        -- PERSISTENT LOCK
                        if(instance.persistentLock) then
                            instance.lockedOnEnemy = enemy
                            app.gui.lockOnTarget(instance, enemy)
                        end
                    end
                end
    
                -- USE CONSUMABLE
                for k, consumable in pairs(game.consumables) do
                    local consumablePos = consumable.consumableSprite
                    local consumablePosX, consumablePosY = game.groundCont:localToContent(consumablePos.x, consumablePos.y)
                    consumablePos = {x = consumablePosX, y = consumablePosY}
                    if(consumable._inactive == false and ssk.math2d.distanceBetween(consumablePos, event) < touchRadius + consumable.radius) then
                        app.consumable.consume(consumable, instance)
                    end
                end
            end
        end
    end

    targetEventListener = display.newRect(game.moveShootGuiCont, 0, 0, display.actualContentWidth - app.gui.guiSideWidth, display.actualContentHeight)
    targetEventListener.anchorX = 0
    targetEventListener.anchorY = 0
    targetEventListener.isVisible = false
    targetEventListener.isHitTestable = true
    player.targetingFn = targetingFactory(instance)
    targetEventListener:addEventListener("touch", player.targetingFn)
end

local moveEventListener
local shootEventListener
player.moveEvent = function(instance)
    -- CLASSIC CONTROL
    if(not player.joystickMovement) then
        app.gui.joystickCircle = display.newCircle(game.guiCont, display.contentCenterX - display.screenOriginX, display.contentCenterY - display.screenOriginY, app.gui.joystickRadius)
        app.gui.joystickCircle.fill = {.5, .5, 1, .1}
        app.gui.joystickCircle.isVisible = false
        app.gui.joystickCircle.isHitTestable = true

        player.listeners.moveCharacter = function(event)
            local target = event.target
            local phase = event.phase

            if(not instance) then return true end
        
            if ( "began" == phase ) then
                display.currentStage:setFocus(target, event.id)
            elseif ( "moved" == phase ) then
                local eventPos = { x = event.x - display.screenOriginX, y = event.y - display.screenOriginY}
                local direction = ssk.math2d.sub(app.gui.joystickCircle, eventPos)
                if(ssk.math2d.length(direction) > app.gui.joystickSensitivity) then
                    local resultVector
                    if(player.moveToTouch) then
                        local normalized = ssk.math2d.normalize(direction)
                        resultVector = {x = normalized.x * instance.movement.maxSpeed, y = normalized.y * instance.movement.maxSpeed}
                    else
                        local directionOpposite = {x = -direction.x, y = -direction.y}
                        local normalized = ssk.math2d.normalize(directionOpposite)
                        local moveSpeed = app.fn.mapRange(ssk.math2d.length(direction), app.gui.joystickSensitivity, app.gui.joystickSensitivityOuter, 0, 1, true) * instance.movement.maxSpeed
                        resultVector = {x = normalized.x * moveSpeed, y = normalized.y * moveSpeed}
                    end

                    if(instance.movement.stun) then
                        instance.movement.moveToX = 0
                        instance.movement.moveToY = 0
                    else
                        instance.movement.moveToX = resultVector.x
                        instance.movement.moveToY = resultVector.y
                        if(instance.lockedOnEnemy) then
                            instance.rotateTo(app.fn.getAngle(instance.getPosition(), instance.lockedOnEnemy.getPosition()))
                        else
                            instance.rotateTo(ssk.math2d.vector2Angle({x = instance.movement.moveToX, y = instance.movement.moveToY}) + 180)
                        end
                    end
                else
                    instance.movement.x = 0
                    instance.movement.y = 0
                    instance.movement.moveToX = 0
                    instance.movement.moveToY = 0
                end
            else
                instance.movement.x = 0
                instance.movement.y = 0
                instance.movement.moveToX = 0
                instance.movement.moveToY = 0
                display.currentStage:setFocus(target, nil)
            end
        end

        app.gui.joystickCircle:addEventListener("touch", player.listeners.moveCharacter)
    else
        -- JOYSTICK CONTROL
        app.gui.joystickCircle = display.newCircle(game.radarCont, display.contentCenterX - display.screenOriginX, display.contentCenterY - display.screenOriginY, app.gui.joystickSensitivityOuter)
        app.gui.joystickCircle.fill = {.5, .5, 1, .2}
        app.gui.joystickCircle.stroke = {.5, .5, 1, .5}
        app.gui.joystickCircle.strokeWidth = 2
        app.gui.joystickCircle.x = display.actualContentWidth/2
        app.gui.joystickCircle.y = display.actualContentHeight/2

        app.gui.joystickCircleInner = display.newCircle(game.radarCont, display.contentCenterX - display.screenOriginX, display.contentCenterY - display.screenOriginY, app.gui.joystickRadius - app.gui.joystickSensitivity)
        app.gui.joystickCircleInner.fill = {.5, .5, 1, .2}
        app.gui.joystickCircleInner.x = display.actualContentWidth/2
        app.gui.joystickCircleInner.y = display.actualContentHeight/2
        
        app.gui.joystickCircle.alpha = 0
        app.gui.joystickCircleInner.alpha = 0

        moveEventListener = display.newRect(game.moveShootGuiCont, 0, 0, display.actualContentWidth - app.gui.guiSideWidth, display.actualContentHeight)
        moveEventListener.anchorX = 0
        moveEventListener.anchorY = 0
        moveEventListener.isVisible = false
        moveEventListener.isHitTestable = true

        shootEventListener = display.newRect(game.moveShootGuiCont, 0, 0, display.actualContentWidth - app.gui.guiSideWidth, display.actualContentHeight)
        shootEventListener.anchorX = 0
        shootEventListener.anchorY = 0
        shootEventListener.isVisible = false
        shootEventListener.isHitTestable = true

        player.listeners.moveCharacter = function(event)

            if(not instance) then return true end

            local target = event.target
            local phase = event.phase
        
            local eventPos = { x = event.x - display.screenOriginX, y = event.y - display.screenOriginY}
            
            if ( "began" == phase ) then
                app.gui.joystickCircle.x = eventPos.x
                app.gui.joystickCircle.y = eventPos.y
                app.gui.joystickCircleInner.x = eventPos.x
                app.gui.joystickCircleInner.y = eventPos.y
            elseif ( "moved" == phase ) then
                if(app.fn.distance(app.gui.joystickCircle, eventPos) < 2) then
                    return
                end
                display.currentStage:setFocus(target, event.id)
                app.gui.joystickCircle.alpha = 1
                app.gui.joystickCircleInner.alpha = 1

                if(app.fn.distance(app.gui.joystickCircle, eventPos) > app.gui.joystickRadius - app.gui.joystickSensitivity) then
                    local pos = app.fn.multiplyVector(
                        app.fn.polarToCartesian(1,
                            app.fn.toRadian(app.fn.getAngle(eventPos, app.gui.joystickCircle) + 90)
                        ), app.gui.joystickRadius - app.gui.joystickSensitivity
                    )
                    app.gui.joystickCircleInner.x = pos.x + app.gui.joystickCircle.x
                    app.gui.joystickCircleInner.y = pos.y + app.gui.joystickCircle.y
                else
                    app.gui.joystickCircleInner.x = eventPos.x
                    app.gui.joystickCircleInner.y = eventPos.y
                end

                local direction = ssk.math2d.sub(app.gui.joystickCircle, eventPos)
                if(ssk.math2d.length(direction) > app.gui.joystickSensitivity) then
                    
                    local normalized = ssk.math2d.normalize(direction)
                    local moveSpeed = app.fn.mapRange(ssk.math2d.length(direction), app.gui.joystickSensitivity, app.gui.joystickSensitivityOuter, 0, 1, true) * instance.movement.maxSpeed
                    local resultVector = {x = normalized.x * moveSpeed, y = normalized.y * moveSpeed}

                    if(instance.movement.stun) then
                        instance.movement.moveToX = 0
                        instance.movement.moveToY = 0
                    else
                        instance.movement.moveToX = resultVector.x
                        instance.movement.moveToY = resultVector.y
                        if(instance.lockedOnEnemy) then
                            instance.rotateTo(app.fn.getAngle(instance.getPosition(), instance.lockedOnEnemy.getPosition()))
                        else
                            instance.rotateTo(ssk.math2d.vector2Angle({x = instance.movement.moveToX, y = instance.movement.moveToY}) + 180)
                        end
                    end
                end
            else
                if(app.gui.joystickCircle and instance) then
                    app.gui.joystickCircle.alpha = 0
                    app.gui.joystickCircleInner.alpha = 0
                    instance.movement.x = 0
                    instance.movement.y = 0
                    instance.movement.moveToX = 0
                    instance.movement.moveToY = 0
                    display.currentStage:setFocus(target, nil)
                end
            end
        end
        moveEventListener:addEventListener("touch", player.listeners.moveCharacter)
    end
end

player.shootEvent = function(instance)
    local btnFill = {.73, .23, .01, .75}
    local btnMarginX = 33

    -- INIT VARS
    local panelLeft
    local panelRight
    local infoBtnLeft1
    local infoBtnLeft2
    local leaveBtnright
    local settingsBtnRight
    local infoBtnRight1
    local infoBtnRight2
    local zoomOutBtnRight
    local timeBtnRight
    local healthBtnRight
    local infoBtnHeight = 18

    -- ACTION 1
    local height = display.actualContentHeight/2 - infoBtnHeight
    local width = app.gui.guiSideWidth
    -- local actionBtnLeft1 = display.newRect(app.game.guiCont, 0, display.actualContentHeight/2, width, height)
    -- actionBtnLeft1.anchorX = 0
    -- actionBtnLeft1.anchorY = 1
    -- actionBtnLeft1.fill = btnFill
    -- -- shootBtnLeft.isVisible = false
    -- -- actionBtnLeft1.isHitTestable = true
    -- app.gui.shootBtnLeft1 = actionBtnLeft1
    
    local actionBtnRight1 = display.newRect(app.game.moveShootGuiCont, display.actualContentWidth - width, display.actualContentHeight/2, width, height)
    actionBtnRight1.anchorX = 0
    actionBtnRight1.anchorY = 1
    actionBtnRight1.fill = btnFill
    -- shootBtnRight.isVisible = false
    -- actionBtnRight1.isHitTestable = true
    app.gui.shootBtnRight1 = actionBtnRight1

    player.listeners.action1 = function(event)
        
        if(not instance) then return true end

        if(instance.movement.stun) then return end

        if ("began" == event.phase) then
            if(instance.actions.action1Time + instance.actions.action1Interval < game.getTime() and instance.actions.action1Available) then
                if(instance.actions.action1IntervalToChange) then
                    instance.actions.action1Interval = instance.actions.action1IntervalToChange
                    instance.actions.action1IntervalToChange = nil
                end
                player.actions[instance.actions.attack1](instance)
                instance.actions.action1Time = game.getTime()
                -- actionBtnLeft1.height = 0
                actionBtnRight1.height = 0
                -- app.gui.infoBtnLeft1 = app.gui.infoBtnLeft1:changeState(app.screen.BTN_STATE_DISABLED)
                app.gui.infoBtnRight1 = app.gui.infoBtnRight1:changeState(app.screen.BTN_STATE_DISABLED)
                -- transition.to(actionBtnLeft1, {time = instance.actions.action1Interval, height = height})
                transition.to(actionBtnRight1, {time = instance.actions.action1Interval, height = height, onComplete = function() 
                    if(app.gui.infoBtnRight1 and app.gui.infoBtnRight1.x) then
                        -- app.gui.infoBtnLeft1 = app.gui.infoBtnLeft1:changeState(app.screen.BTN_STATE_INFOBOX)
                        app.gui.infoBtnRight1 = app.gui.infoBtnRight1:changeState(app.screen.BTN_STATE_INFOBOX)
                    end
                end})
            end
        end
    end
    -- actionBtnLeft1:addEventListener("touch", player.listeners.action1)
    actionBtnRight1:addEventListener("touch", player.listeners.action1)
    instance.actions.defaultActionBtn = actionBtnRight1

    -- -- ACTION 2
    local height = display.actualContentHeight/2 - infoBtnHeight
    local width = app.gui.guiSideWidth
    -- local actionBtnLeft2 = display.newRect(app.game.guiCont, 0, display.actualContentHeight/2, width, height)
    -- actionBtnLeft2.anchorX = 0
    -- actionBtnLeft2.anchorY = 0
    -- actionBtnLeft2.fill = btnFill
    -- -- actionBtnLeft.isVisible = false
    -- -- actionBtnLeft2.isHitTestable = true
    -- app.gui.actionBtnLeft2 = actionBtnLeft2
    
    local actionBtnRight2 = display.newRect(app.game.moveShootGuiCont, display.actualContentWidth - width, display.actualContentHeight/2, width, height)
    actionBtnRight2.anchorX = 0
    actionBtnRight2.anchorY = 0
    actionBtnRight2.fill = btnFill
    -- actionBtnRight.isVisible = false
    -- actionBtnRight2.isHitTestable = true
    app.gui.actionBtnRight2 = actionBtnRight2

    player.listeners.action2 = function(event)

        if(not instance) then return true end

        if(instance.movement.stun) then return end

        if ("began" == event.phase) then
            if(instance.actions.action2Time + instance.actions.action2Interval < game.getTime() and instance.actions.action2Available) then
                if(instance.actions.action2IntervalToChange) then
                    instance.actions.action2Interval = instance.actions.action2IntervalToChange
                    instance.actions.action2IntervalToChange = nil
                end
                player.actions[instance.actions.attack2](instance)
                instance.actions.action2Time = game.getTime()
                -- actionBtnLeft2.height = 0
                actionBtnRight2.height = 0
                -- app.gui.infoBtnLeft2 = app.gui.infoBtnLeft2:changeState(app.screen.BTN_STATE_DISABLED)
                app.gui.infoBtnRight2 = app.gui.infoBtnRight2:changeState(app.screen.BTN_STATE_DISABLED)
                -- transition.to(actionBtnLeft2, {time = instance.actions.action2Interval, height = height})
                transition.to(actionBtnRight2, {time = instance.actions.action2Interval, height = height, onComplete = function()
                    if(app.gui.actionBtnRight2 and app.gui.actionBtnRight2.x) then
                        if(instance.type == state.CHARACTER_ASSAULT and instance.isSupercharged) then
                        else
                            -- app.gui.infoBtnLeft2 = app.gui.infoBtnLeft2:changeState(app.screen.BTN_STATE_INFOBOX)
                            app.gui.infoBtnRight2 = app.gui.infoBtnRight2:changeState(app.screen.BTN_STATE_INFOBOX)
                        end
                    end
                end})
            end
        end
    end
    -- actionBtnLeft2:addEventListener("touch", player.listeners.action2)
    actionBtnRight2:addEventListener("touch", player.listeners.action2)

    -- PANEL LEFT
    -- panelLeft = display.newImageRect(app.game.guiCont, "assets/ui/panel-left.png", 140, 1185)
    -- panelLeft.xScale = .5
    -- panelLeft.yScale = .5
    -- panelLeft.anchorX = 0
    -- panelLeft.x = 0
    -- panelLeft.y = display.actualContentHeight/2

    -- infoBtnLeft1 = app.screen.createBtn(app.game.guiCont, instance.actions.action1Icon, function()end, app.screen.BTN_STATE_INFOBOX)
    -- infoBtnLeft1.x = btnMarginX
    -- infoBtnLeft1.y = infoBtnLeft1.height/2

    -- infoBtnLeft2 = app.screen.createBtn(app.game.guiCont, instance.actions.action2Icon, function()end, app.screen.BTN_STATE_INFOBOX)
    -- infoBtnLeft2.x = btnMarginX
    -- infoBtnLeft2.y = display.actualContentHeight - infoBtnLeft2.height/2

    leaveBtnright = app.screen.createBtn(app.game.zoomOutContHidden, "menu", function()
        app.screens.confirmation.show()
        return true
    end)
    leaveBtnright.x = display.actualContentWidth - btnMarginX
    leaveBtnright.y = display.actualContentHeight/2 + 60

    settingsBtnRight = app.screen.createBtn(app.game.zoomOutContHidden, "settings", function()
        app.screens.settings.show(true)
        return true
    end)
    settingsBtnRight.x = display.actualContentWidth - btnMarginX
    settingsBtnRight.y = display.actualContentHeight/2 - 60

    -- PANEL RIGHT
    panelRight = display.newImageRect(app.game.moveShootGuiCont, "assets/ui/panel-left.png", 140, 1185)
    panelRight.xScale = .5
    panelRight.yScale = .5
    panelRight.anchorX = 0
    panelRight.rotation = 180
    panelRight.x = display.actualContentWidth
    panelRight.y = display.actualContentHeight/2

    infoBtnRight1 = app.screen.createBtn(app.game.moveShootGuiCont, instance.actions.action1Icon, function()end, app.screen.BTN_STATE_INFOBOX)
    infoBtnRight1.x = display.actualContentWidth - btnMarginX
    infoBtnRight1.y = infoBtnRight1.height/2

    infoBtnRight2 = app.screen.createBtn(app.game.moveShootGuiCont, instance.actions.action2Icon, function()end, app.screen.BTN_STATE_INFOBOX)
    infoBtnRight2.x = display.actualContentWidth - btnMarginX
    infoBtnRight2.y = display.actualContentHeight - infoBtnRight2.height/2

    zoomOutBtnRight = app.screen.createBtn(app.game.zoomOutCont, "planet", game.zoomAnimation, app.screen.BTN_STATE_NORMAL)
    zoomOutBtnRight.x = display.actualContentWidth - btnMarginX
    zoomOutBtnRight.y = display.actualContentHeight/2

    timeBtnRight = app.screen.createTextBtn(app.game.zoomOutCont, "0", function()end)
    timeBtnRight.x = display.actualContentWidth - btnMarginX
    timeBtnRight.y = infoBtnHeight/2
    timeBtnRight.xScale = .55
    timeBtnRight.yScale = .55

    healthBtnRight = app.screen.createTextBtn(app.game.zoomOutCont, "0", function()end)
    healthBtnRight.x = display.actualContentWidth - btnMarginX
    healthBtnRight.y = display.actualContentHeight - infoBtnHeight/2
    healthBtnRight.xScale = .55
    healthBtnRight.yScale = .55

    -- MAX HEALTH ARC INDICATOR
    app.gui.createArcHealthIndicator(instance)
    
    -- local debugBtnRight = app.screen.createTextBtn(app.game.guiCont, "0", function()end)
    -- debugBtnRight.x = display.actualContentWidth - btnMarginX
    -- debugBtnRight.y = display.actualContentHeight/2 + 27
    -- debugBtnRight.xScale = .55
    -- debugBtnRight.yScale = .55

    -- app.gui.debugBtnRight = debugBtnRight
    app.gui.healthBtnRight = healthBtnRight
    app.gui.timeBtnRight = timeBtnRight
    app.gui.infoBtnRight1 = infoBtnRight1
    -- app.gui.infoBtnLeft1 = infoBtnLeft1
    app.gui.infoBtnRight2 = infoBtnRight2
    -- app.gui.infoBtnLeft2 = infoBtnLeft2
    app.gui.zoomOutBtnRight = zoomOutBtnRight
end

player.actions = {
    shoot1 = function(instance)
        local shot = app.shot.create(instance, "shotPlayerBasic", {initialDegrees = 6, initialPosition = 35, isPlayerShot = true})
        table.insert(game.shots, shot)
    end,
    shoot2 = function(instance)
        table.insert(game.shots, app.shot.create(instance, "shotPlayerShotgun", {initialDegrees = 8, initialPosition = 28, isPlayerShot = true}))
        table.insert(game.shots, app.shot.create(instance, "shotPlayerShotgun", {initialDegrees = 8, initialPosition = 28, shotAngle = 8, isPlayerShot = true}))
        table.insert(game.shots, app.shot.create(instance, "shotPlayerShotgun", {initialDegrees = 8, initialPosition = 28, shotAngle = -8, isPlayerShot = true}))
    end,
    shoot3 = function(instance)
        table.insert(game.shots, app.shot.create(instance, "shotPlayerMedic", {initialDegrees = 1, initialPosition = 28, isPlayerShot = true}))
    end,
    shoot4 = function(instance)
        table.insert(game.shots, app.shot.create(instance, "shotPlayerAssault", {initialDegrees = 8, initialPosition = 28, isPlayerShot = true}))
    end,
    shoot5 = function(instance)
        table.insert(game.shots, app.shot.create(instance, "shotPlayerRocket", {initialDegrees = 8, initialPosition = 28, explodeDistance = 120, isPlayerShot = true}))
    end,
    shoot6 = function(instance)
        local shoot = function(i)
            local rotation = math.sin(i)*12
            table.insert(game.shots, app.shot.create(instance, "shotPlayerGas", {initialDegrees = 8, initialPosition = 28, isPlayerShot = true, shotAngle = rotation}))
        end

        local i = 0
        local loop
        loop = function()
            if(i < 25) then
                i = i + 1
                shoot(i)
                player.timers.shoot6 = timer.performWithDelay(40, loop, 1)
            else
                player.timers.shoot6 = nil
            end
        end
        loop()
    end,
    shoot7 = function(instance)
        table.insert(game.shots, app.shot.create(instance, "shotPlayerSniper", {initialDegrees = 8, initialPosition = 28, isPlayerShot = true}))
    end,
    shoot8 = function(instance)
        table.insert(game.shots, app.shot.create(instance, "shotPlayerSniperUltra", {initialDegrees = 8, initialPosition = 28, isPlayerShot = true}))
    end,
    heal = function(instance)
        local healTo = math.min(instance.hp + instance.maxHp/2, instance.maxHp)

        local updateHealthbar = function()
            app.gui.updateHealthbarSize(instance)
        end
        local removeEnterFrame = function()
            transition.cancel(instance.healing)
            instance.healing = nil
            Runtime:removeEventListener("enterFrame", updateHealthbar)
        end
        instance.healing = transition.to(instance, {hp = healTo, time = 5000, onComplete = removeEnterFrame, onCancel = removeEnterFrame})
        instance.removeHealing = removeEnterFrame
        Runtime:addEventListener("enterFrame", updateHealthbar)
    end,
    supercharge = function(instance, time)
        local time = time and time or 5000
        instance.isSupercharged = true
        if(instance.type == state.CHARACTER_ASSAULT) then
            app.gui.infoBtnRight2 = app.gui.infoBtnRight2:changeState(app.screen.BTN_STATE_DISABLED)
            -- app.gui.infoBtnLeft2 = app.gui.infoBtnLeft2:changeState(app.screen.BTN_STATE_DISABLED)
            instance.actions.action2Available = false
        end

        local maxSpeed = instance.movement.maxSpeed
        -- instance.movement.maxSpeed = maxSpeed*2
        local action1Interval = instance.actions.action1IntervalToChange and instance.actions.action1IntervalToChange or instance.actions.action1Interval
        instance.actions.action1Interval = instance.actions.action1IntervalToChange and instance.actions.action1IntervalToChange/2 or instance.actions.action1Interval/2
        local action2Interval = instance.actions.action2IntervalToChange and instance.actions.action2IntervalToChange or instance.actions.action2Interval
        instance.actions.action2Interval = instance.actions.action2IntervalToChange and instance.actions.action2IntervalToChange/2 or instance.actions.action2Interval/2

        local supercharge = display.newImageRect(instance.cont, "assets/effects/supercharge.png", 472, 472)
        supercharge.xScale = game.scale * .65
        supercharge.yScale = game.scale * .65
        transition.from(supercharge, {alpha = 0, time = 500})
        player.timers.supercharge = timer.performWithDelay(time, function()
            instance.movement.maxSpeed = maxSpeed
            instance.actions.action1IntervalToChange = action1Interval
            instance.actions.action2IntervalToChange = action2Interval
            if(instance.type == state.CHARACTER_ASSAULT) then
                if(instance.actions.action2Time + instance.actions.action2Interval < game.getTime()) then
                    app.gui.infoBtnRight2 = app.gui.infoBtnRight2:changeState(app.screen.BTN_STATE_INFOBOX)
                    -- app.gui.infoBtnLeft2 = app.gui.infoBtnLeft2:changeState(app.screen.BTN_STATE_INFOBOX)
                end
                instance.actions.action2Available = true
            end
            instance.isSupercharged = false
            transition.to(supercharge, {alpha = 0, time = 500, onComplete = function()
                if(supercharge.x) then
                    supercharge:removeSelf()
                    supercharge = nil
                end
            end})
        end, 1)
    end,
}

player.update = function(instance, consumables, terrain)
    local minX = display.screenOriginX + display.actualContentWidth/2 - app.bg.stagePadding
    local minY = display.screenOriginY + display.actualContentHeight/2 - app.bg.stagePadding
    local maxX = -app.bg.width + display.actualContentWidth/2 + display.screenOriginX + app.bg.stagePadding
    local maxY = -app.bg.height + display.actualContentHeight/2 + display.screenOriginY + app.bg.stagePadding

    if(not instance.movement.stun) then
        instance.movement.x = instance.movement.moveToX
        instance.movement.y = instance.movement.moveToY
    else
        instance.movement.x = 0
        instance.movement.y = 0
    end

    if(instance.movement.knockback) then
        instance.movement.x = instance.movement.knockbackX
        instance.movement.y = instance.movement.knockbackY
    end

    -- TERRAIN
    for i, item in pairs(terrain) do
        if(ssk.math2d.distanceBetween(instance.getPosition(), item.cont) < item.radius) then
            instance.movement.x = instance.movement.x * item.effect.movementUpdated
            instance.movement.y = instance.movement.y * item.effect.movementUpdated
            if(item.type == app.terrain.TYPE_TRAP and not item.effect.isActive) then
                item.effect.isActive = true
                timer.performWithDelay(item.effect.breakTrapAfter, function()
                    app.terrain.breakTrap(item)
                end)
            end
        end
    end

    -- GROUND MOVEMENT
    game.groundCont.x = game.groundCont.x + instance.movement.x
    game.groundCont.y = game.groundCont.y + instance.movement.y
    game.groundCont.x = math.min(game.groundCont.x, minX)
    game.groundCont.y = math.min(game.groundCont.y, minY)
    game.groundCont.x = math.max(game.groundCont.x, maxX)
    game.groundCont.y = math.max(game.groundCont.y, maxY)

    -- ENEMIES MOVEMENT
    game.enemiesCont.x = game.enemiesCont.x + instance.movement.x
    game.enemiesCont.y = game.enemiesCont.y + instance.movement.y
    game.enemiesCont.x = math.min(game.enemiesCont.x, minX)
    game.enemiesCont.y = math.min(game.enemiesCont.y, minY)
    game.enemiesCont.x = math.max(game.enemiesCont.x, maxX)
    game.enemiesCont.y = math.max(game.enemiesCont.y, maxY)

    -- ENEMIES MOVEMENT
    game.projectilesCont.x = game.projectilesCont.x + instance.movement.x
    game.projectilesCont.y = game.projectilesCont.y + instance.movement.y
    game.projectilesCont.x = math.min(game.projectilesCont.x, minX)
    game.projectilesCont.y = math.min(game.projectilesCont.y, minY)
    game.projectilesCont.x = math.max(game.projectilesCont.x, maxX)
    game.projectilesCont.y = math.max(game.projectilesCont.y, maxY)

    -- BG MOVEMENT
    game.bgCont.x = game.bgCont.x + instance.movement.x
    game.bgCont.y = game.bgCont.y + instance.movement.y
    game.bgCont.x = math.min(game.bgCont.x, minX)
    game.bgCont.y = math.min(game.bgCont.y, minY)
    game.bgCont.x = math.max(game.bgCont.x, maxX)
    game.bgCont.y = math.max(game.bgCont.y, maxY)

    -- CONSUMABLES
    for k, consumable in pairs(consumables) do
        if(consumable._inactive == false and ssk.math2d.distanceBetween(consumable.consumableSprite, instance.getPosition()) < instance.radius + consumable.radius) then
            app.consumable.consume(consumable, instance)
        end
    end

    -- PERSISTENT LOCK
    if(instance.lockedOnEnemy) then
        instance.rotateTo(app.fn.getAngle(instance.getPosition(), instance.lockedOnEnemy.getPosition()))
        local visionPaddingRight = app.gui.guiSideWidth - 25
        local visionPaddingLeft = -25
        local visionPaddingY = -25
        local enemyPositionX, enemyPositionY = game.enemiesCont:localToContent(instance.lockedOnEnemy.getPosition().x, instance.lockedOnEnemy.getPosition().y)
        enemyPositionX = enemyPositionX - display.screenOriginX
        enemyPositionY = enemyPositionY - display.screenOriginY
        if(enemyPositionX < visionPaddingLeft or enemyPositionY < visionPaddingY or enemyPositionX > display.actualContentWidth - visionPaddingRight or enemyPositionY > display.actualContentHeight - visionPaddingY) then
            app.gui.destroyLockOn()
            instance.lockedOnEnemy = nil
        end

        if(instance.autoShoot) then
            instance.actions.defaultActionBtn:dispatchEvent({name = "touch", target = instance.actions.defaultActionBtn, phase = "began"})
        end
    end
end

player.destroy = function(instance)
    instance.cont:removeSelf()
    instance = nil

    targetEventListener:removeEventListener("touch", player.targetingFn)
    if(player.joystickMovement) then
        moveEventListener:removeEventListener("touch", player.listeners.moveCharacter)
    else
        app.gui.joystickCircle:removeEventListener("touch", player.listeners.moveCharacter)
    end
    
    -- app.gui.shootBtnLeft1:removeEventListener("touch", player.listeners.action1)
    app.gui.shootBtnRight1:removeEventListener("touch", player.listeners.action1)
    -- app.gui.actionBtnLeft2:removeEventListener("touch", player.listeners.action2)
    app.gui.actionBtnRight2:removeEventListener("touch", player.listeners.action2)

    if(player.timers.shoot6) then
        timer.cancel(player.timers.shoot6)
        player.timers.shoot6 = nil
    end
    if(player.timers.supercharge) then
        timer.cancel(player.timers.supercharge)
        player.timers.supercharge = nil
    end
    if(player.timers.stun) then
        timer.cancel(player.timers.stun)
    end
    if(player.timers.knockback) then
        timer.cancel(player.timers.knockback)
    end

    app.gui.actionBtnRight = nil
    -- app.gui.actionBtnLeft = nil
    app.gui.shootBtnRight = nil
    -- app.gui.shootBtnLeft = nil

    app.gui.infoBtnRight1 = nil
    -- app.gui.infoBtnLeft1 = nil
    app.gui.infoBtnRight2 = nil
    -- app.gui.infoBtnLeft2 = nil

    app.gui.joystickCircle = nil
    app.gui.joystickCircleInner = nil
end

return player