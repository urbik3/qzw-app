local app = require("src.app")
local game = require("src.game")

local turret = {}

turret.init = function(position, active, isObjective, options)
    if(active == nil) then active = true end
    if(isObjective == nil) then isObjective = true end
    if(options == nil) then options = {} end

    local instance = {
        _id = math.getUID(20),
        _deleted = false,
        name = "turret",
        anchorY = .5,
        anchorX = .5,
        radius = 16,
        isMetal = true,
        knockback = false,
        active = active,
        toActivateRadius = 50,
        timers = {},
        gfx = {
            initialShotDegrees = 0,
            initialShotPosition = 0,
            shotScale = 1,
        },
        movement = {
            rotation = 0,
        },
        ai = {
            shootWhen = false,
            shootDelay = 500,
            noticeDistance = 250,
            isExplosive = true,
        },
    }

    local cont = display.newGroup()
    game.groundCont:insert(cont)
    cont.xScale = game.scale
    cont.yScale = game.scale
    cont.anchorY = instance.anchorY
    cont.anchorX = instance.anchorX
    cont.x = position.x
    cont.y = position.y
    cont.rotation = math.random(0, 360)
    instance.cont = cont

    local charSprite = display.newImageRect(cont, "assets/objects/turret.png", 195, 279)
    instance.charSprite = charSprite

    instance.getPosition = function()
        return {x = cont.x, y = cont.y}
    end

    if(isObjective) then
        app.gui.createTurretGuide(game.playerInstance, instance)
    end

    if(options.hp) then
        instance.hp = options.hp
        instance.maxHp = options.hp
        app.gui.createHealthbar(instance)
    end

    if(not active) then
        local activateCircle = display.newCircle(game.groundCont, instance.cont.x, instance.cont.y, instance.toActivateRadius)
        activateCircle.fill = {1, .5, .5, .1}
        activateCircle.stroke = {1, .5, .5, .5}
        activateCircle.strokeWidth = 2
        instance.activateCircle = activateCircle
    end

    return instance
end

turret.update = function(instance, player, enemies)
    if(instance.active) then
        if(instance.targetEnemy) then
            local angleBetween = app.fn.rotateTo(instance, instance.cont, instance.targetEnemy.getPosition(), {maxRotationSpeed = 5, rotationAcc = .5})
            if(angleBetween < 5 and instance.ai.shootWhen and instance.ai.shootWhen < game.getTime()) then
                local shot = app.shot.create(instance, "shotTurret", {
                    initialDegrees = -5,
                    initialPosition = 35,
                    scale = 1,
                    isPlayerShot = true,
                })
                table.insert(game.shots, shot)
                local shot = app.shot.create(instance, "shotTurret", {
                    initialDegrees = 5,
                    initialPosition = 35,
                    scale = 1,
                    isPlayerShot = true,
                })
                table.insert(game.shots, shot)
                instance.ai.shootWhen = false
            elseif(not instance.ai.shootWhen) then
                instance.ai.shootWhen = game.getTime() + instance.ai.shootDelay
            end
        else
            local nearestEnemyDistance = 99999999
            local nearestEnemy = nil
            for i, enemy in pairs(enemies) do
                local distance = ssk.math2d.distanceBetween(instance.cont, enemy.getPosition())
                if(distance < nearestEnemyDistance) then
                    nearestEnemyDistance = distance
                    nearestEnemy = enemy
                end
            end
            if(nearestEnemyDistance < instance.ai.noticeDistance) then
                instance.targetEnemy = nearestEnemy
            end
        end
    elseif(not instance.timers.activate) then
        if(ssk.math2d.distanceBetween(player.getPosition(), instance.getPosition()) < instance.toActivateRadius) then
            game.objectiveComplete("findTurret")
            app.gui.removeObjectiveTurret(instance)
            instance.timers.activate = timer.performWithDelay(60000, function()
                instance.active = true
                instance.timers.activate = nil
            end)
            transition.to(instance.activateCircle, {time = 500, xScale = .001, yScale = .001, onComplete = function()
                if(instance.activateCircle and instance.activateCircle.x) then
                    instance.activateCircle:removeSelf()
                end
                instance.activateCircle = nil
            end})
        end
    end
    app.gui.updateHealthbarPosition(instance)
    app.gui.updateHealthbarSize(instance)
end

return turret