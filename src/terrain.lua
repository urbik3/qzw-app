local app = require("src.app")
local game = require("src.game")
local json = require("json")

local terrain = {}

terrain.TYPE_OIL = "terrainOil"
terrain.TYPE_TRAP = "terrainTrap"

terrain.type = {}
terrain.type[terrain.TYPE_OIL] = {
    effect = {
        movementUpdated = .4,
    },
}
terrain.type[terrain.TYPE_TRAP] = {
    effect = {
        movementUpdated = 0,
        breakTrapAfter = 4000,
        isActive = false,
    },
}

terrain.init = function(position, radius, type)
    if(type == nil) then type = terrain.TYPE_OIL end

    local instance = {
        _id = math.getUID(20),
        _deleted = false,
        anchorY = .5,
        anchorX = .5,
        type = type,
        effect = {},
    }

    instance.effect = app.fn.mergeTables(instance.effect, terrain.type[type].effect)

    local cont = display.newGroup()
    game.groundCont:insert(cont)
    cont.anchorY = instance.anchorY
    cont.anchorX = instance.anchorX
    cont.x = position.x
    cont.y = position.y
    instance.cont = cont

    instance.getPosition = function()
        return {x = cont.x, y = cont.y}
    end

    local gfx = display.newCircle(cont, 0, 0, radius)
    gfx.fill = {1, .5, .5, .1}
    gfx.stroke = {1, .5, .5, .5}
    gfx.strokeWidth = 3
    instance.gfx = gfx

    instance.radius = radius

    return instance
end

terrain.breakTrap = function(instance)
    instance.cont:removeSelf()
    instance._deleted = true
    local filteredTerrains = {}
    for k, item in pairs(game.terrain) do
        if(item._deleted == false) then
            table.insert(filteredTerrains, item)
        end
    end
    game.terrain = filteredTerrains
    instance = nil
end

return terrain