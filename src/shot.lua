local app = require("src.app")
local game = require("src.game")

local shot = {
    shotStagePadding = 100,
    gfx = {}
}

shot.init = function()
    shot.gfx.explosion = graphics.newImageSheet("assets/effects/explosion.png", {
        width = 200,
        height = 200,
        numFrames = 3*10
    })
end

shot.types = {
    shotPlayerBasic = {
        gfx = {
            imagePath = "assets/shots/shot1.png",
            imageWidth = 86,
            imageHeight = 226,
            anchorY = .15,
        },
        damage = 5,
    },
    shotPlayerShotgun = {
        gfx = {
            imagePath = "assets/shots/shot7.png",
            imageWidth = 111,
            imageHeight = 100,
            anchorY = .5,
        },
        radius = 8,
        speed = 6,
        damage = 8,
    },
    shotPlayerMedic = {
        gfx = {
            imagePath = "assets/shots/shot2.png",
            imageWidth = 172,
            imageHeight = 183,
            anchorY = .45,
        },
        radius = 5,
        speed = 6,
        damage = 3,
        fadeOutStart = 110,
        fadeOutLength = 40,
    },
    shotPlayerAssault = {
        gfx = {
            imagePath = "assets/shots/shot1.png",
            imageWidth = 86,
            imageHeight = 226,
            anchorY = .15,
        },
        damage = 8,
    },
    shotPlayerRocket = {
        gfx = {
            imagePath = "assets/shots/shot8.png",
            imageWidth = 246,
            imageHeight = 360,
            anchorY = .35,
        },
        radius = 11,
        damage = 50,
        speed = 3,
        isExplosive = true,
        blastRadius = 80,
        hitOnCollision = false,
        knockback = 200,
        knockbackDuration = 200,
    },
    shotPlayerGas = {
        gfx = {
            imagePath = "assets/shots/shot9.png",
            imageWidth = 112,
            imageHeight = 408,
            anchorY = .2,
        },
        radius = 6,
        damage = 1,
        speed = 2,
        fadeOutStart = 110,
        fadeOutLength = 40,
        scale = .75,
    },
    shotPlayerSniper = {
        gfx = {
            imagePath = "assets/shots/shot5.png",
            imageWidth = 14,
            imageHeight = 65,
            anchorY = .15,
        },
        damage = 18,
    },
    shotPlayerSniperUltra = {
        gfx = {
            imagePath = "assets/shots/shot6.png",
            imageWidth = 69,
            imageHeight = 224,
            anchorY = .15,
        },
        damage = 30,
        speed = 12,
        stun = 5000,
        stunColor = "red",
        knockback = 400,
        knockbackDuration = 800,
    },
    shotBasic = {
        gfx = {
            imagePath = "assets/shots/shot1.png",
            imageWidth = 86,
            imageHeight = 226,
            anchorY = .15,
        },
        damage = 5,
    },
    shotHeavy = {
        gfx = {
            imagePath = "assets/shots/shot4.png",
            imageWidth = 221,
            imageHeight = 384,
            anchorY = .7,
            rotation = 180,
        },
        radius = 8,
        damage = 20,
        stun = 500,
        stunColor = "blue",
        knockback = 200,
        knockbackDuration = 200,
    },
    shotMutant = {
        gfx = {
            imagePath = "assets/shots/shot4.png",
            imageWidth = 221,
            imageHeight = 384,
            anchorY = .7,
            rotation = 180,
        },
        radius = 8,
        damage = 30,
        stun = 1000,
        stunColor = "blue",
        knockback = 200,
        knockbackDuration = 200,
    },
    shotLaser = {
        gfx = {
            imagePath = "assets/shots/shot5.png",
            imageWidth = 14,
            imageHeight = 65,
            anchorY = .15,
        },
        damage = 3,
    },
    shotSniper = {
        gfx = {
            imagePath = "assets/shots/shot6.png",
            imageWidth = 69,
            imageHeight = 224,
            anchorY = .15,
        },
        damage = 200,
        speed = 12,
        stun = 2000,
        stunColor = "red",
        knockback = 500,
        knockbackDuration = 500,
    },
    shotCannoneer = {
        gfx = {
            imagePath = "assets/shots/shot8.png",
            imageWidth = 246,
            imageHeight = 360,
            anchorY = .35,
        },
        radius = 11,
        damage = 50,
        speed = 2,
        isExplosive = true,
        blastRadius = 80,
        hitOnCollision = false,
        knockback = 200,
        knockbackDuration = 200,
    },
    shotMedic = {
        gfx = {
            imagePath = "assets/shots/shot10.png",
            imageWidth = 106,
            imageHeight = 571,
            anchorY = .1,
        },
        radius = 8,
        damage = 15,
        speed = 3,
        isHeal = true,
    },
    shotNebulizer = {
        gfx = {
            imagePath = "assets/shots/shot4.png",
            imageWidth = 221,
            imageHeight = 384,
            anchorY = .7,
            rotation = 180,
        },
        radius = 8,
        damage = 60,
        stun = 4000,
    },
    shotExplosive = {
        gfx = {
            imagePath = "assets/shots/shot8.png",
            imageWidth = 0,
            imageHeight = 0,
            anchorY = .35,
        },
        radius = 8,
        damage = 50,
        speed = .1,
        isExplosive = true,
        knockback = 200,
        knockbackDuration = 200,
        blastRadius = 100,
        hitOnCollision = false,
    },
    shotTurret = {
        gfx = {
            imagePath = "assets/shots/shot1.png",
            imageWidth = 86,
            imageHeight = 226,
            anchorY = .15,
        },
        radius = 8,
        damage = 5,
        speed = 8,
    },
}

shot.typesHard = {
    shotBasic = {
        damage = 20,
    },
    shotLaser = {
        damage = 15,
    },
    shotCannoneer = {
        speed = 3,
        blastRadius = 100,
        damage = 100,
    },
}

shot.create = function(character, shotType, options)
    if(options.scale == nil) then options.scale = 1 end

    local instance = {
        name = "shot",
        shotType = shotType,
        _deleted = false,
        radius = 5,
        speed = 6,
        damage = 5,
        isExplosive = false,
        isPlayerShot = options.isPlayerShot,
        explodeDistance = options.explodeDistance,
        positionStart = character.getPosition(),
        fadeOutStart = 9999,
        fadeOutLength = 0,
        hitOnCollision = true,
        scale = 1,
        stun = 0,
        isHeal = false,
        gfx = {
            rotation = 0
        },
    }

    instance.gfx = app.fn.mergeTables(instance.gfx, shot.types[shotType].gfx)
    instance = app.fn.mergeTables(instance, shot.types[shotType])

    if((app.state.actualChapter == app.state.CHAPTER_HARD or app.state.actualChapter == app.state.CHAPTER_ENDLESS) and shot.typesHard[shotType]) then
        instance = app.fn.mergeTables(instance, shot.typesHard[shotType])
    end

    local cont = display.newGroup()
    game.projectilesCont:insert(cont)
    local normalizedVectorPosition = ssk.math2d.angle2Vector(character.cont.rotation + options.initialDegrees, true) -- TODO: rotation is nil after death
    local scaledVectorPosition = ssk.math2d.scale(normalizedVectorPosition, options.initialPosition)
    cont.x = character.getPosition().x + scaledVectorPosition.x
    cont.y = character.getPosition().y + scaledVectorPosition.y
    cont.xScale = instance.scale
    cont.yScale = instance.scale
    cont.rotation = character.cont.rotation
    if(options.shotAngle) then
        cont.rotation = cont.rotation + options.shotAngle
    end
    cont:toBack()
    instance.cont = cont

    local charSprite = display.newImageRect(cont, instance.gfx.imagePath, instance.gfx.imageWidth, instance.gfx.imageHeight)
    charSprite.x = 0
    charSprite.y = 0
    charSprite.xScale = game.scale * options.scale
    charSprite.yScale = game.scale * options.scale
    charSprite.anchorY = instance.gfx.anchorY
    charSprite.rotation = instance.gfx.rotation
    instance.charSprite = charSprite

    instance.getPosition = function()
        return {x = cont.x, y = cont.y}
    end

    -- local collisionsCircle = display.newCircle(cont, 0, 0, instance.radius)
    -- collisionsCircle.fill = {1, .5, .5, .5}

    return instance
end

shot.update = function(instance, player, enemies, turret)
    local newPos = app.fn.polarToCartesian(instance.speed, app.fn.toRadian(instance.cont.rotation - 90))
    instance.cont.x = instance.cont.x + newPos.x
    instance.cont.y = instance.cont.y + newPos.y

    -- BOUNDARIES
    local contentX, contentY = instance.cont:localToContent(0, 0)
    if(contentX < -shot.shotStagePadding + display.screenOriginX or contentX > display.contentWidth + shot.shotStagePadding - display.screenOriginX) then
        shot.destroy(instance)
    end
    if(contentY < -shot.shotStagePadding + display.screenOriginY or contentY > display.contentHeight + shot.shotStagePadding - display.screenOriginY) then
        shot.destroy(instance)
    end

    -- EXPLODE
    local explode = function(pos)
        local explosionEffect = display.newSprite(game.projectilesCont, shot.gfx.explosion, {
            name = "explode",
            start = 1,
            count = 3*10,
            time = 700,
            loopCount = 1,
        })
        explosionEffect.x = pos.x
        explosionEffect.y = pos.y
        explosionEffect.xScale = .5
        explosionEffect.yScale = .5
        explosionEffect.rotation = math.random(0, 360)
        explosionEffect:play()
        explosionEffect:addEventListener("sprite", function(event)
            if(event.phase == "ended") then
                explosionEffect:removeSelf()
                explosionEffect = nil
            end
        end)
        app.sound.playSFX("explode")
    end

    -- COLLISION
    if(instance.hitOnCollision) then
        local characters
        if(instance.isPlayerShot) then
            characters = enemies
        elseif(instance.shotType == "shotMedic") then
            characters = enemies
        else
            characters = {player}
            if(turret) then
                table.insert(characters, turret)
            end
        end
        local filteredCharacters = {}
        for k, character in pairs(characters) do
            local ePos = character.getPosition()
            local iPos = instance.getPosition()
            if(ssk.math2d.distanceBetween(ePos, iPos) < instance.radius + character.radius) then
                table.insert(filteredCharacters, character)
            end
        end
        for k, character in pairs(filteredCharacters) do
            if(instance.isExplosive == true) then
                explode(instance.cont)
            end
            if(instance.isHeal) then
                character.hp = math.min(character.hp + instance.damage, character.maxHp)
                app.gui.updateHealthbarSize(character)
            else
                app.enemy.takeDamage(character, instance)
            end
            shot.destroy(instance)
        end
    end

    -- EXPLOSIVE
    local distanceFromStart = ssk.math2d.distanceBetween(instance.positionStart, instance.cont)
    if(instance.isExplosive == true and instance.explodeDistance and distanceFromStart >= instance.explodeDistance) then
        local characters = app.fn.concatTables(enemies, {player})
        for k, character in pairs(characters) do
            local ePos = character.getPosition()
            if(ssk.math2d.distanceBetween(ePos, instance.cont) < instance.blastRadius) then
                app.enemy.takeDamage(character, instance)
            end
        end
        explode(instance.cont)
        shot.destroy(instance)
    end

    -- FADEOUT
    local distanceFromStart = ssk.math2d.distanceBetween(instance.positionStart, instance.cont)
    if(distanceFromStart > instance.fadeOutStart) then
        instance.cont.alpha = app.fn.mapRange(distanceFromStart, instance.fadeOutStart, instance.fadeOutStart + instance.fadeOutLength, 1, 0)
        if(instance.cont.alpha <= 0) then
            shot.destroy(instance)
        end
    end
end

shot.destroy = function(instance)
    instance.cont:removeSelf()
    instance._deleted = true
    local filteredShots = {}
    for k, shot in pairs(game.shots) do
        if(shot._deleted == false) then
            table.insert(filteredShots, shot)
        end
    end
    game.shots = filteredShots
    instance = nil
end

return shot