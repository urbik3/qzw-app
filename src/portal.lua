local app = require("src.app")
local game = require("src.game")

local portal = {}

portal.STATUS_IDLE = "status_idle"
portal.STATUS_ACTIVATED = "status_activated"
portal.STATUS_READY = "status_ready"

portal.spriteSheet = graphics.newImageSheet("assets/objects/portal.png", {
    width = 128,
    height = 128,
    numFrames = 5*5
})

portal.create = function(position, options)
    local sprite = display.newSprite(game.groundCont, portal.spriteSheet, {
        name = "glow",
        start = 1,
        count = 5*5,
        time = 1500,
        loopCount = 0,
    })
    sprite.x = position.x
    sprite.y = position.y
    sprite:toBack()
    sprite:play()

    local instance = {
        radius = 20,
        sprite = sprite,
        clearRadius = 500,
        status = portal.STATUS_IDLE,
    }

    app.gui.createPortalGuide(game.playerInstance, instance)
    
    -- TUTORIAL
    -- if(game.levelNumber == 5 and app.state.actualCharacter == app.state.CHARACTER_BASIC) then
        game.stop()
        app.tutorial.createPopup(app.data.lang.get("continue"), app.data.lang.get("tutorialFindPortal"), function()
            app.tutorial.destroyPopup()
            game.run()
        end)
    -- end

    return instance
end

portal.update = function(instance, player, enemies)
    if(instance.status == portal.STATUS_IDLE and ssk.math2d.distanceBetween(player.getPosition(), instance.sprite) <= player.radius + instance.radius) then
        local clearCircle = display.newCircle(game.groundCont, instance.sprite.x, instance.sprite.y, instance.clearRadius)
        clearCircle.fill = {1, .5, .5, .1}
        clearCircle.stroke = {1, .5, .5, .5}
        clearCircle.strokeWidth = 4
        clearCircle.xScale = .001
        clearCircle.yScale = .001
        transition.to(clearCircle, {time = 2000, xScale = 1, yScale = 1})
        instance.clearCircle = clearCircle
        instance.status = portal.STATUS_ACTIVATED
        
        -- TUTORIAL
        if(game.levelNumber == 5 and app.state.actualCharacter == app.state.CHARACTER_BASIC) then
            game.stop()
            app.tutorial.createPopup(app.data.lang.get("continue"), app.data.lang.get("tutorialPortalClear"), function()
                app.tutorial.destroyPopup()
                game.run()
            end)
        end
    elseif(instance.status == portal.STATUS_ACTIVATED) then
        local enemiesToClear = {}
        for k, enemy in pairs(enemies) do
            if(ssk.math2d.distanceBetween(enemy.getPosition(), instance.sprite) < instance.clearRadius) then
                table.insert(enemiesToClear, enemy)
            end
        end
        if(#enemiesToClear <= 0) then
            instance.status = portal.STATUS_READY
            transition.to(instance.clearCircle, {time = 5000, xScale = .001, yScale = .001, onComplete = function()
                if(instance.clearCircle.x) then
                    instance.clearCircle:removeSelf()
                end
                instance.clearCircle = nil
            end})
        end
    elseif(instance.status == portal.STATUS_READY and ssk.math2d.distanceBetween(player.getPosition(), instance.sprite) <= player.radius + instance.radius) then
        game.objectiveComplete("portal")
    end
end




return portal