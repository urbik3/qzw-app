local app = require("src.app")
local game = require("src.game")
local state = require("src.managers.state")
local json = require("json")

local level = {
    [state.CHAPTER_TUTORIAL] = {},
    [state.CHAPTER_CASUAL] = {},
    [state.CHAPTER_HARD] = {},
    [state.CHAPTER_ENDLESS] = {},
}

level.levelNumber = 1
level.chapter = state.CHAPTER_CASUAL
level.endlessLoop = nil
level.objectiveStack = {}

level.init = function(levelNumber, chapter)
    level.chapter = chapter
    level.levelNumber = levelNumber
    local config = level[level.chapter]["config"..levelNumber]
    app.sound.playMusic(config.music)
    app.bg[level.chapter]["init"..levelNumber]()
    level[level.chapter]["init"..levelNumber](config)
end

level.reset = function()
    level.objectiveStack = {}
end

level.xMin = 0
level.xMax = 0
level.yMin = 0
level.yMax = 0
level.spawn = function(enemyType, number, isObjective, clearRadius, consumableDrop, spawnedCb, radiusOptions)
    local enemiesSpawned = {}
    if(number > 0) then
        for i = 1, number, 1 do
            local position
            if(radiusOptions) then
                position = level.getRadiusPosition(radiusOptions.position, radiusOptions.radius, radiusOptions.minRadius)
            else
                position = level.getMapPosition(clearRadius)
            end
            local enemy
            if(spawnedCb == true) then
                enemy = app.enemy.create(position, enemyType, {dropsConsumable = consumableDrop, spawnedCb = function()
                    table.insert(game.enemies, enemy)
                    if(isObjective) then
                        table.insert(game.objectiveEnemies, enemy)
                        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
                    end
                end})
            else
                enemy = app.enemy.create(position, enemyType, {dropsConsumable = consumableDrop, spawnedCb = spawnedCb})
                table.insert(game.enemies, enemy)
                if(isObjective) then
                    table.insert(game.objectiveEnemies, enemy)
                    app.gui.addObjectiveEnemy(game.playerInstance, enemy)
                end
            end
            table.insert(enemiesSpawned, enemy)
        end
    end
    return enemiesSpawned
end

level.getMapPosition = function(clearRadius)
    local position
    local maxIterations = 50000
    local iteration = 1
    repeat
        iteration = iteration + 1
        position = {x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}
        local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
    until(distance > clearRadius or iteration > maxIterations)
    return position
end

level.getRadiusPosition = function(position, radius, minRadius)
    minRadius = minRadius and minRadius or 50
    local maxIterations = 50000
    local iteration = 1
    local positionEnemy
    repeat
        iteration = iteration + 1
        local positionAngle = math.random(1, 360)
        local positionRadius = math.random(minRadius, radius)
        positionEnemy = ssk.math2d.scale(ssk.math2d.angle2Vector(positionAngle, true), positionRadius)
        positionEnemy.x = positionEnemy.x + position.x
        positionEnemy.y = positionEnemy.y + position.y
    until((positionEnemy.x > level.xMin and positionEnemy.x < level.xMax and positionEnemy.y > level.yMin and positionEnemy.y < level.yMax) or iteration > maxIterations)

    return positionEnemy
end

-- 
-- CHAPTER TUTORIAL
-- 

level[state.CHAPTER_TUTORIAL].config1 = {
    music = "RB - Run",
    clearRadius = 300,
}

level[state.CHAPTER_TUTORIAL].init1 = function(config)
    game.playerInstance = app.player.create({position = {x = -display.actualContentWidth/2, y = -display.actualContentHeight/2}})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

        
    local enemy = app.enemy.create({x = 800, y = 800}, app.enemy.ENEMY_BASIC, {dropsConsumable = app.consumable.TYPE_ARMOR})
    table.insert(game.enemies, enemy)
    table.insert(game.objectiveEnemies, enemy)
    app.gui.addObjectiveEnemy(game.playerInstance, enemy)

    if(app.state.actualCharacter == app.state.CHARACTER_BASIC) then
        local consumable = app.consumable.create({x = 800, y = 720}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_BASIC})
        if(consumable) then
            table.insert(game.consumables, consumable)
        end

        local consumable = app.consumable.create({x = 720, y = 800}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_SOLDIER})
        if(consumable) then
            table.insert(game.consumables, consumable)
        end
    end

    local newObjective = function()
        local enemy
        enemy = app.enemy.create({x = app.game.playerInstance.getPosition().x - 100, y = app.game.playerInstance.getPosition().y}, app.enemy.ENEMY_SOLDIER, {dropsConsumable = app.consumable.TYPE_HEAL, spawnedCb = function()
            table.insert(game.enemies, enemy)
            table.insert(game.objectiveEnemies, enemy)
            app.gui.addObjectiveEnemy(game.playerInstance, enemy)
        end})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, true, config.clearRadius, 0, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 5, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        local heavyPosition = {x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}
        local enemyHeavy
        enemyHeavy = app.enemy.create(heavyPosition, app.enemy.ENEMY_HEAVY, {dropsConsumable = app.consumable.TYPE_HEAL, spawnedCb = function()
            table.insert(game.enemies, enemyHeavy)
            table.insert(game.objectiveEnemies, enemyHeavy)
            app.gui.addObjectiveEnemy(game.playerInstance, enemyHeavy)
        end})
        level.spawn(app.enemy.ENEMY_SOLDIER, 5, false, config.clearRadius, 0, true, {position = heavyPosition, radius = 150})

        if(app.state.actualCharacter == app.state.CHARACTER_BASIC) then
            local consumable = app.consumable.create({x = app.game.playerInstance.getPosition().x - 50, y = app.game.playerInstance.getPosition().y - 50}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_HEAVY})
            if(consumable) then
                table.insert(game.consumables, consumable)
            end
        end
    end
    table.insert(app.level.objectiveStack, newObjective)

    -- TUTORIAL
    if(app.state.actualCharacter == app.state.CHARACTER_BASIC) then
        app.tutorial.createPopup(app.data.lang.get("next"), app.data.lang.get("tutorialHowToPlay1"), function()
            app.tutorial.destroyPopup()
            app.tutorial.createPopup(app.data.lang.get("next"), app.data.lang.get("tutorialHowToPlay2"), function()
                app.tutorial.destroyPopup()
                app.tutorial.createPopup(app.data.lang.get("startPlaying"), app.data.lang.get("tutorialHowToPlay3"), function()
                    app.tutorial.destroyPopup()
                end)
            end)
        end)
    elseif(app.state.actualCharacter == app.state.CHARACTER_ASSAULT) then
        app.tutorial.createPopup(app.data.lang.get("startPlaying"), app.data.lang.get("tutorialHowToPlayAssault"), function()
            app.tutorial.destroyPopup()
        end)
    elseif(app.state.actualCharacter == app.state.CHARACTER_SNIPER) then
        app.tutorial.createPopup(app.data.lang.get("startPlaying"), app.data.lang.get("tutorialHowToPlaySniper"), function()
            app.tutorial.destroyPopup()
        end)
    elseif(app.state.actualCharacter == app.state.CHARACTER_MEDIC) then
        app.tutorial.createPopup(app.data.lang.get("startPlaying"), app.data.lang.get("tutorialHowToPlayMedic"), function()
            app.tutorial.destroyPopup()
        end)
    elseif(app.state.actualCharacter == app.state.CHARACTER_PYRO) then
        app.tutorial.createPopup(app.data.lang.get("startPlaying"), app.data.lang.get("tutorialHowToPlayPyro"), function()
            app.tutorial.destroyPopup()
        end)
    end
end

-- 
-- CHAPTER CASUAL
-- 

level[state.CHAPTER_CASUAL].config1 = {
    music = "RB - Run",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init1 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding

    for i = 1, 10, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 5
        local enemy
        enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end
end

level[state.CHAPTER_CASUAL].config2 = {
    music = "RB - Run",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init2 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding

    for i = 1, 20, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 10
        local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end

    for i = 1, 2, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 2 -- NUTNO ABY PADALY Z KAŽDÉHO SOLDIERA CONSUMABLES KVŮLI TUTORIALU
        local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end
end

level[state.CHAPTER_CASUAL].config3 = {
    music = "RB - Run",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init3 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding

    for i = 1, 15, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 5
        local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 7, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 4
        local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end

    game.addLightning(.5)
    transition.to(game.darkness, {alpha = .95, time = 5*60*1000})

    -- INFO POTION
    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin,yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_SCOUT})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config4 = {
    music = "RB - Run",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init4 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding

    for i = 1, 30, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 10
        local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 10, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 5
        local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 5, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyScout", {})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end

    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin,yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_SCOUT})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config5 = {
    music = "RB - Run",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init5 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding

    for i = 1, 30, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 10
        local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 15, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 8
        local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 7, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyScout", {})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end

    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin, yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_STEALTH})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end

    local newObjective = function()
        local positionPortal
        repeat
            positionPortal = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(positionPortal, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        game.portal = app.portal.create(positionPortal, {})

        for i = 1, 10, 1 do
            local positionEnemy
            repeat
                local positionAngle = math.random(1, 360)
                local positionRadius = math.random(50, game.portal.clearRadius)
                positionEnemy = ssk.math2d.scale(ssk.math2d.angle2Vector(positionAngle, true), positionRadius)
                positionEnemy.x = positionEnemy.x + positionPortal.x
                positionEnemy.y = positionEnemy.y + positionPortal.y
            until(positionEnemy.x > xMin and positionEnemy.x < xMax and positionEnemy.y > yMin and positionEnemy.y < yMax)

            local dropsConsumable = i <= 3
            local enemy
            enemy = app.enemy.create(positionEnemy, "enemyBasic", {dropsConsumable = dropsConsumable, spawnedCb = function()
                table.insert(game.enemies, enemy)
            end})
        end

        for i = 1, 3, 1 do
            local positionEnemy
            repeat
                local positionAngle = math.random(1, 360)
                local positionRadius = math.random(20, game.portal.clearRadius)
                positionEnemy = ssk.math2d.scale(ssk.math2d.angle2Vector(positionAngle, true), positionRadius)
                positionEnemy.x = positionEnemy.x + positionPortal.x
                positionEnemy.y = positionEnemy.y + positionPortal.y
            until(positionEnemy.x > xMin and positionEnemy.x < xMax and positionEnemy.y > yMin and positionEnemy.y < yMax)

            local dropsConsumable = i <= 2
            local enemy
            enemy = app.enemy.create(positionEnemy, "enemySoldier", {dropsConsumable = dropsConsumable, spawnedCb = function()
                table.insert(game.enemies, enemy)
            end})
        end
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_CASUAL].config6 = {
    music = "RB - Rapture",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init6 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding
    level.xMin = xMin
    level.xMax = xMax
    level.yMin = yMin
    level.yMax = yMax

    for i = 1, 15, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 7
        local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 7, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 4
        local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 3, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyScout", {})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 3, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyStealth", {})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SCOUT, 6, true, config.clearRadius, 3, true, {position = game.playerInstance.getPosition(), radius = display.contentWidth/2, minRadius = 50})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_STEALTH, 8, true, config.clearRadius, nil, true, {position = game.playerInstance.getPosition(), radius = display.contentWidth/2, minRadius = 50})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SOLDIER, 10, true, config.clearRadius, 0, true, {position = game.playerInstance.getPosition(), radius = display.contentWidth/2, minRadius = 50})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin, yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_STEALTH})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config7 = {
    music = "RB - Rapture",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init7 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding

    for i = 1, 20, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 7
        local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 10, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 5
        local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 5, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyScout", {})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 3, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyStealth", {})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end

    local newObjective = function()
        for i = 1, 20, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > config.clearRadius)
            local dropsConsumable = i <= 7
            local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
            table.insert(game.enemies, enemy)
        end
    
        for i = 1, 8, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > config.clearRadius)
            local dropsConsumable = i <= 4
            local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
            table.insert(game.enemies, enemy)
        end
        for i = 1, 3, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > config.clearRadius)
            local enemy = app.enemy.create(position, "enemyStealth", {})
            table.insert(game.enemies, enemy)
            table.insert(game.objectiveEnemies, enemy)
            app.gui.addObjectiveEnemy(game.playerInstance, enemy)
        end
    end
    table.insert(app.level.objectiveStack, newObjective)

    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin, yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_CANNONEER})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config8 = {
    music = "RB - Rapture",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init8 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding

    -- OIL & TRAP
    local oilRadius = 100
    table.insert(game.terrain,
        app.terrain.init({
            x = math.random(xMin + oilRadius/2, xMax - oilRadius/2),
            y = math.random(yMin + oilRadius/2, yMax - oilRadius/2)},
            oilRadius, app.terrain.TYPE_OIL))
    table.insert(game.terrain, app.terrain.init({x = math.random(xMin, xMax), y = math.random(yMin, yMax)}, 10, app.terrain.TYPE_TRAP))

    -- OBSTACLES
    local holeRadius = 50
    table.insert(game.obstacles,
        app.obstacle.spawn({
            x = math.random(xMin + holeRadius/2, xMax - holeRadius/2),
            y = math.random(yMin + holeRadius/2, yMax - holeRadius/2)},
            app.obstacle.TYPE_CIRCLE, {radius = holeRadius}))

    for i = 1, 15, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 5
        table.insert(game.enemies, app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable}))
    end
    for i = 1, 10, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 5
        local soldier = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, soldier)
    end
    for i = 1, 2, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local scout = app.enemy.create(position, "enemyScout", {})
        table.insert(game.enemies, scout)
    end
    for i = 1, 4, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyStealth", {})
        table.insert(game.enemies, enemy)
        table.insert(game.objectiveEnemies, enemy)
        app.gui.addObjectiveEnemy(game.playerInstance, enemy)
    end

    local newObjective = function()
        for i = 1, 15, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > config.clearRadius)
            local dropsConsumable = i <= 5
            local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
            table.insert(game.enemies, enemy)
        end
    
        for i = 1, 10, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > config.clearRadius)
            local dropsConsumable = i <= 5
            local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
            table.insert(game.enemies, enemy)
        end
        for i = 1, 2, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > 600)
            local enemy = app.enemy.create(position, "enemyCannoneer", {})
            table.insert(game.enemies, enemy)
            table.insert(game.objectiveEnemies, enemy)
            app.gui.addObjectiveEnemy(game.playerInstance, enemy)
        end
    end
    table.insert(app.level.objectiveStack, newObjective)

    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin, yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_CANNONEER})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end

    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin, yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_NEBULIZER})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config9 = {
    music = "RB - Rapture",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init9 = function(config)
    game.playerInstance = app.player.create({position = false})

    local xMin = app.bg.stagePadding
    local xMax = app.bg.width - app.bg.stagePadding
    local yMin = app.bg.stagePadding
    local yMax = app.bg.height - app.bg.stagePadding
    level.xMin = xMin
    level.xMax = xMax
    level.yMin = yMin
    level.yMax = yMax

    for i = 1, 30, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 10
        local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 15, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local dropsConsumable = i <= 7
        local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 5, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyScout", {})
        table.insert(game.enemies, enemy)
    end

    for i = 1, 5, 1 do
        local position
        repeat
            position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
            local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        local enemy = app.enemy.create(position, "enemyStealth", {})
        table.insert(game.enemies, enemy)
    end
    level.spawn("enemyCannoneer", 5, true, config.clearRadius, 0)

    local newObjective = function()
        for i = 1, 20, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > config.clearRadius)
            local dropsConsumable = i <= 7
            local enemy = app.enemy.create(position, "enemyBasic", {dropsConsumable = dropsConsumable})
            table.insert(game.enemies, enemy)
        end
    
        for i = 1, 8, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > config.clearRadius)
            local dropsConsumable = i <= 4
            local enemy = app.enemy.create(position, "enemySoldier", {dropsConsumable = dropsConsumable})
            table.insert(game.enemies, enemy)
        end
        for i = 1, 3, 1 do
            local position
            repeat
                position = {x = math.random(xMin, xMax), y = math.random(yMin,yMax)}
                local distance = ssk.math2d.distanceBetween(position, game.playerInstance.getPosition())
            until(distance > config.clearRadius)
            local enemy = app.enemy.create(position, "enemyStealth", {})
            table.insert(game.enemies, enemy)
        end
        level.spawn("enemyCannoneer", 5, true, config.clearRadius, 0)
    end
    table.insert(app.level.objectiveStack, newObjective)

    level.spawn(app.enemy.ENEMY_NEBULIZER, 3, true, config.clearRadius)

    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin, yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_HEAVY})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end

    local consumable = app.consumable.create({x = math.random(xMin, xMax), y = math.random(yMin, yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_NEBULIZER})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config10 = {
    music = "RB - Rapture",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init10 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn("enemyBasic", 50, false, config.clearRadius, 5)
    level.spawn("enemySoldier", 15, false, config.clearRadius, 7)
    level.spawn("enemyScout", 15, false, config.clearRadius, 0)
    level.spawn("enemyStealth", 15, false, config.clearRadius, 0)
    level.spawn("enemyCannoneer", 5, false, config.clearRadius, 0)
    
    level.spawn("enemyBasic", 1, true, config.clearRadius, 0)
    level.spawn("enemySoldier", 1, true, config.clearRadius, 0)
    level.spawn("enemyScout", 1, true, config.clearRadius, 0)
    level.spawn("enemyStealth", 1, true, config.clearRadius, 0)
    level.spawn("enemyCannoneer", 1, true, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_NEBULIZER, 1, true, config.clearRadius)
    level.spawn(app.enemy.ENEMY_HEAVY, 1, true, config.clearRadius, 0)

    local newObjective = function()
        local positionPortal
        repeat
            positionPortal = {x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}
            local distance = ssk.math2d.distanceBetween(positionPortal, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        game.portal = app.portal.create(positionPortal, {})

        level.spawn("enemyCannoneer", 4, false, config.clearRadius, 0, true, {position = positionPortal, radius = game.portal.clearRadius, minRadius = 50})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local consumable = app.consumable.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_HEAVY})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end

    local consumable = app.consumable.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_MEDIC})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config11 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init11 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn("enemyBasic", 25, false, config.clearRadius, 10)
    level.spawn("enemySoldier", 15, false, config.clearRadius, 7)
    level.spawn("enemyScout", 5, false, config.clearRadius, 0)
    level.spawn("enemyStealth", 5, false, config.clearRadius, 0)
    level.spawn("enemyCannoneer", 5, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_NEBULIZER, 3, true, config.clearRadius)
    level.spawn("enemyMedic", 5, true, config.clearRadius, 0)
    level.spawn("enemyHeavy", 1, true, config.clearRadius, 0)

    local consumable = app.consumable.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_MEDIC})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config12 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init12 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn("enemySoldier", 15, false, config.clearRadius, 7)
    level.spawn(app.enemy.ENEMY_HEAVY, 3, true, config.clearRadius, 0)
    level.spawn("enemyCannoneer", 5, true, config.clearRadius, 0)
    level.spawn("enemyMedic", 5, false, config.clearRadius, 0)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_CANNONEER, 8, true, config.clearRadius, 3, true, {position = game.playerInstance.getPosition(), radius = display.contentWidth/2, minRadius = 50})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_NEBULIZER, 6, true, config.clearRadius, nil, true, {position = game.playerInstance.getPosition(), radius = display.contentWidth/2, minRadius = 50})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_HEAVY, 3, true, config.clearRadius, 0, true, {position = game.playerInstance.getPosition(), radius = display.contentWidth/2, minRadius = 50})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local consumable = app.consumable.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_SNIPER})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end

    local consumable = app.consumable.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_MUTANT})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config13 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init13 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn("enemyBasic", 15, false, config.clearRadius, 5)
    level.spawn("enemySoldier", 15, false, config.clearRadius, 7)
    level.spawn("enemyScout", 5, false, config.clearRadius, 0)
    level.spawn("enemyStealth", 5, false, config.clearRadius, 0)
    level.spawn("enemyCannoneer", 8, false, config.clearRadius, 0)
    level.spawn("enemyMedic", 3, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_NEBULIZER, 3, false, config.clearRadius)
    level.spawn("enemySniper", 3, true, config.clearRadius + 100, 0)

    local newObjective = function()
        local enemies = level.spawn("enemyMutant", 1, true, config.clearRadius + 200, 0, false)
        level.spawn("enemyStealth", 5, false, config.clearRadius, 0, false, {position = enemies[1].getPosition(), radius = display.contentWidth/2})
        level.spawn("enemySoldier", 5, false, config.clearRadius, 0, false, {position = enemies[1].getPosition(), radius = display.contentWidth/2})
        level.spawn("enemyScout", 5, false, config.clearRadius, 0, false, {position = enemies[1].getPosition(), radius = display.contentWidth/2})
    end
    table.insert(app.level.objectiveStack, newObjective)

    local consumable = app.consumable.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_SNIPER})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end

    local consumable = app.consumable.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.consumable.TYPE_INFO, 0, {enemy = app.enemy.ENEMY_MUTANT})
    if(consumable) then
        table.insert(game.consumables, consumable)
    end
end

level[state.CHAPTER_CASUAL].config14 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init14 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn("enemyBasic", 15, false, config.clearRadius, 5)
    level.spawn("enemySoldier", 25, false, config.clearRadius, 12)
    level.spawn("enemyScout", 2, false, config.clearRadius, 0)
    level.spawn("enemyStealth", 50, false, config.clearRadius, 0)
    level.spawn("enemyCannoneer", 2, true, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_HEAVY, 2, true, config.clearRadius, 0)
    level.spawn("enemyMedic", 2, true, config.clearRadius, 0)
    level.spawn("enemySniper", 2, true, config.clearRadius + 100, 0)

    local newObjective = function()
        local enemies = level.spawn("enemyMutant", 1, true, config.clearRadius + 200, 0, false)
        level.spawn("enemyMedic", 5, false, config.clearRadius, 0, false, {position = enemies[1].getPosition(), radius = display.contentWidth/2})
    end
    table.insert(app.level.objectiveStack, newObjective)

    game.addLightning(.5)
    transition.to(game.darkness, {alpha = .95, time = 4*60*1000})
end

level[state.CHAPTER_CASUAL].config15 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 300,
}

level[state.CHAPTER_CASUAL].init15 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn("enemyBasic", 5, true, config.clearRadius, 5)
    level.spawn("enemySoldier", 25, false, config.clearRadius, 25)
    level.spawn("enemyCannoneer", 10, false, config.clearRadius, 0)
    level.spawn("enemyMedic", 5, false, config.clearRadius, 0)
    level.spawn("enemySniper", 5, false, config.clearRadius + 100, 0)
    level.spawn(app.enemy.ENEMY_HEAVY, 3, false, config.clearRadius, 0)

    local newObjective = function()
        local enemies = level.spawn("enemyMutant", 3, true, config.clearRadius + 200, 0, false)
        for k, enemy in pairs(enemies) do
            level.spawn("enemyMedic", 1, false, config.clearRadius, 0, false, {position = enemy.getPosition(), radius = display.contentWidth/2})
            level.spawn(app.enemy.ENEMY_STEALTH, 5, false, config.clearRadius, 0, false, {position = enemy.getPosition(), radius = display.contentWidth/2})
        end
        level.spawn("enemyCannoneer", 10, false, config.clearRadius, 0)
    end
    table.insert(app.level.objectiveStack, newObjective)

    game.addLightning(.9)
end

-- 
-- CHAPTER HARD
-- 

level[state.CHAPTER_HARD].config1 = {
    music = "RB - Run",
    clearRadius = 450,
}

level[state.CHAPTER_HARD].init1 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn(app.enemy.ENEMY_SNIPER, 5, true, config.clearRadius)
    level.spawn(app.enemy.ENEMY_SOLDIER, 10, false, config.clearRadius)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SNIPER, 7, true, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 15, false, 0, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SNIPER, 10, true, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 20, false, 0, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_HARD].config2 = {
    music = "RB - Run",
    clearRadius = 300,
}

level[state.CHAPTER_HARD].init2 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn(app.enemy.ENEMY_BASIC, 50, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_SOLDIER, 10, true, config.clearRadius)
    level.spawn(app.enemy.ENEMY_STEALTH, 30, false, config.clearRadius)
    
    for i = 1, 30, 1 do
        table.insert(game.terrain, app.terrain.init({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, 16, app.terrain.TYPE_TRAP))
    end
    
    for i = 1, 10, 1 do
        table.insert(game.terrain, app.terrain.init({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, math.random(50, 150), app.terrain.TYPE_OIL))
    end

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SOLDIER, 10, true, 0, 10, true)
        level.spawn(app.enemy.ENEMY_STEALTH, 30, false, 0, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SOLDIER, 10, false, 0, 10, true)
        level.spawn(app.enemy.ENEMY_STEALTH, 20, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 5, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_HARD].config3 = {
    music = "RB - Run",
    clearRadius = 300,
}

level[state.CHAPTER_HARD].init3 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn(app.enemy.ENEMY_BASIC, 100, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_SCOUT, 30, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_CANNONEER, 10, true, config.clearRadius)

    level.spawn(app.enemy.ENEMY_EXPLOSIVE, 20, false, config.clearRadius, nil, false)
    
    for i = 1, 15, 1 do
        local oilRadius = math.random(50, 150)
        table.insert(game.terrain,
            app.terrain.init({
                x = math.random(level.xMin + oilRadius/2, level.xMax - oilRadius/2),
                y = math.random(level.yMin + oilRadius/2, level.yMax - oilRadius/2)},
                oilRadius, app.terrain.TYPE_OIL)) 
    end

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SOLDIER, 15, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_SCOUT, 30, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_CANNONEER, 15, true, config.clearRadius)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SOLDIER, 15, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_SCOUT, 30, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_CANNONEER, 15, false, config.clearRadius)
        local positionPortal
        repeat
            positionPortal = {x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}
            local distance = ssk.math2d.distanceBetween(positionPortal, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        game.portal = app.portal.create(positionPortal, {})
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_HARD].config4 = {
    music = "RB - Rapture",
    clearRadius = 300,
}

level[state.CHAPTER_HARD].init4 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn(app.enemy.ENEMY_BASIC, 30, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_SCOUT, 10, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_NEBULIZER, 5, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_MUTANT, 1, true, config.clearRadius)

    for i = 1, 10, 1 do
        local explosive = app.enemy.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.enemy.ENEMY_EXPLOSIVE, {})
        table.insert(game.enemies, explosive) 
    end

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_SCOUT, 10, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_NEBULIZER, 5, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_MUTANT, 1, true, config.clearRadius)

        for i = 1, 10, 1 do
            local explosive = app.enemy.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.enemy.ENEMY_EXPLOSIVE, {})
            table.insert(game.enemies, explosive) 
        end
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_STEALTH, 10, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_NEBULIZER, 5, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_HEAVY, 1, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_MUTANT, 1, true, config.clearRadius)

        for i = 1, 10, 1 do
            local explosive = app.enemy.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.enemy.ENEMY_EXPLOSIVE, {})
            table.insert(game.enemies, explosive) 
        end
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_STEALTH, 10, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_NEBULIZER, 5, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_HEAVY, 2, false, config.clearRadius)
        level.spawn(app.enemy.ENEMY_MUTANT, 1, true, config.clearRadius)
        
        for i = 1, 10, 1 do
            local explosive = app.enemy.create({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, app.enemy.ENEMY_EXPLOSIVE, {})
            table.insert(game.enemies, explosive) 
        end
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_HARD].config5 = {
    music = "RB - Rapture",
    clearRadius = 300,
}

level[state.CHAPTER_HARD].init5 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding
    
    -- TURRET
    game.turret = app.turret.init({x = math.random(level.xMin + level.xMax/4, level.xMax*3/4), y = math.random(level.yMin + level.yMax/4, level.yMax*3/4)}, false, true, {hp = 150000})

    level.spawn(app.enemy.ENEMY_BASIC, 20, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_SOLDIER, 10, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_STEALTH, 7, false, config.clearRadius)
    level.spawn(app.enemy.ENEMY_NEBULIZER, 5, false, config.clearRadius)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 15, false, 0, 5, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 10, false, 0, 10, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 1, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 15, false, 0, 5, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 10, false, 0, 10, true)
        level.spawn(app.enemy.ENEMY_STEALTH, 5, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 2, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 15, false, 0, 15, true)
        level.spawn(app.enemy.ENEMY_STEALTH, 10, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_NEBULIZER, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 3, true, config.clearRadius, 0, true)
        level.spawn(app.enemy.ENEMY_MUTANT, 1, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_HARD].config6 = {
    music = "RB - Rapture",
    clearRadius = 300,
}

level[state.CHAPTER_HARD].init6 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn(app.enemy.ENEMY_BASIC, 20, false, config.clearRadius, 7)
    level.spawn(app.enemy.ENEMY_SOLDIER, 15, false, config.clearRadius, 15)
    level.spawn(app.enemy.ENEMY_SCOUT, 5, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_STEALTH, 10, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_CANNONEER, 2, true, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_MEDIC, 5, false, config.clearRadius, 0)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 15, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 7, false, 0, 7, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 2, true, config.clearRadius, 0, true)
        level.spawn(app.enemy.ENEMY_MEDIC, 3, false, 0, 0)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 15, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 7, false, 0, 7, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 2, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 1, true, config.clearRadius, 0, true)
        level.spawn(app.enemy.ENEMY_MEDIC, 3, false, 0, 0)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 20, false, 0, 20, true)
        level.spawn(app.enemy.ENEMY_SCOUT, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_STEALTH, 5, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 1, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_MEDIC, 5, false, 0, 0)
        local positionPortal
        repeat
            positionPortal = {x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}
            local distance = ssk.math2d.distanceBetween(positionPortal, game.playerInstance.getPosition())
        until(distance > config.clearRadius)
        game.portal = app.portal.create(positionPortal, {})
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_HARD].config7 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 300,
}

level[state.CHAPTER_HARD].init7 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn(app.enemy.ENEMY_BASIC, 10, false, config.clearRadius, 3)
    level.spawn(app.enemy.ENEMY_SOLDIER, 10, false, config.clearRadius, 10)
    level.spawn(app.enemy.ENEMY_SCOUT, 8, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_CANNONEER, 3, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_SNIPER, 3, true, config.clearRadius, 0)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 7, false, 0, 7, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 2, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SNIPER, 3, true, 0, config.clearRadius, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 7, false, 0, 7, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 2, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SNIPER, 3, true, 0, config.clearRadius, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 7, false, 0, 7, true)
        level.spawn(app.enemy.ENEMY_SCOUT, 5, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 2, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SNIPER, 3, true, 0, config.clearRadius, true)
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_HARD].config8 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 300,
}

level[state.CHAPTER_HARD].init8 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn(app.enemy.ENEMY_BASIC, 20, false, config.clearRadius, 7)
    level.spawn(app.enemy.ENEMY_SOLDIER, 20, false, config.clearRadius, 20)
    level.spawn(app.enemy.ENEMY_SCOUT, 8, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_STEALTH, 10, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_CANNONEER, 5, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_MEDIC, 3, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_SNIPER, 3, true, config.clearRadius, 0)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 15, false, 0, 15, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_MEDIC, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SNIPER, 3, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 15, false, 0, 15, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 2, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SNIPER, 3, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 25, false, 0, 25, true)
        level.spawn(app.enemy.ENEMY_SCOUT, 5, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_STEALTH, 7, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 3, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_HARD].config9 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 300,
}

level[state.CHAPTER_HARD].init9 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    level.spawn(app.enemy.ENEMY_BASIC, 20, false, config.clearRadius, 7)
    level.spawn(app.enemy.ENEMY_SOLDIER, 20, false, config.clearRadius, 20)
    level.spawn(app.enemy.ENEMY_CANNONEER, 5, true, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_MEDIC, 3, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_SNIPER, 3, false, config.clearRadius, 0)
    level.spawn(app.enemy.ENEMY_NEBULIZER, 3, true, config.clearRadius, 0)
    
    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 15, false, 0, 15, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_MEDIC, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SNIPER, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 3, true, config.clearRadius, 0, true)
        level.spawn(app.enemy.ENEMY_NEBULIZER, 3, false, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 20, false, 0, 12, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 20, false, 0, 20, true)
        level.spawn(app.enemy.ENEMY_SCOUT, 20, false, 0, 15, true)
        level.spawn(app.enemy.ENEMY_STEALTH, 20, false, 0, 15, true)
        level.spawn(app.enemy.ENEMY_MUTANT, 1, true, config.clearRadius, 0, true)
    end
    table.insert(app.level.objectiveStack, newObjective)

    local newObjective = function()
        level.spawn(app.enemy.ENEMY_BASIC, 10, false, 0, 3, true)
        level.spawn(app.enemy.ENEMY_SOLDIER, 25, false, 0, 25, true)
        level.spawn(app.enemy.ENEMY_HEAVY, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_CANNONEER, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_MEDIC, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_SNIPER, 3, false, 0, 0, true)
        level.spawn(app.enemy.ENEMY_MUTANT, 3, true, config.clearRadius, 15, true)
    end
    table.insert(app.level.objectiveStack, newObjective)
end

level[state.CHAPTER_ENDLESS].config1 = {
    music = "RB - Welcome to Chaos",
    clearRadius = 200,
}

level[state.CHAPTER_ENDLESS].init1 = function(config)
    game.playerInstance = app.player.create({position = false})

    level.xMin = app.bg.stagePadding
    level.xMax = app.bg.width - app.bg.stagePadding
    level.yMin = app.bg.stagePadding
    level.yMax = app.bg.height - app.bg.stagePadding

    local oilRadius = math.round(75, 125)
    table.insert(game.terrain, app.terrain.init(
        {x = math.random(level.xMin + oilRadius, level.xMax - oilRadius),
        y = math.random(level.yMin + oilRadius, level.yMax - oilRadius)}, oilRadius, app.terrain.TYPE_OIL))

    level.spawn(app.enemy.ENEMY_BASIC, 17, false, config.clearRadius*2, nil, false)
    level.spawn(app.enemy.ENEMY_BASIC, 3, false, config.clearRadius*2, app.consumable.TYPE_ARMOR, false)
    level.spawn(app.enemy.ENEMY_SOLDIER, 8, false, config.clearRadius*2, nil, false)
    level.spawn(app.enemy.ENEMY_SOLDIER, 2, false, config.clearRadius*2, app.consumable.TYPE_HEAL, false)
    level.spawn(app.enemy.ENEMY_EXPLOSIVE, 2, false, config.clearRadius, nil, false)

    game.addLightning(0)

    local i = 0
    local spawnRound
    spawnRound = function()
        i = i + 1
        local nextRound = 80000 + i*5000

        level.spawn(app.enemy.ENEMY_EXPLOSIVE, 2, false, config.clearRadius, nil, true)

        local basicNum = math.max(20 - i*2, i)
        level.spawn(app.enemy.ENEMY_BASIC, basicNum, false, config.clearRadius, nil, true)
        
        if(i < 6) then
            level.spawn(app.enemy.ENEMY_BASIC, 3, false, config.clearRadius, app.consumable.TYPE_ARMOR, true)
        end

        if(i < 10) then
            level.spawn(app.enemy.ENEMY_SOLDIER, i*2, false, config.clearRadius, nil, true)
        else
            level.spawn(app.enemy.ENEMY_SOLDIER, 20, false, config.clearRadius, nil, true)
        end
        
        if(i < 6) then
            level.spawn(app.enemy.ENEMY_SOLDIER, 1, false, config.clearRadius, app.consumable.TYPE_HEAL, true)
        end

        if(i == 15) then
            transition.to(game.darkness, {alpha = .9, time = nextRound*5})
            local oilRadius = math.round(75, 125)
            table.insert(game.terrain, app.terrain.init(
                {x = math.random(level.xMin + oilRadius, level.xMax - oilRadius),
                y = math.random(level.yMin + oilRadius, level.yMax - oilRadius)}, oilRadius, app.terrain.TYPE_OIL))
        end

        if(i > 2) then
            local stealthNum = math.min(6, math.round(i+4)) - math.max(i - 15, 0)
            level.spawn(app.enemy.ENEMY_STEALTH, stealthNum, false, config.clearRadius, 0, true)
        end
        if(i > 4) then
            local scoutNum = math.min(4, math.round(i/2)) - math.max(i - 10, 0)
            level.spawn(app.enemy.ENEMY_SCOUT, scoutNum, false, config.clearRadius, 0, true)
        end
        if(i > 6) then
            local cannoneerNum = math.max(math.round(i/2 - 2.5) - math.max(i - 10, 0), 2)
            level.spawn(app.enemy.ENEMY_CANNONEER, cannoneerNum, false, config.clearRadius, 0, true)
        end
        if(i > 8) then
            local medicNum = math.min(math.round(i/3), 5)
            level.spawn(app.enemy.ENEMY_MEDIC, medicNum, false, config.clearRadius, 0, true)
        end
        if(i > 9) then
            local nebulizerNum = math.min(math.round(i/3), 3)
            level.spawn(app.enemy.ENEMY_NEBULIZER, nebulizerNum, false, config.clearRadius, 0, true)
        end
        if(i > 10) then
            local heavyNum = math.min(math.round(i/8), 4)
            level.spawn(app.enemy.ENEMY_HEAVY, heavyNum, false, config.clearRadius, 0, true)
        end
        if(i > 11) then
            table.insert(game.terrain, app.terrain.init({x = math.random(level.xMin, level.xMax), y = math.random(level.yMin, level.yMax)}, math.random(8, 16), app.terrain.TYPE_TRAP))
        end
        if(i > 12) then
            local sniperNum = math.min(math.round(i/7), 10)
            level.spawn(app.enemy.ENEMY_SNIPER, sniperNum, false, config.clearRadius, 0, true)
        end
        if(i > 14) then
            local mutantNum = math.min(math.round(i/12), 5)
            level.spawn(app.enemy.ENEMY_MUTANT, mutantNum, false, config.clearRadius, 0, true)
        end
        level.endlessLoop = timer.performWithDelay(nextRound, spawnRound, 1)
    end
    level.endlessLoop = timer.performWithDelay(80000, spawnRound, 1)
end

return level