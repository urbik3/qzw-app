local app = require("src.app")
local game = require("src.game")
local json = require("json")

local screen = {}

screen.textColor = {.92, .45, .24, 1}
screen.textColorLighter = {.98, .65, .54, 1}
screen.font = "assets/fonts/Roboto-Regular.ttf"
screen.uiScale = .3

screen.init = function()
    screen.gameCont = display.newGroup()
    screen.gameUICont = display.newGroup()
    screen.menuCont = display.newGroup()
    screen.tutorialCont = display.newGroup() -- BASIC POPUP GROUP
    screen.tutorialCont.x = display.screenOriginX
    screen.tutorialCont.y = display.screenOriginY
end

screen.formatTime = function(time)
    local time = math.round(time/1000)
    local textTime = math.floor(time/60).."m "..(time%60).."s"
    return textTime
end

screen.startLevel = function()
    local loadingText = display.newText({
        text = app.data.lang.get("loadingLevel"),
        font = app.screen.font,
        fontSize = 24,
    })
    loadingText.fill = app.screen.textColor
    loadingText.x = display.contentCenterX
    loadingText.y = display.contentCenterY
    local loadTimer = timer.performWithDelay(100, function()
        loadingText.text = loadingText.text.."."
    end, 0)
    timer.performWithDelay(500, function()
        timer.cancel(loadTimer)
        loadingText:removeSelf()
        app.game.init(app.state.actualCharacter, app.state.actualChapter, app.state.actualLevel)
        app.game.run()
    end, 1)
end

screen.BTN_STATE_NORMAL = "btn_state_normal"
screen.BTN_STATE_HIGHLIGHTED = "btn_state_highlighted"
screen.BTN_STATE_REDUCED = "btn_state_reduced"
screen.BTN_STATE_DISABLED = "btn_state_disabled"
screen.BTN_STATE_INFOBOX = "btn_state_infobox"

screen.createBtn = function(parent, icon, touchFn, state)
    if(state == nil) then state = screen.BTN_STATE_NORMAL end

    local btnGroup = display.newGroup()
    btnGroup.xScale = screen.uiScale
    btnGroup.yScale = screen.uiScale
    parent:insert(btnGroup)

    local btn
    if(state == screen.BTN_STATE_DISABLED or state == screen.BTN_STATE_REDUCED) then
        btn = display.newImageRect(btnGroup, "assets/ui/btn-disabled.png", 132, 132)
    else
        btn = display.newImageRect(btnGroup, "assets/ui/btn.png", 132, 132)
    end

    if(icon) then
        local iconImg = display.newImageRect(btnGroup, "assets/ui/icons/icon-"..icon..".png", 132, 132)
        if(state == screen.BTN_STATE_DISABLED) then
            iconImg.alpha = .25
        end
    end

    if(state ~= screen.BTN_STATE_DISABLED and state ~= screen.BTN_STATE_INFOBOX) then
        btn:addEventListener("touch", function(event)
            if(event.phase == "began") then
                touchFn()
                return true
            end
        end)
    end

    btnGroup.state = state

    btnGroup.changeState = function(self, newState)
        local newBtn = screen.createBtn(parent, icon, touchFn, newState)
        newBtn.x = self.x
        newBtn.y = self.y
        newBtn.xScale = self.xScale
        newBtn.yScale = self.yScale
        newBtn.anchorX = self.anchorX
        newBtn.anchorY = self.anchorY
        btnGroup.state = newState
        self:removeSelf()
        self = nil
        return newBtn
    end

    return btnGroup
end

screen.BTN_GREEN = "btnGreen"
screen.BTN_ORANGE = "btnOrange"
screen.createTextBtn = function(parent, text, touchFn, btnType)
    btnType = btnType == nil and screen.BTN_ORANGE or btnType
    local btnSrc = btnType == screen.BTN_ORANGE and "btn-text" or "btn-text-green"

    local btnGroup = display.newGroup()
    parent:insert(btnGroup)

    local btn = display.newImageRect(btnGroup, "assets/ui/"..btnSrc..".png", 428, 132)
    btn.xScale = screen.uiScale
    btn.yScale = screen.uiScale
    btn:addEventListener("touch", function(event)
        if(event.phase == "began") then
            touchFn()
            return true
        end
    end)

    local headerText = display.newText({parent = btnGroup, text = text, font = screen.font, fontSize = 14, align = "center"})
    headerText.fill = screen.textColorLighter
    btnGroup.text = headerText

    return btnGroup
end

screen.createPopup = function()
    local popup = {}
    popup.popupGroup = display.newGroup()
    screen.menuCont:insert(popup.popupGroup)

    local bg = display.newRect(popup.popupGroup, 0, 0, display.actualContentWidth, display.actualContentHeight)
    bg.fill = {0, .5}
    bg.x = display.actualContentWidth/2
    bg.y = display.actualContentHeight/2
    bg:addEventListener("touch", function()
        return true
    end)

    local window = display.newImageRect(popup.popupGroup, "assets/ui/window-small.png", 512, 638)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.contentCenterX
    window.y = display.contentCenterY

    return popup
end

screen.createPopupLarge = function(headerText)
    local popupGroup = display.newGroup()
    screen.menuCont:insert(popupGroup)

    local bg = display.newRect(popupGroup, 0, 0, display.actualContentWidth*2, display.actualContentHeight*2)
    bg.x = display.screenOriginX
    bg.y = display.screenOriginY
    bg.fill = {0, .5}
    bg:addEventListener("touch", function()
        return true
    end)

    local window = display.newImageRect(popupGroup, "assets/ui/window.png", 1312, 848)
    window.xScale = screen.uiScale
    window.yScale = screen.uiScale
    window.x = display.contentCenterX
    window.y = display.contentCenterY

    local header = display.newImageRect(popupGroup, "assets/ui/header.png", 478, 112)
    header.xScale = screen.uiScale
    header.yScale = screen.uiScale
    header.x = display.contentCenterX
    header.y = 35

    local headerText = display.newText({parent = popupGroup, text = headerText, font = screen.font, fontSize = 16, align = "center"})
    headerText.x = display.contentCenterX
    headerText.y = 35
    headerText.fill = screen.textColor

    return {popupGroup = popupGroup}
end

screen.createLoader = function(parent)
    local group = display.newGroup()
    parent:insert(group)
    local circle = display.newCircle(group, 0, 0, 20)
    circle.fill = {.73, .23, .01, 1}
    transition.to(circle, {time = 1000, xScale = 2, yScale = 2, alpha = 0, iterations = -1})
    local circle2 = display.newCircle(group, 0, 0, 20)
    circle2.fill = {.73, .23, .01, 1}
    transition.to(circle2, {time = 1000, delay = 500, xScale = 2, yScale = 2, alpha = 0, iterations = -1})
    return group
end

return screen