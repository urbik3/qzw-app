local app = require("src.app")
local game = require("src.game")
local json = require("json")

local state = {
    original = {}
}

-- FILES
local FILE_SAVE = "savedState.json"

state.loadSavedData = function()
    local path = system.pathForFile(FILE_SAVE, system.DocumentsDirectory)
    local file, errorString = io.open(path, "r")
    if file then
        local contents = file:read("*a")
        contents = json.decode(contents)
        -- print("LOAD", json.prettify(contents))
        state.enemyInfos = contents.enemyInfos
        state.levels = contents.levels
        state.player = contents.player
        state.progression = contents.progression
        state.score = contents.score
        state.username = contents.username
        state.lang = contents.lang
        app.data.lang.lang = state.lang
        app.data.achievement.loadDataFromSave(contents.achievements)
        io.close(file)
    else
        state.enemyInfos = state.original.enemyInfos
        for enemyType, enemy in pairs(app.screens.newEnemy.enemyTexts) do
            state.enemyInfos[enemyType] = false
        end
        state.levels = app.fn.deepcopy(state.original.levels)
        state.player = app.fn.deepcopy(state.original.player)
        state.progression = app.fn.deepcopy(state.original.progression)
        state.score = app.fn.deepcopy(state.original.score)
        app.data.lang.lang = app.data.lang.LANG_EN
    end
    file = nil
end

state.saveData = function()
    local achievementsData = app.data.achievement.getDataForSave()
    local saveData = {
        levels = state.levels,
        player = state.player,
        progression = state.progression,
        score = state.score,
        username = state.username,
        lang = state.lang,
        achievements = achievementsData,
        enemyInfos = state.enemyInfos,
    }
    local saveDataJson = json.encode(saveData)
    local path = system.pathForFile(FILE_SAVE, system.DocumentsDirectory)
    local file, errorString = io.open(path, "w")
    if not file then
        print("File error: " .. errorString)
    else
        file:write(saveDataJson)
        io.close(file)
    end
    file = nil
end

-- CONSTANTS
state.CHAPTER_TUTORIAL = "chapterTutorial"
state.CHAPTER_CASUAL = "chapterCasual"
state.CHAPTER_HARD = "chapterHard"
state.CHAPTER_ENDLESS = "chapterEndless"

state.CHARACTER_BASIC = "characterSoldier"
state.CHARACTER_MEDIC = "characterMedic"
state.CHARACTER_ASSAULT = "characterAssault"
state.CHARACTER_PYRO = "characterPyro"
state.CHARACTER_SNIPER = "characterSniper"

-- VARS
state.username = nil
state.lang = nil
state.actualCharacter = state.CHARACTER_BASIC
state.actualChapter = state.CHAPTER_CASUAL
state.actualLevel = 1

-- SAVE SCORE LOCALLY
state.original.score = {
    [state.CHARACTER_BASIC] = {
        [state.CHAPTER_TUTORIAL] = nil,
        [state.CHAPTER_CASUAL] = nil,
        [state.CHAPTER_HARD] = nil,
        [state.CHAPTER_ENDLESS] = nil,
    },
    [state.CHARACTER_MEDIC] = {
        [state.CHAPTER_TUTORIAL] = nil,
        [state.CHAPTER_CASUAL] = nil,
        [state.CHAPTER_HARD] = nil,
        [state.CHAPTER_ENDLESS] = nil,
    },
    [state.CHARACTER_PYRO] = {
        [state.CHAPTER_TUTORIAL] = nil,
        [state.CHAPTER_CASUAL] = nil,
        [state.CHAPTER_HARD] = nil,
        [state.CHAPTER_ENDLESS] = nil,
    },
    [state.CHARACTER_SNIPER] = {
        [state.CHAPTER_TUTORIAL] = nil,
        [state.CHAPTER_CASUAL] = nil,
        [state.CHAPTER_HARD] = nil,
        [state.CHAPTER_ENDLESS] = nil,
    },
    [state.CHARACTER_ASSAULT] = {
        [state.CHAPTER_TUTORIAL] = nil,
        [state.CHAPTER_CASUAL] = nil,
        [state.CHAPTER_HARD] = nil,
        [state.CHAPTER_ENDLESS] = nil,
    },
}
state.saveScoreLocally = function()
    local lastScore = state.score[state.actualCharacter][state.actualChapter]
    local actualScore = app.state.getChapterTime()
    local saveScore = true
    if(lastScore) then
        saveScore = state.actualChapter == state.CHAPTER_ENDLESS and lastScore > actualScore or lastScore < actualScore
    end
    if(saveScore) then
        state.score[state.actualCharacter][state.actualChapter] = actualScore
        state.saveData()
    end
end

-- ENEMIES
state.original.enemyInfos = {}


-- WIN LEVEL
state.winLevel = function(levelNumber)
    local tutorialFn = nil
    state.updateLevelTime(levelNumber, state.actualChapter)

    local nextLevel = app.state.getLevel(levelNumber+1) and app.state.getLevel(levelNumber+1).state
    if(not nextLevel and state.getLevel(levelNumber).state ~= state.LEVEL_FINISHED and state.actualChapter ~= state.CHAPTER_TUTORIAL) then
        app.network.postScore(app.state.getChapterTime())
    end
    
    state.getLevel(levelNumber).state = state.LEVEL_FINISHED

    local nextCharacter = state.getNextCharacter(state.actualCharacter)
    if(state.actualChapter == state.CHAPTER_CASUAL and levelNumber == 5 and nextCharacter and state.progression[nextCharacter].available == false) then
        state.progression[nextCharacter].available = true
        tutorialFn = function(cb)
            if(not cb) then cb = function()end end
            app.tutorial.createPopup(app.data.lang.get("continue"), app.data.lang.get("tutorialNewCharacter"), function()
                app.tutorial.destroyPopup()
                cb()
            end)
        end
    end

    if(nextLevel and nextLevel == app.state.LEVEL_LOCKED) then
        app.state.getLevel(levelNumber+1).state = app.state.LEVEL_AVAILABLE
    elseif(not nextLevel) then
        local nextChapter
        if(state.actualChapter == state.CHAPTER_TUTORIAL) then
            nextChapter = state.CHAPTER_CASUAL
        else
            nextChapter = state.actualChapter == state.CHAPTER_CASUAL and state.CHAPTER_HARD or state.CHAPTER_ENDLESS
        end
        if(state.actualChapter ~= state.CHAPTER_ENDLESS and not state.progression[state.actualCharacter][nextChapter]) then
            tutorialFn = function(cb)
                if(not cb) then cb = function()end end
                app.tutorial.createPopup(app.data.lang.get("continue"), app.data.lang.get("tutorialNewChapter"), function()
                    app.tutorial.destroyPopup()
                    cb()
                end)
            end
        end
        if(state.actualChapter == state.CHAPTER_TUTORIAL) then
            state.progression[state.actualCharacter][state.CHAPTER_CASUAL] = true
        elseif(state.actualChapter == state.CHAPTER_CASUAL) then
            state.progression[state.actualCharacter][state.CHAPTER_HARD] = true
            state.progression[state.actualCharacter][state.CHAPTER_ENDLESS] = true
        end
    end
    state.saveData()

    return tutorialFn
end

-- PROGRESSION
state.getNextCharacter = function(character)
    local hash = {
        [state.CHARACTER_BASIC] = state.CHARACTER_MEDIC,
        [state.CHARACTER_MEDIC] = state.CHARACTER_PYRO,
        [state.CHARACTER_PYRO] = state.CHARACTER_SNIPER,
        [state.CHARACTER_SNIPER] = state.CHARACTER_ASSAULT,
    }
    return hash[character]
end

state.original.progression = {
    [state.CHARACTER_BASIC] = {
        available = true,
        [state.CHAPTER_CASUAL] = false,
        [state.CHAPTER_HARD] = false,
        [state.CHAPTER_ENDLESS] = false,
    },
    [state.CHARACTER_MEDIC] = {
        available = false,
        [state.CHAPTER_CASUAL] = false,
        [state.CHAPTER_HARD] = false,
        [state.CHAPTER_ENDLESS] = false,
    },
    [state.CHARACTER_PYRO] = {
        available = false,
        [state.CHAPTER_CASUAL] = false,
        [state.CHAPTER_HARD] = false,
        [state.CHAPTER_ENDLESS] = false,
    },
    [state.CHARACTER_SNIPER] = {
        available = false,
        [state.CHAPTER_CASUAL] = false,
        [state.CHAPTER_HARD] = false,
        [state.CHAPTER_ENDLESS] = false,
    },
    [state.CHARACTER_ASSAULT] = {
        available = false,
        [state.CHAPTER_CASUAL] = false,
        [state.CHAPTER_HARD] = false,
        [state.CHAPTER_ENDLESS] = false,
    },
}

-- PLAYER DATA
state.getPlayer = function()
    return state.player[app.state.actualCharacter][app.state.actualChapter]
end

state.ENDLESS_STARTING_MAX_HP = 100
state.original.player = {}
state.original.player[state.CHARACTER_BASIC] = {}
state.original.player[state.CHARACTER_BASIC][state.CHAPTER_TUTORIAL] = {
    hp = 400
}
state.original.player[state.CHARACTER_BASIC][state.CHAPTER_CASUAL] = {
    hp = 75
}
state.original.player[state.CHARACTER_BASIC][state.CHAPTER_HARD] = {
    hp = 1000
}
state.original.player[state.CHARACTER_BASIC][state.CHAPTER_ENDLESS] = {
    hp = state.ENDLESS_STARTING_MAX_HP
}
state.original.player[state.CHARACTER_MEDIC] = app.fn.deepcopy(state.original.player[state.CHARACTER_BASIC])
state.original.player[state.CHARACTER_PYRO] = app.fn.deepcopy(state.original.player[state.CHARACTER_BASIC])
state.original.player[state.CHARACTER_SNIPER] = app.fn.deepcopy(state.original.player[state.CHARACTER_BASIC])
state.original.player[state.CHARACTER_ASSAULT] = app.fn.deepcopy(state.original.player[state.CHARACTER_BASIC])

-- LEVEL DATA
state.LEVEL_LOCKED = "level_locked"
state.LEVEL_AVAILABLE = "level_available"
state.LEVEL_FINISHED = "level_finished"
state.getMaxLevel = function()
    local maxLevel = 0
    for k, level in pairs(state.levels[state.actualCharacter][state.actualChapter]) do
        if((level.state == state.LEVEL_AVAILABLE or level.state == state.LEVEL_FINISHED) and level.num > maxLevel) then
            maxLevel = level.num
        end
    end
    maxLevel = state.levels[state.actualCharacter][state.actualChapter]["level"..maxLevel]
    return maxLevel
end

state.getLevel = function(levelNumber)
    return state.levels[state.actualCharacter][state.actualChapter]["level"..levelNumber]
end

state.getChapterTime = function()
    local chapterTime = 0
    for k, level in pairs(state.levels[state.actualCharacter][state.actualChapter]) do
        if(level.state == state.LEVEL_FINISHED or level.state == state.LEVEL_AVAILABLE) then
            chapterTime = chapterTime + level.time
        end
    end
    return chapterTime
end

state.updateLevelTime = function(levelNumber, chapter)
    if(chapter == state.CHAPTER_ENDLESS) then
        state.getLevel(levelNumber).time = (state.getLevel(levelNumber).time and state.getLevel(levelNumber).time > game.getGameTime()) and state.getLevel(levelNumber).time or game.getGameTime()
    else
        state.getLevel(levelNumber).time = state.getLevel(levelNumber).time + game.getGameTime()
    end
end

state.original.levels = {}
state.original.levels[state.CHARACTER_BASIC] = {}
state.original.levels[state.CHARACTER_BASIC][state.CHAPTER_TUTORIAL] = {
    level1 = {
        num = 1,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 500,
        time = 0,
    },
}
state.original.levels[state.CHARACTER_BASIC][state.CHAPTER_CASUAL] = {
    level1 = {
        num = 1,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 200,
        time = 0,
    },
    level2 = {
        num = 2,
        state = state.LEVEL_LOCKED,
        maxHealth = 300,
        time = 0,
    },
    level3 = {
        num = 3,
        state = state.LEVEL_LOCKED,
        maxHealth = 400,
        time = 0,
    },
    level4 = {
        num = 4,
        state = state.LEVEL_LOCKED,
        maxHealth = 500,
        time = 0,
    },
    level5 = {
        num = 5,
        state = state.LEVEL_LOCKED,
        maxHealth = 600,
        time = 0,
    },
    level6 = {
        num = 6,
        state = state.LEVEL_LOCKED,
        maxHealth = 700,
        time = 0,
    },
    level7 = {
        num = 7,
        state = state.LEVEL_LOCKED,
        maxHealth = 800,
        time = 0,
    },
    level8 = {
        num = 8,
        state = state.LEVEL_LOCKED,
        maxHealth = 900,
        time = 0,
    },
    level9 = {
        num = 9,
        state = state.LEVEL_LOCKED,
        maxHealth = 1000,
        time = 0,
    },
    level10 = {
        num = 10,
        state = state.LEVEL_LOCKED,
        maxHealth = 1100,
        time = 0,
    },
    level11 = {
        num = 11,
        state = state.LEVEL_LOCKED,
        maxHealth = 1200,
        time = 0,
    },
    level12 = {
        num = 12,
        state = state.LEVEL_LOCKED,
        maxHealth = 1300,
        time = 0,
    },
    level13 = {
        num = 13,
        state = state.LEVEL_LOCKED,
        maxHealth = 1400,
        time = 0,
    },
    level14 = {
        num = 14,
        state = state.LEVEL_LOCKED,
        maxHealth = 1500,
        time = 0,
    },
    level15 = {
        num = 15,
        state = state.LEVEL_LOCKED,
        maxHealth = 1600,
        time = 0,
    },
}
state.original.levels[state.CHARACTER_BASIC][state.CHAPTER_HARD] = {
    level1 = {
        num = 1,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 200,
        time = 0,
    },
    level2 = {
        num = 2,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 300,
        time = 0,
    },
    level3 = {
        num = 3,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 400,
        time = 0,
    },
    level4 = {
        num = 4,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 500,
        time = 0,
    },
    level5 = {
        num = 5,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 600,
        time = 0,
    },
    level6 = {
        num = 6,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 700,
        time = 0,
    },
    level7 = {
        num = 7,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 800,
        time = 0,
    },
    level8 = {
        num = 8,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 900,
        time = 0,
    },
    level9 = {
        num = 9,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 1500,
        time = 0,
    },
}
state.original.levels[state.CHARACTER_BASIC][state.CHAPTER_ENDLESS] = {
    level1 = {
        num = 1,
        state = state.LEVEL_AVAILABLE,
        maxHealth = 2500,
        time = 0,
    },
}
state.original.levels[state.CHARACTER_MEDIC] = app.fn.deepcopy(state.original.levels[state.CHARACTER_BASIC])
state.original.levels[state.CHARACTER_PYRO] = app.fn.deepcopy(state.original.levels[state.CHARACTER_BASIC])
state.original.levels[state.CHARACTER_SNIPER] = app.fn.deepcopy(state.original.levels[state.CHARACTER_BASIC])
state.original.levels[state.CHARACTER_ASSAULT] = app.fn.deepcopy(state.original.levels[state.CHARACTER_BASIC])

return state
