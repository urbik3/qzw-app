local app = require("src.app")
local json = require("json")
local applovin = require( "plugin.applovin" )

local game = {
    time = false,
    stopTime = nil,
    pausedTime = 0,
}

game.timers = {}

game.STATUS_IDLE = "status_idle"
game.STATUS_PLAYING = "status_playing"
game.status = game.STATUS_IDLE

game.levelNumber = false
game.chapter = false

game._win = false
game._lose = false

game.playerInstance = nil
game.enemies = {}
game.shots = {}
game.consumables = {}
game.portal = nil
game.turret = nil
game.terrain = {}
game.obstacles = {}
game.objectiveEnemies = {}

local touchStop

game.getGameTime = function()
    return game.time - game.pausedTime - app.time.gameStartTime
end

game.getTime = function()
    return game.time - game.pausedTime
end

game.init = function(character, chapter, level)
    game.chapter = chapter
    game.levelNumber = level

    game.scale = .2
    game.zoom = 1

    game.skyboxCont = display.newGroup()
    app.screen.gameCont:insert(game.skyboxCont)
    game.bgCont = display.newGroup()
    app.screen.gameCont:insert(game.bgCont)
    game.groundCont = display.newGroup()
    app.screen.gameCont:insert(game.groundCont)
    game.projectilesCont = display.newGroup()
    app.screen.gameCont:insert(game.projectilesCont)
    game.enemiesCont = display.newGroup()
    app.screen.gameCont:insert(game.enemiesCont)
    game.playerCont = display.newGroup()
    app.screen.gameCont:insert(game.playerCont)
    game.guiCont = display.newGroup()
    app.screen.gameCont:insert(game.guiCont)
    game.lightningCont = display.newGroup()
    app.screen.gameCont:insert(game.lightningCont)
    game.radarCont = display.newGroup()
    app.screen.gameCont:insert(game.radarCont)
    game.moveShootGuiCont = display.newGroup()
    app.screen.gameUICont:insert(game.moveShootGuiCont)
    game.zoomOutCont = display.newGroup()
    app.screen.gameUICont:insert(game.zoomOutCont)
    game.pauseMenuCont = display.newGroup()
    app.screen.gameUICont:insert(game.pauseMenuCont)

    game.bgCont.x = display.screenOriginX
    game.bgCont.y = display.screenOriginY
    game.groundCont.x = display.screenOriginX
    game.groundCont.y = display.screenOriginY
    game.projectilesCont.x = display.screenOriginX
    game.projectilesCont.y = display.screenOriginY
    game.enemiesCont.x = display.screenOriginX
    game.enemiesCont.y = display.screenOriginY
    game.guiCont.x = display.screenOriginX
    game.guiCont.y = display.screenOriginY
    game.lightningCont.x = display.screenOriginX
    game.lightningCont.y = display.screenOriginY
    game.radarCont.x = display.screenOriginX
    game.radarCont.y = display.screenOriginY
    game.moveShootGuiCont.x = display.screenOriginX
    game.moveShootGuiCont.y = display.screenOriginY
    game.zoomOutCont.x = display.screenOriginX
    game.zoomOutCont.y = display.screenOriginY
    game.pauseMenuCont.x = display.screenOriginX
    game.pauseMenuCont.y = display.screenOriginY

    touchStop = display.newRect(game.zoomOutCont, 0, 0, display.actualContentWidth, display.actualContentHeight)
    touchStop.alpha = .01
    touchStop.anchorX = 0
    touchStop.anchorY = 0
    touchStop.isVisible = false
    touchStop:addEventListener("tap", function() return true end)
    touchStop:addEventListener("touch", function() return true end)

    game.zoomOutContHidden = display.newGroup()
    game.zoomOutCont:insert(game.zoomOutContHidden)
    game.zoomOutContHidden.isVisible = false

    if(app.state.actualChapter == app.state.CHAPTER_ENDLESS) then
        app.state.player[app.state.actualCharacter][app.state.actualChapter].hp = app.state.ENDLESS_STARTING_MAX_HP
    end

    -- LEVEL INIT
    app.gui.init()
    app.level.init(level, chapter)

    app.data.achievement.levelData = {}
    
    app.time.gameStart()
    game.pausedTime = 0
    game.stopTime = nil

    app.flurry.logEvent("Start game", {
        character = app.state.actualCharacter,
        level = ""..app.state.actualLevel,
        chapter = app.state.actualChapter,
    })
end

game.addLightning = function(amount)
    local rectSideLength
    if(display.actualContentWidth > display.actualContentHeight) then
        rectSideLength = display.actualContentWidth*3
    else
        rectSideLength = display.actualContentHeight*3
    end
    local darkness = display.newRect(game.lightningCont, 0, 0, rectSideLength, rectSideLength)
    darkness.fill = {0}
    darkness.alpha = amount
    darkness.x = display.actualContentWidth/2
    darkness.y = display.actualContentHeight/2
    local light = graphics.newMask("assets/lightning-mask.jpg")
    darkness:setMask(light)
    game.darkness = darkness
end

game.getEnemiesInShotRange = function(enemies)
    local enemiesInShotRange = {}
    for k, enemy in pairs(enemies) do
        local insertToTable = true
        local enemyPos = enemy.getPosition()
        local contentX, contentY = game.enemiesCont:localToContent(enemyPos.x, enemyPos.y)
        if(contentX < -app.shot.shotStagePadding + display.screenOriginX or contentX > display.actualContentWidth + app.shot.shotStagePadding) then
            insertToTable = false
        end
        if(contentY < -app.shot.shotStagePadding + display.screenOriginY or contentY > display.actualContentHeight + app.shot.shotStagePadding) then
            insertToTable = false
        end
        if(insertToTable == true) then
            table.insert(enemiesInShotRange, enemy)
        end
    end

    return enemiesInShotRange
end

game.getActiveEnemies = function(enemies)
    local activeEnemies = {}

    for k, enemy in pairs(enemies) do
        if(enemy.type == app.enemy.ENEMY_SCOUT or enemy.type == app.enemy.ENEMY_SNIPER) then
            table.insert(activeEnemies, enemy)
        elseif(enemy.isPatrol) then
            table.insert(activeEnemies, enemy)
        end
    end

    activeEnemies = app.fn.concatTables(activeEnemies, game.enemiesInShotRange)

    return activeEnemies
end

game.run = function()
    if(game.gameLoopTimer) then
        game.stop()
    end
    game.status = game.STATUS_PLAYING
    transition.resume()
    timer.resumeAll()
    local gameLoop = function(event)
        -- GAME TIME
        if(game.status ~= game.STATUS_PLAYING) then return end
        game.time = event.time
        if(game.stopTime) then
            game.pausedTime = game.pausedTime + game.time - game.stopTime
            game.stopTime = nil
        end

        -- PLAYER
        app.player.update(game.playerInstance, game.consumables, game.terrain)

        -- CONSUMABLES
        local filteredConsumables = {}
        for k, consumable in pairs(game.consumables) do
            if(consumable._deleted == false) then
                table.insert(filteredConsumables, consumable)
            else
                consumable.consumableSprite:removeSelf()
                consumable = nil
            end
        end
        game.consumables = filteredConsumables
        
        -- ENEMIES IN SHOT RANGE
        game.enemiesInShotRange = game.getEnemiesInShotRange(game.enemies)
        
        -- TUTORIAL
        if(app.tutorial.firstEnemySighted and #game.enemiesInShotRange > 0 and app.state.actualCharacter == app.state.CHARACTER_BASIC and app.game.chapter == app.state.CHAPTER_TUTORIAL) then
            local isVisible = false
            for k, enemy in pairs(game.enemiesInShotRange) do
                local positionX, positionY = enemy.cont:localToContent(0, 0)
                positionX = positionX - display.screenOriginX
                positionY = positionY - display.screenOriginY
                local positionMargin = 20
                if(positionX > positionMargin + app.gui.guiSideWidth and positionY > positionMargin and positionX < display.actualContentWidth - positionMargin - app.gui.guiSideWidth and positionY < display.actualContentHeight - positionMargin) then
                    isVisible = true
                    break
                end
            end
            if(isVisible) then
                app.tutorial.firstEnemySighted = false
                game.stop()
                app.tutorial.createPopup(app.data.lang.get("continue"), app.data.lang.get("tutorialAiming"), function()
                    app.tutorial.destroyPopup()
                    game.run()
                end)
            end
        end

        if(app.game.chapter == app.state.CHAPTER_TUTORIAL and app.tutorial.firstInfoConsumableSighted and app.state.actualCharacter == app.state.CHARACTER_BASIC) then
            local isVisible = false
            for k, consumable in pairs(game.consumables) do
                local positionX, positionY = consumable.consumableSprite:localToContent(0, 0)
                positionX = positionX - display.screenOriginX
                positionY = positionY - display.screenOriginY
                local positionMargin = 20
                if(positionX > positionMargin + app.gui.guiSideWidth and positionY > positionMargin and positionX < display.actualContentWidth - positionMargin - app.gui.guiSideWidth and positionY < display.actualContentHeight - positionMargin) then
                    isVisible = true
                    break
                end
            end
            if(isVisible) then
                app.tutorial.firstInfoConsumableSighted = false
                game.stop()
                app.tutorial.createPopup(app.data.lang.get("continue"), app.data.lang.get("tutorialInfoConsumable"), function()
                    app.tutorial.destroyPopup()
                    game.run()
                end)
            end
        end

        -- SHOTS
        for k, shot in pairs(game.shots) do
            app.shot.update(shot, game.playerInstance, game.enemiesInShotRange, game.turret)
        end

        -- ENEMIES
        if(game.turret and (game.turret.timers.activate or game.turret.active)) then
            for k, enemy in pairs(game.enemies) do
                enemy:onUpdate(game.playerInstance, game.enemiesInShotRange)
            end
        else
            game.activeEnemies = game.getActiveEnemies(game.enemies)
            for k, enemy in pairs(game.activeEnemies) do
                enemy:onUpdate(game.playerInstance, game.enemiesInShotRange)
            end
        end

        for i, obstacle in pairs(game.obstacles) do
            app.obstacle.update(obstacle, game.playerInstance, game.enemiesInShotRange)
        end

        -- PORTAL
        if(game.portal) then
            app.portal.update(game.portal, game.playerInstance, game.enemies)
        end

        -- TURRET
        if(game.turret) then
            app.turret.update(game.turret, game.playerInstance, game.enemies)
        end

        -- GUI
        app.gui.update(game.playerInstance, game.portal, game.turret, game.enemies, game.objectiveEnemies)

        -- WIN/LOSE
        if(game._lose) then game.lose()
        elseif(game._win) then game.win() end
    end
    game.mspf = 1000/30
    game.gameLoopTimer = timer.performWithDelay(game.mspf, gameLoop, 0)
end

game.objectiveComplete = function(objectiveType)
    if(#app.level.objectiveStack <= 0) then
        game._win = true
    else
        app.level.objectiveStack[1]()
        table.remove(app.level.objectiveStack, 1)
    end
end

game.ZOOM_OUT = .5
game.ZOOM_IN = 1
game.zoomAnimation = function(iterations, targetZoom)
    if(not iterations) then iterations = 15 end

    if(not targetZoom) then
        if(game.zoom == game.ZOOM_IN) then
            targetZoom = game.ZOOM_OUT
        else
            targetZoom = game.ZOOM_IN
        end
    end

    if(targetZoom == game.ZOOM_IN) then
        game.run()
        touchStop.isVisible = false
    else
        game.stop()
        touchStop.isVisible = true
    end

    local zoom = game.zoom
    local iterations = 15
    local step = (zoom - targetZoom) / iterations
    
    local i = 0;
    local zoomAnim = function()
        i = i + 1
        if(i == iterations) then
            zoom = targetZoom
        else
            zoom = zoom - step
        end

        local xPos = (display.contentWidth - (display.contentWidth * zoom)) / 2
        local yPos = (display.contentHeight - (display.contentHeight * zoom)) / 2

        app.screen.gameCont.xScale = zoom
        app.screen.gameCont.yScale = zoom
        app.screen.gameCont.x = xPos
        app.screen.gameCont.y = yPos

        if(targetZoom == game.ZOOM_IN and zoom == targetZoom) then
            game.guiCont.isVisible = true
            game.moveShootGuiCont.isVisible = true
            game.zoomOutContHidden.isVisible = false
        elseif(targetZoom == game.ZOOM_OUT and zoom ~= game.ZOOM_IN) then
            game.guiCont.isVisible = false
            game.moveShootGuiCont.isVisible = false
            game.zoomOutContHidden.isVisible = false
            if(math.abs(zoom - game.ZOOM_OUT) < .0001) then
                game.zoomOutContHidden.isVisible = true
            end
        else
            game.zoomOutContHidden.isVisible = false
        end

        if(math.abs(zoom - targetZoom) < .0001) then
            app.gui.zoomOutBtnRight = app.gui.zoomOutBtnRight:changeState(app.screen.BTN_STATE_NORMAL)
        elseif(app.gui.zoomOutBtnRight.state == app.screen.BTN_STATE_NORMAL) then
            app.gui.zoomOutBtnRight = app.gui.zoomOutBtnRight:changeState(app.screen.BTN_STATE_DISABLED)
        end

        game.zoom = zoom
    end
    
    timer.performWithDelay(game.mspf, zoomAnim, iterations)
end

local blurScreen = function()
    local screenCapture = display.captureScreen(false)
    app.screen.menuCont:insert(screenCapture)

    screenCapture.anchorX = 0
    screenCapture.anchorY = 0
    screenCapture.x = display.screenOriginX
    screenCapture.y = display.screenOriginY

    screenCapture.fill.effect = "filter.blurGaussian"
    screenCapture.fill.effect.horizontal.blurSize = 32
    screenCapture.fill.effect.vertical.blurSize = 32
    app.screen.screenCapture = screenCapture

    transition.from(screenCapture.fill.effect.horizontal, {time = 1000, blurSize = 0, sigma = 0})
    transition.from(screenCapture.fill.effect.vertical, {time = 1000, blurSize = 0, sigma = 0})
end

game.win = function()
    game.status = game.STATUS_IDLE
    system.vibrate()
    game.stop()
    local tutorialFn = app.state.winLevel(app.level.levelNumber)
    app.flurry.logEvent("Win game", {
        character = app.state.actualCharacter,
        level = ""..app.state.actualLevel,
        chapter = app.state.actualChapter,
        time = ""..game.getGameTime(),
    })

    local health, maxHealth = game.playerInstance.hp, game.playerInstance.maxHp

    app.screens.win.show(function()
        app.player.destroy(game.playerInstance)
        -- blurScreen()
        game.clearScene()
        if(app.state.actualChapter == app.state.CHAPTER_TUTORIAL) then
            app.screens.gameSelect.show()
        else
            app.screens.levels.showMenu(true)
        end

        local achievementsFn = function()
            -- ACHIEVEMENTS
            if(health == maxHealth) then
                if(game.chapter == app.state.CHAPTER_CASUAL) then
                    app.data.achievement.logEvent(app.data.achievement.ACHIEVEMENT_NO_DAMAGE_TAKEN, app.data.achievement.FULL_HEALTH_CASUAL)
                elseif(game.chapter == app.state.CHAPTER_HARD) then
                    app.data.achievement.logEvent(app.data.achievement.ACHIEVEMENT_NO_DAMAGE_TAKEN, app.data.achievement.FULL_HEALTH_HARD)
                end
            end

            if(not app.data.achievement.levelData.hasBeenHit) then
                if(game.chapter == app.state.CHAPTER_CASUAL) then
                    app.data.achievement.logEvent(app.data.achievement.ACHIEVEMENT_NO_DAMAGE_TAKEN, app.data.achievement.NO_DAMAGE_TAKEN_CASUAL)
                elseif(game.chapter == app.state.CHAPTER_HARD) then
                    app.data.achievement.logEvent(app.data.achievement.ACHIEVEMENT_NO_DAMAGE_TAKEN, app.data.achievement.NO_DAMAGE_TAKEN_HARD)
                end
            end
        end

        if(tutorialFn) then
            tutorialFn(achievementsFn)
        else
            achievementsFn()
        end
    end)
end

game.lose = function()
    game.status = game.STATUS_IDLE
    local bloodEffect = display.newSprite(game.groundCont, app.enemy.gfx.bloodEffect, {
        name = "splash",
        start = 1,
        count = 4*8-16,
        time = 600,
        loopCount = 1,
    })
    bloodEffect.x = game.playerInstance.getPosition().x
    bloodEffect.y = game.playerInstance.getPosition().y
    bloodEffect.rotation = math.random(0, 360)
    bloodEffect:play()
    bloodEffect:addEventListener("sprite", function(event)
        if(event.phase == "ended") then
            app.screens.lose.show(function()
                -- blurScreen()
                game.clearScene()
                app.screens.story.showDeath()
                if(app.state.actualChapter == app.state.CHAPTER_ENDLESS) then
                    app.network.postScore(app.state.getChapterTime())
                end

                -- if(applovin.isLoaded("interstitial")) then
                --     applovin.show("interstitial")
                -- end
            end)
        end
    end)
    app.sound.playSFX("bloodSplash")

    -- ENDLESS
    if(app.level.endlessLoop) then
        timer.cancel(app.level.endlessLoop)
        app.level.endlessLoop = nil
    end
    if(app.state.actualChapter == app.state.CHAPTER_ENDLESS) then
        app.state.player[app.state.actualCharacter][app.state.actualChapter].hp = app.state.ENDLESS_STARTING_MAX_HP
    end

    app.data.achievement.logEvent(app.data.achievement.ACHIEVEMENT_REINCARNATION, 1)
    
    -- END IT
    system.vibrate()
    game.stop()
    app.player.destroy(game.playerInstance)
    app.state.updateLevelTime(app.level.levelNumber, app.level.chapter)
    app.state.saveData()
    app.flurry.logEvent("Lose game", {
        character = app.state.actualCharacter,
        level = ""..app.state.actualLevel,
        chapter = app.state.actualChapter,
        time = ""..game.getGameTime(),
    })
end

game.stop = function()
    if(game.gameLoopTimer) then
        timer.cancel(game.gameLoopTimer)
        game.gameLoopTimer = nil
        game.stopTime = game.time
        transition.pause()
        timer.pauseAll()
    end
end

game.clearScene = function()
    game.playerInstance = nil
    game.enemies = {}
    game.shots = {}
    game.consumables = {}
    game.terrain = {}
    game.obstacles = {}
    game.portal = nil
    game.turret = nil
    game.objectiveEnemies = {}
    game.darkness = nil

    game._win = false
    game._lose = false

    transition.cancelAll()
    timer.cancelAll()

    game.skyboxCont:removeSelf()
    game.bgCont:removeSelf()
    game.groundCont:removeSelf()
    game.projectilesCont:removeSelf()
    game.enemiesCont:removeSelf()
    game.playerCont:removeSelf()
    game.guiCont:removeSelf()
    game.lightningCont:removeSelf()
    game.radarCont:removeSelf()
    game.moveShootGuiCont:removeSelf()
    game.pauseMenuCont:removeSelf()
    game.zoomOutCont:removeSelf()
    
    game.zoom = 1
    app.screen.gameCont.xScale = game.zoom
    app.screen.gameCont.yScale = game.zoom
    app.screen.gameCont.x = 0
    app.screen.gameCont.y = 0

    app.level.reset()
    
    if(app.gui.lockOnEnemy) then
        app.gui.destroyLockOn()
    end
end

return game