local app = require("src.app")

local obstacle = {}

obstacle.TYPE_CIRCLE = "typeCircle"

obstacle.spawn = function(position, type, dimensions)
    local instance = {
        type = type
    }

    local cont = display.newGroup()
    app.game.groundCont:insert(cont)

    if(type == obstacle.TYPE_CIRCLE) then
        local shape = display.newCircle(cont, 0, 0, dimensions.radius)
        shape.fill = {0, 0, 0, 1}
        shape.stroke = {1, 0, 0, 1}
        shape.strokeWidth = 2

        instance.shape = shape
        instance.cont = cont
        instance.cont.x = position.x
        instance.cont.y = position.y
        instance.radius = dimensions.radius
    end
    
    return instance
end

obstacle.update = function(instance, player, enemies)
    local characters = {player}
    for i, enemy in pairs(enemies) do
        table.insert(characters, enemy)    
    end

    if(instance.type == obstacle.TYPE_CIRCLE) then
        for i, character in pairs(characters) do
            local distance = app.fn.distance(character.getPosition(), instance.cont)
            if(distance < instance.radius + character.radius) then
                local angle = app.fn.getAngle(instance.cont, character.getPosition()) - math.pi
                local distanceCorrection = instance.radius + character.radius - distance
                local vectorCorrection = ssk.math2d.scale(ssk.math2d.angle2Vector(angle, true), distanceCorrection)
                local characterPosition = character.getPosition()
                character.setPosition({x = characterPosition.x + vectorCorrection.x, y = characterPosition.y + vectorCorrection.y})
            end
        end
    end
end

return obstacle