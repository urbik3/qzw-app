local app = require("src.app")
local game = require("src.game")
local json = require("json")

local sound = {
    sfx = {},
    sfxMinDelay = 100,
    playingMusic = false,
    filenamePlaying = false
}

sound.SOUND_CONTROL_ON = "soundControlOn"
sound.SOUND_CONTROL_OFF = "soundControlOff"
sound.MUSIC_CONTROL_ON = "musicControlOn"
sound.MUSIC_CONTROL_OFF = "musicControlOff"
sound.soundControl = sound.SOUND_CONTROL_ON
sound.musicControl = sound.MUSIC_CONTROL_ON

sound.init = function()
    sound.sfx.bloodSplash = {sound = audio.loadSound("assets/sound/sfx/blood_sfx1.mp3"), timePlayed = 0}
    sound.sfx.bloodSplash2 = {sound = audio.loadSound("assets/sound/sfx/blood_sfx3.mp3"), timePlayed = 0}
    sound.sfx.growl = {sound = audio.loadSound("assets/sound/sfx/monster_growl.mp3"), timePlayed = 0}
    sound.sfx.growl2 = {sound = audio.loadSound("assets/sound/sfx/monster_growl2.mp3"), timePlayed = 0}
    sound.sfx.armorUp = {sound = audio.loadSound("assets/sound/sfx/armor_up.mp3"), timePlayed = 0}
    sound.sfx.healUp = {sound = audio.loadSound("assets/sound/sfx/armor_up.mp3"), timePlayed = 0}
    sound.sfx.supercharge = {sound = audio.loadSound("assets/sound/sfx/armor_up.mp3"), timePlayed = 0}
    sound.sfx.explode = {sound = audio.loadSound("assets/sound/sfx/explosion.mp3"), timePlayed = 0}

    sound.reset()
    
    Runtime:addEventListener("system", function(event)
        if(event.type == "applicationResume") then
            sound.reset()
        end
    end)
end

sound.reset = function()
    audio.setVolume(1, {channel = 1})
    -- audio.setVolume(0, {channel = 1})
    for i = 2, audio.totalChannels, 1 do
        audio.setVolume(.5, {channel = i})
    end
end

sound.soundOn = function()
    for i = 2, audio.totalChannels, 1 do
        audio.setMaxVolume(.5, {channel = i})
    end
    sound.soundControl = sound.SOUND_CONTROL_ON
end

sound.soundOff = function()
    for i = 2, audio.totalChannels, 1 do
        audio.setMaxVolume(0, {channel = i})
    end
    sound.soundControl = sound.SOUND_CONTROL_OFF
end

sound.musicOn = function()
    audio.setMaxVolume(1, {channel = 1})
    sound.musicControl = sound.MUSIC_CONTROL_ON
end

sound.musicOff = function()
    audio.setMaxVolume(0, {channel = 1})
    sound.musicControl = sound.MUSIC_CONTROL_OFF
end

sound.playSFX = function(sfx)
    if(game.getTime() - sound.sfx[sfx].timePlayed >= sound.sfxMinDelay) then
        sound.sfx[sfx].timePlayed = game.getTime()
        audio.play(sound.sfx[sfx].sound)
    end
end

sound.playMusic = function(filename)
    local playMusic = function()
        sound.filenamePlaying = filename
        local backgroundMusic = audio.loadStream("assets/sound/music/"..filename..".mp3")
        sound.playingMusic = audio.play( backgroundMusic, { loops = -1, channel = 1 } )
    end

    if(sound.filenamePlaying and filename ~= sound.filenamePlaying) then
        if(sound.playingMusic) then
            audio.fadeOut({channel = 1, time = 1000})
            timer.performWithDelay(1000, function()
                audio.stop(1)
                audio.setVolume(1, {channel = 1})
                playMusic()
            end, 1)
        end
    else
        playMusic()
    end
end

return sound